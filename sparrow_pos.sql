/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100418
 Source Host           : localhost:3306
 Source Schema         : sparrow_pos

 Target Server Type    : MySQL
 Target Server Version : 100418
 File Encoding         : 65001

 Date: 04/04/2021 22:50:31
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for page
-- ----------------------------
DROP TABLE IF EXISTS `page`;
CREATE TABLE `page`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `object_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of page
-- ----------------------------
INSERT INTO `page` VALUES (1, 'customer', 'list', 'customer', NULL, NULL, NULL);
INSERT INTO `page` VALUES (2, 'customer', 'card', 'customer', NULL, NULL, NULL);
INSERT INTO `page` VALUES (4, 'Item', 'list', 'Item', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for page_group
-- ----------------------------
DROP TABLE IF EXISTS `page_group`;
CREATE TABLE `page_group`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `page_id` bigint NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `group_range` int NULL DEFAULT NULL,
  `stage` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'In',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `page_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of page_group
-- ----------------------------
INSERT INTO `page_group` VALUES (1, 1, 'General', 1, 'show', NULL, NULL, NULL);
INSERT INTO `page_group` VALUES (2, 2, 'Customer', 2, 'show', NULL, NULL, NULL);
INSERT INTO `page_group` VALUES (3, 2, 'People', 1, 'show', NULL, NULL, NULL);
INSERT INTO `page_group` VALUES (4, 4, 'Item Info', 1, 'show', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for page_group_field
-- ----------------------------
DROP TABLE IF EXISTS `page_group_field`;
CREATE TABLE `page_group_field`  (
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `id` bigint NOT NULL AUTO_INCREMENT,
  `page_id` bigint NOT NULL,
  `page_group_id` bigint NOT NULL,
  `table_id` int NULL DEFAULT NULL,
  `field_id` int NULL DEFAULT NULL,
  `page_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field_description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `index` int NULL DEFAULT NULL,
  `field_data_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field_width` int NULL DEFAULT NULL,
  `field_alignment` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field_sortable` tinyint NULL DEFAULT NULL,
  `field_decimal_place` tinyint NULL DEFAULT NULL,
  `field_decimal_option` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'general\r\nquantity\r\ncost\r\nprice\r\namount\r\nmeasurement',
  `hide` tinyint NULL DEFAULT NULL,
  `mandatory` tinyint NULL DEFAULT NULL,
  `read_only` tinyint NULL DEFAULT NULL,
  `max_length` int NULL DEFAULT NULL,
  `search_index` tinyint NULL DEFAULT 0,
  `placeholder` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `table_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `input_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `option_text` varchar(4000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `relation_table_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `relation_code_field` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `relation_desc_field` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `relation_desc_field_2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`, `page_id`, `page_group_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of page_group_field
-- ----------------------------
INSERT INTO `page_group_field` VALUES ('phearom@gmail.com', 1, 1, 1, NULL, NULL, 'list', 'person_id', 'person_id', 1, 'text', 120, 'text-left', NULL, 0, 'text', 0, 0, 0, 250, 0, 'person_id', 'customer', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 20:19:42', '2021-04-04 20:19:42', '2021-04-04 20:19:42');
INSERT INTO `page_group_field` VALUES ('phearom@gmail.com', 2, 1, 1, NULL, NULL, 'list', 'account_number', 'account_number', 7, 'text', 120, 'text-left', NULL, 0, 'text', 0, 1, 0, 250, 0, 'account_number', 'customer', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 17:42:09', '2021-04-04 17:42:09', '2021-04-04 17:42:09');
INSERT INTO `page_group_field` VALUES ('phearom@gmail.com', 3, 1, 1, NULL, NULL, 'list', 'override_default_tax', 'override_default_tax', 6, 'text', 120, 'text-left', NULL, 0, 'text', 0, 0, 0, 250, 0, 'override_default_tax', 'customer', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 17:42:07', '2021-04-04 17:42:07', '2021-04-04 17:42:07');
INSERT INTO `page_group_field` VALUES ('phearom@gmail.com', 4, 1, 1, NULL, NULL, 'list', 'company_name', 'company_name', 5, 'text', 120, 'text-left', NULL, 0, 'text', 0, 0, 0, 250, 0, 'company_name', 'customer', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 17:42:06', '2021-04-04 17:42:06', '2021-04-04 17:42:06');
INSERT INTO `page_group_field` VALUES ('phearom@gmail.com', 6, 2, 3, NULL, NULL, 'card', 'first_name', 'First Name', 1, 'text', 120, 'text-left', NULL, 0, 'text', 0, 1, 0, 250, 0, 'No', 'phppos_people', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 16:37:07', '2021-04-04 16:37:07', '2021-04-04 16:37:07');
INSERT INTO `page_group_field` VALUES ('phearom@gmail.com', 7, 2, 3, NULL, NULL, 'card', 'last_name', 'Last Name', 2, 'text', 120, 'text-left', NULL, 0, 'text', 0, 1, 0, 250, 0, 'Name', 'phppos_people', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 16:37:08', '2021-04-04 16:37:08', '2021-04-04 16:37:08');
INSERT INTO `page_group_field` VALUES ('phearom@gmail.com', 8, 2, 3, NULL, NULL, 'card', 'name_2', 'Name 2', 3, 'text', 120, 'text-left', NULL, 0, 'text', 0, 0, 0, 250, 0, 'Name 2', 'phppos_people', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 16:37:14', '2021-04-04 16:37:14', '2021-04-04 16:37:14');
INSERT INTO `page_group_field` VALUES ('phearom@gmail.com', 9, 2, 3, NULL, NULL, 'card', 'email', 'Email', 4, 'text', 120, 'text-left', NULL, 0, 'text', 0, 1, 0, 250, 0, 'Post Code', 'phppos_people', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 16:37:10', '2021-04-04 16:37:10', '2021-04-04 16:37:10');
INSERT INTO `page_group_field` VALUES ('phearom@gmail.com', 10, 1, 1, NULL, NULL, 'list', 'balance', 'balance', 4, 'text', 120, 'text-left', NULL, 0, 'text', 0, 0, 0, 250, 0, 'override_default_tax', 'customer', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 20:22:52', '2021-04-04 20:22:52', '2021-04-04 20:22:52');
INSERT INTO `page_group_field` VALUES ('phearom@gmail.com', 11, 1, 1, NULL, NULL, 'list', 'credit_limit', 'credit_limit', 3, 'text', 120, 'text-left', NULL, 0, 'text', 0, 0, 0, 250, 0, 'person_id', 'customer', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 20:22:27', '2021-04-04 20:22:27', '2021-04-04 20:22:27');
INSERT INTO `page_group_field` VALUES ('phearom@gmail.com', 13, 2, 3, NULL, NULL, 'card', 'phone_number', 'phone_number', 4, 'text', 120, 'text-left', NULL, 0, 'text', 0, 0, 0, 250, 0, 'Post Code', 'phppos_people', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 16:37:11', '2021-04-04 16:37:11', '2021-04-04 16:37:11');
INSERT INTO `page_group_field` VALUES ('phearom@gmail.com', 14, 2, 3, NULL, NULL, 'card', 'address_1', 'address_1', 4, 'text', 120, 'text-left', NULL, 0, 'text', 0, 1, 0, 250, 0, 'Post Code', 'phppos_people', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 16:37:12', '2021-04-04 16:37:12', '2021-04-04 16:37:12');
INSERT INTO `page_group_field` VALUES ('phearom@gmail.com', 15, 2, 2, NULL, NULL, 'card', 'account_number', 'account_number', 4, 'text', 120, 'text-left', NULL, 0, 'text', 0, 1, 0, 250, 0, 'account_number', 'phppos_customers', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 00:46:06', '2021-04-04 00:46:06', '2021-04-04 00:46:06');
INSERT INTO `page_group_field` VALUES ('phearom@gmail.com', 16, 2, 2, NULL, NULL, 'card', 'company_name', 'company_name', 4, 'text', 120, 'text-left', NULL, 0, 'text', 0, 1, 0, 250, 0, 'company_name', 'phppos_customers', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 00:46:07', '2021-04-04 00:46:07', '2021-04-04 00:46:07');
INSERT INTO `page_group_field` VALUES ('phearom@gmail.com', 18, 1, 1, NULL, NULL, 'list', 'points', 'points', 2, 'text', 120, 'text-right', NULL, 0, 'text', 0, 0, 0, 250, 0, 'points', 'customer', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 17:42:03', '2021-04-04 17:42:03', '2021-04-04 17:42:03');
INSERT INTO `page_group_field` VALUES ('phearom@gmail.com', 19, 1, 1, NULL, NULL, 'list', 'first_name', 'First Name 2', 2, 'text', 120, 'text-left', NULL, 0, 'text', 0, 1, 0, 250, 0, 'First Name', 'phppos_people', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 20:19:17', '2021-04-04 20:19:17', '2021-04-04 20:19:17');
INSERT INTO `page_group_field` VALUES ('phearom@gmail.com', 20, 1, 1, NULL, NULL, 'list', 'disable_loyalty', 'disable_loyalty', 8, 'text', 120, 'text-left', NULL, 0, 'text', 0, 0, 0, 250, 0, 'disable_loyalty', 'phppos_customers', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 20:16:24', '2021-04-04 20:16:24', '2021-04-04 20:16:24');
INSERT INTO `page_group_field` VALUES ('phearom@gmail.com', 21, 2, 3, NULL, NULL, 'card', 'city', 'city', 8, 'text', 120, 'text-left', NULL, 0, 'text', 0, 0, 0, 250, 0, 'City', 'phppos_people', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 20:31:11', '2021-04-04 20:31:11', '2021-04-04 20:31:11');
INSERT INTO `page_group_field` VALUES ('phearom@gmail.com', 22, 2, 2, NULL, NULL, 'card', 'taxable', 'taxable', 1, 'text', 120, 'text-left', NULL, 0, 'text', 0, 1, 0, 250, 0, 'Taxable', 'phppos_customers', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 20:34:51', '2021-04-04 20:34:51', '2021-04-04 20:34:51');
INSERT INTO `page_group_field` VALUES ('phearom@gmail.com', 23, 4, 4, NULL, NULL, 'list', 'name', 'Name', 1, 'text', 120, 'text-left', NULL, 0, 'text', 0, 0, 0, 250, 0, 'Name', 'phppos_item', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 20:46:40', '2021-04-04 20:46:40', '2021-04-04 20:46:40');

-- ----------------------------
-- Table structure for page_group_field_template
-- ----------------------------
DROP TABLE IF EXISTS `page_group_field_template`;
CREATE TABLE `page_group_field_template`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `page_id` bigint NOT NULL,
  `page_group_id` bigint NOT NULL,
  `table_id` int NULL DEFAULT NULL,
  `field_id` int NULL DEFAULT NULL,
  `page_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field_description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `index` int NULL DEFAULT NULL,
  `field_data_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field_width` int NULL DEFAULT NULL,
  `field_alignment` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field_sortable` tinyint NULL DEFAULT NULL,
  `field_decimal_place` tinyint NULL DEFAULT NULL,
  `field_decimal_option` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'general\r\nquantity\r\ncost\r\nprice\r\namount\r\nmeasurement',
  `hide` tinyint NULL DEFAULT NULL,
  `mandatory` tinyint NULL DEFAULT NULL,
  `read_only` tinyint NULL DEFAULT NULL,
  `max_length` int NULL DEFAULT NULL,
  `search_index` tinyint NULL DEFAULT 0,
  `placeholder` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `table_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `input_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `option_text` varchar(4000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `relation_table_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `relation_code_field` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `relation_desc_field` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `relation_desc_field_2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`, `page_id`, `page_group_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of page_group_field_template
-- ----------------------------
INSERT INTO `page_group_field_template` VALUES (1, 1, 1, NULL, NULL, 'list', 'person_id', 'person_id', 8, 'text', 120, 'text-left', NULL, 0, 'text', 1, 0, 0, 250, 0, 'person_id', 'customer', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 17:42:12', '2021-04-04 17:42:12', '2021-04-04 17:42:12');
INSERT INTO `page_group_field_template` VALUES (2, 1, 1, NULL, NULL, 'list', 'account_number', 'account_number', 7, 'text', 120, 'text-left', NULL, 0, 'text', 0, 1, 0, 250, 0, 'account_number', 'customer', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 17:42:09', '2021-04-04 17:42:09', '2021-04-04 17:42:09');
INSERT INTO `page_group_field_template` VALUES (3, 1, 1, NULL, NULL, 'list', 'override_default_tax', 'override_default_tax', 6, 'text', 120, 'text-left', NULL, 0, 'text', 0, 0, 0, 250, 0, 'override_default_tax', 'customer', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 17:42:07', '2021-04-04 17:42:07', '2021-04-04 17:42:07');
INSERT INTO `page_group_field_template` VALUES (4, 1, 1, NULL, NULL, 'list', 'company_name', 'company_name', 5, 'text', 120, 'text-left', NULL, 0, 'text', 0, 0, 0, 250, 0, 'company_name', 'customer', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 17:42:06', '2021-04-04 17:42:06', '2021-04-04 17:42:06');
INSERT INTO `page_group_field_template` VALUES (6, 2, 3, NULL, NULL, 'card', 'first_name', 'First Name', 1, 'text', 120, 'text-left', NULL, 0, 'text', 0, 1, 0, 250, 0, 'No', 'phppos_people', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 16:37:07', '2021-04-04 16:37:07', '2021-04-04 16:37:07');
INSERT INTO `page_group_field_template` VALUES (7, 2, 3, NULL, NULL, 'card', 'last_name', 'Last Name', 2, 'text', 120, 'text-left', NULL, 0, 'text', 0, 1, 0, 250, 0, 'Name', 'phppos_people', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 16:37:08', '2021-04-04 16:37:08', '2021-04-04 16:37:08');
INSERT INTO `page_group_field_template` VALUES (8, 2, 3, NULL, NULL, 'card', 'name_2', 'Name 2', 3, 'text', 120, 'text-left', NULL, 0, 'text', 0, 0, 0, 250, 0, 'Name 2', 'phppos_people', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 16:37:14', '2021-04-04 16:37:14', '2021-04-04 16:37:14');
INSERT INTO `page_group_field_template` VALUES (9, 2, 3, NULL, NULL, 'card', 'email', 'Email', 4, 'text', 120, 'text-left', NULL, 0, 'text', 0, 1, 0, 250, 0, 'Post Code', 'phppos_people', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 16:37:10', '2021-04-04 16:37:10', '2021-04-04 16:37:10');
INSERT INTO `page_group_field_template` VALUES (10, 1, 1, NULL, NULL, 'list', 'balance', 'balance', 4, 'text', 120, 'text-right', NULL, 0, 'text', 0, 0, 0, 250, 0, 'override_default_tax', 'customer', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 17:42:05', '2021-04-04 17:42:05', '2021-04-04 17:42:05');
INSERT INTO `page_group_field_template` VALUES (11, 1, 1, NULL, NULL, 'list', 'credit_limit', 'credit_limit', 3, 'text', 120, 'text-right', NULL, 0, 'text', 0, 0, 0, 250, 0, 'person_id', 'customer', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 17:42:03', '2021-04-04 17:42:03', '2021-04-04 17:42:03');
INSERT INTO `page_group_field_template` VALUES (13, 2, 3, NULL, NULL, 'card', 'phone_number', 'phone_number', 4, 'text', 120, 'text-left', NULL, 0, 'text', 0, 0, 0, 250, 0, 'Post Code', 'phppos_people', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 16:37:11', '2021-04-04 16:37:11', '2021-04-04 16:37:11');
INSERT INTO `page_group_field_template` VALUES (14, 2, 3, NULL, NULL, 'card', 'address_1', 'address_1', 4, 'text', 120, 'text-left', NULL, 0, 'text', 0, 1, 0, 250, 0, 'Post Code', 'phppos_people', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 16:37:12', '2021-04-04 16:37:12', '2021-04-04 16:37:12');
INSERT INTO `page_group_field_template` VALUES (15, 2, 2, NULL, NULL, 'card', 'account_number', 'account_number', 4, 'text', 120, 'text-left', NULL, 0, 'text', 0, 1, 0, 250, 0, 'account_number', 'phppos_customers', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 00:46:06', '2021-04-04 00:46:06', '2021-04-04 00:46:06');
INSERT INTO `page_group_field_template` VALUES (16, 2, 2, NULL, NULL, 'card', 'company_name', 'company_name', 4, 'text', 120, 'text-left', NULL, 0, 'text', 0, 1, 0, 250, 0, 'company_name', 'phppos_customers', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 00:46:07', '2021-04-04 00:46:07', '2021-04-04 00:46:07');
INSERT INTO `page_group_field_template` VALUES (18, 1, 1, NULL, NULL, 'list', 'points', 'points', 2, 'text', 120, 'text-right', NULL, 0, 'text', 0, 0, 0, 250, 0, 'points', 'customer', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 17:42:03', '2021-04-04 17:42:03', '2021-04-04 17:42:03');
INSERT INTO `page_group_field_template` VALUES (19, 1, 1, NULL, NULL, 'list', 'first_name', 'First Name 2', 1, 'text', 120, 'text-left', NULL, 0, 'text', 0, 1, 0, 250, 0, 'First Name', 'phppos_people', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 20:12:28', '2021-04-04 20:12:28', '2021-04-04 20:12:28');
INSERT INTO `page_group_field_template` VALUES (20, 1, 1, NULL, NULL, 'list', 'disable_loyalty', 'disable_loyalty', 8, 'text', 120, 'text-left', NULL, 0, 'text', 0, 0, 0, 250, 0, 'disable_loyalty', 'phppos_customers', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 20:16:24', '2021-04-04 20:16:24', '2021-04-04 20:16:24');
INSERT INTO `page_group_field_template` VALUES (21, 2, 3, NULL, NULL, 'card', 'city', 'city', 8, 'text', 120, 'text-left', NULL, 0, 'text', 0, 0, 0, 250, 0, 'City', 'phppos_people', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 20:31:11', '2021-04-04 20:31:11', '2021-04-04 20:31:11');
INSERT INTO `page_group_field_template` VALUES (22, 2, 2, NULL, NULL, 'card', 'taxable', 'taxable', 1, 'text', 120, 'text-left', NULL, 0, 'text', 0, 1, 0, 250, 0, 'Taxable', 'phppos_customers', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 20:34:51', '2021-04-04 20:34:51', '2021-04-04 20:34:51');
INSERT INTO `page_group_field_template` VALUES (23, 4, 4, NULL, NULL, 'list', 'name', 'Name', 1, 'text', 120, 'text-left', NULL, 0, 'text', 0, 0, 0, 250, 0, 'Name', 'phppos_item', 'input', NULL, NULL, NULL, NULL, NULL, '2021-04-04 20:46:40', '2021-04-04 20:46:40', '2021-04-04 20:46:40');

-- ----------------------------
-- Table structure for permission_page
-- ----------------------------
DROP TABLE IF EXISTS `permission_page`;
CREATE TABLE `permission_page`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `permission_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `page_id` int NULL DEFAULT NULL,
  `page_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `insert_record` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `delete_record` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_record` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `execute` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `foundation` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'No',
  `department_code` varchar(155) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_deleted` tinyint NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `permission_objects_unique01`(`permission_code`, `page_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2250 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of permission_page
-- ----------------------------
INSERT INTO `permission_page` VALUES (2249, 'Admin', 1, 'customer', 'Yes', 'Yes', 'Yes', 'Yes', 'No', NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for phppos_access
-- ----------------------------
DROP TABLE IF EXISTS `phppos_access`;
CREATE TABLE `phppos_access`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `key` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `all_access` tinyint(1) NOT NULL DEFAULT 0,
  `controller` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `date_created` datetime(0) NULL DEFAULT NULL,
  `date_modified` timestamp(0) NOT NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `controller`(`controller`) USING BTREE,
  INDEX `phppos_access_key_fk`(`key`) USING BTREE,
  CONSTRAINT `phppos_access_key_fk` FOREIGN KEY (`key`) REFERENCES `phppos_keys` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_access
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_additional_item_numbers
-- ----------------------------
DROP TABLE IF EXISTS `phppos_additional_item_numbers`;
CREATE TABLE `phppos_additional_item_numbers`  (
  `item_id` int NOT NULL,
  `item_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `item_variation_id` int NULL DEFAULT NULL,
  PRIMARY KEY (`item_id`, `item_number`) USING BTREE,
  UNIQUE INDEX `item_number`(`item_number`) USING BTREE,
  INDEX `phppos_additional_item_numbers_ibfk_2`(`item_variation_id`) USING BTREE,
  CONSTRAINT `phppos_additional_item_numbers_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `phppos_items` (`item_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_additional_item_numbers_ibfk_2` FOREIGN KEY (`item_variation_id`) REFERENCES `phppos_item_variations` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_additional_item_numbers
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_app_config
-- ----------------------------
DROP TABLE IF EXISTS `phppos_app_config`;
CREATE TABLE `phppos_app_config`  (
  `key` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`key`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_app_config
-- ----------------------------
INSERT INTO `phppos_app_config` VALUES ('additional_payment_types', '');
INSERT INTO `phppos_app_config` VALUES ('always_minimize_menu', '1');
INSERT INTO `phppos_app_config` VALUES ('always_print_duplicate_receipt_all', '0');
INSERT INTO `phppos_app_config` VALUES ('always_show_item_grid', '0');
INSERT INTO `phppos_app_config` VALUES ('always_use_average_cost_method', '0');
INSERT INTO `phppos_app_config` VALUES ('announcement_special', '');
INSERT INTO `phppos_app_config` VALUES ('auto_focus_on_item_after_sale_and_receiving', '0');
INSERT INTO `phppos_app_config` VALUES ('automatically_email_receipt', '0');
INSERT INTO `phppos_app_config` VALUES ('automatically_print_duplicate_receipt_for_cc_transactions', '0');
INSERT INTO `phppos_app_config` VALUES ('automatically_show_comments_on_receipt', '0');
INSERT INTO `phppos_app_config` VALUES ('averaging_method', 'moving_average');
INSERT INTO `phppos_app_config` VALUES ('barcode_price_include_tax', '0');
INSERT INTO `phppos_app_config` VALUES ('calculate_average_cost_price_from_receivings', '0');
INSERT INTO `phppos_app_config` VALUES ('calculate_profit_for_giftcard_when', '');
INSERT INTO `phppos_app_config` VALUES ('capture_sig_for_all_payments', '0');
INSERT INTO `phppos_app_config` VALUES ('change_sale_date_for_new_sale', '0');
INSERT INTO `phppos_app_config` VALUES ('change_sale_date_when_completing_suspended_sale', '0');
INSERT INTO `phppos_app_config` VALUES ('change_sale_date_when_suspending', '0');
INSERT INTO `phppos_app_config` VALUES ('charge_tax_on_recv', '0');
INSERT INTO `phppos_app_config` VALUES ('commission_default_rate', '0');
INSERT INTO `phppos_app_config` VALUES ('commission_percent_type', 'selling_price');
INSERT INTO `phppos_app_config` VALUES ('company', 'PHP Point Of Sale, LLC');
INSERT INTO `phppos_app_config` VALUES ('confirm_error_adding_item', '0');
INSERT INTO `phppos_app_config` VALUES ('crlf', '\r\n');
INSERT INTO `phppos_app_config` VALUES ('currency_code', '');
INSERT INTO `phppos_app_config` VALUES ('currency_symbol', '$');
INSERT INTO `phppos_app_config` VALUES ('currency_symbol_location', 'before');
INSERT INTO `phppos_app_config` VALUES ('customers_store_accounts', '0');
INSERT INTO `phppos_app_config` VALUES ('date_format', 'middle_endian');
INSERT INTO `phppos_app_config` VALUES ('decimal_point', '.');
INSERT INTO `phppos_app_config` VALUES ('default_new_items_to_service', '0');
INSERT INTO `phppos_app_config` VALUES ('default_payment_type', 'Cash');
INSERT INTO `phppos_app_config` VALUES ('default_reorder_level_when_creating_items', '');
INSERT INTO `phppos_app_config` VALUES ('default_sales_person', 'logged_in_employee');
INSERT INTO `phppos_app_config` VALUES ('default_tax_1_name', 'Sales Tax');
INSERT INTO `phppos_app_config` VALUES ('default_tax_1_rate', '');
INSERT INTO `phppos_app_config` VALUES ('default_tax_2_cumulative', '0');
INSERT INTO `phppos_app_config` VALUES ('default_tax_2_name', 'Sales Tax 2');
INSERT INTO `phppos_app_config` VALUES ('default_tax_2_rate', '');
INSERT INTO `phppos_app_config` VALUES ('default_tax_3_name', '');
INSERT INTO `phppos_app_config` VALUES ('default_tax_3_rate', '');
INSERT INTO `phppos_app_config` VALUES ('default_tax_4_name', '');
INSERT INTO `phppos_app_config` VALUES ('default_tax_4_rate', '');
INSERT INTO `phppos_app_config` VALUES ('default_tax_5_name', '');
INSERT INTO `phppos_app_config` VALUES ('default_tax_5_rate', '');
INSERT INTO `phppos_app_config` VALUES ('default_tax_rate', '8');
INSERT INTO `phppos_app_config` VALUES ('default_tier_fixed_type_for_excel_import', 'fixed_amount');
INSERT INTO `phppos_app_config` VALUES ('default_tier_percent_type_for_excel_import', 'percent_off');
INSERT INTO `phppos_app_config` VALUES ('default_type_for_grid', 'categories');
INSERT INTO `phppos_app_config` VALUES ('deleted_payment_types', '');
INSERT INTO `phppos_app_config` VALUES ('disable_confirmation_sale', '0');
INSERT INTO `phppos_app_config` VALUES ('disable_giftcard_detection', '0');
INSERT INTO `phppos_app_config` VALUES ('disable_price_rules_dialog', '0');
INSERT INTO `phppos_app_config` VALUES ('disable_quick_complete_sale', '0');
INSERT INTO `phppos_app_config` VALUES ('disable_sale_notifications', '0');
INSERT INTO `phppos_app_config` VALUES ('disable_store_account_when_over_credit_limit', '0');
INSERT INTO `phppos_app_config` VALUES ('disable_test_mode', '0');
INSERT INTO `phppos_app_config` VALUES ('discount_percent_earned', '0');
INSERT INTO `phppos_app_config` VALUES ('do_not_allow_below_cost', '0');
INSERT INTO `phppos_app_config` VALUES ('do_not_allow_out_of_stock_items_to_be_sold', '0');
INSERT INTO `phppos_app_config` VALUES ('do_not_force_http', '0');
INSERT INTO `phppos_app_config` VALUES ('do_not_group_same_items', '0');
INSERT INTO `phppos_app_config` VALUES ('do_not_show_closing', '0');
INSERT INTO `phppos_app_config` VALUES ('do_not_tax_service_items_for_deliveries', '0');
INSERT INTO `phppos_app_config` VALUES ('ecom_store_location', '1');
INSERT INTO `phppos_app_config` VALUES ('ecommerce_cron_sync_operations', 'a:13:{i:0;s:22:\"sync_inventory_changes\";i:1;s:33:\"import_ecommerce_tags_into_phppos\";i:2;s:39:\"import_ecommerce_categories_into_phppos\";i:3;s:39:\"import_ecommerce_attributes_into_phppos\";i:4;s:30:\"import_tax_classes_into_phppos\";i:5;s:35:\"import_shipping_classes_into_phppos\";i:6;s:34:\"import_ecommerce_items_into_phppos\";i:7;s:35:\"import_ecommerce_orders_into_phppos\";i:8;s:31:\"export_phppos_tags_to_ecommerce\";i:9;s:37:\"export_phppos_categories_to_ecommerce\";i:10;s:37:\"export_phppos_attributes_to_ecommerce\";i:11;s:30:\"export_tax_classes_into_phppos\";i:12;s:32:\"export_phppos_items_to_ecommerce\";}');
INSERT INTO `phppos_app_config` VALUES ('ecommerce_platform', '');
INSERT INTO `phppos_app_config` VALUES ('edit_item_price_if_zero_after_adding', '0');
INSERT INTO `phppos_app_config` VALUES ('email_charset', '');
INSERT INTO `phppos_app_config` VALUES ('email_provider', 'Use System Default');
INSERT INTO `phppos_app_config` VALUES ('emailed_receipt_subject', '');
INSERT INTO `phppos_app_config` VALUES ('enable_customer_loyalty_system', '0');
INSERT INTO `phppos_app_config` VALUES ('enable_ebt_payments', '0');
INSERT INTO `phppos_app_config` VALUES ('enable_markup_calculator', '0');
INSERT INTO `phppos_app_config` VALUES ('enable_quick_edit', '0');
INSERT INTO `phppos_app_config` VALUES ('enable_scale', '0');
INSERT INTO `phppos_app_config` VALUES ('enable_sounds', '0');
INSERT INTO `phppos_app_config` VALUES ('enable_wic', '0');
INSERT INTO `phppos_app_config` VALUES ('enhanced_search_method', '0');
INSERT INTO `phppos_app_config` VALUES ('fast_user_switching', '0');
INSERT INTO `phppos_app_config` VALUES ('force_https', '0');
INSERT INTO `phppos_app_config` VALUES ('group_all_taxes_on_receipt', '0');
INSERT INTO `phppos_app_config` VALUES ('hide_barcode_on_sales_and_recv_receipt', '0');
INSERT INTO `phppos_app_config` VALUES ('hide_customer_recent_sales', '0');
INSERT INTO `phppos_app_config` VALUES ('hide_desc_on_receipt', '0');
INSERT INTO `phppos_app_config` VALUES ('hide_layaways_sales_in_reports', '0');
INSERT INTO `phppos_app_config` VALUES ('hide_name_on_barcodes', '0');
INSERT INTO `phppos_app_config` VALUES ('hide_out_of_stock_grid', '0');
INSERT INTO `phppos_app_config` VALUES ('hide_points_on_receipt', '0');
INSERT INTO `phppos_app_config` VALUES ('hide_price_on_barcodes', '0');
INSERT INTO `phppos_app_config` VALUES ('hide_sales_to_discount_on_receipt', '0');
INSERT INTO `phppos_app_config` VALUES ('hide_signature', '0');
INSERT INTO `phppos_app_config` VALUES ('hide_size_field', '1');
INSERT INTO `phppos_app_config` VALUES ('hide_store_account_balance_on_receipt', '0');
INSERT INTO `phppos_app_config` VALUES ('hide_store_account_payments_from_report_totals', '0');
INSERT INTO `phppos_app_config` VALUES ('hide_store_account_payments_in_reports', '0');
INSERT INTO `phppos_app_config` VALUES ('hide_suspended_recv_in_reports', '0');
INSERT INTO `phppos_app_config` VALUES ('hide_test_mode_home', '0');
INSERT INTO `phppos_app_config` VALUES ('highlight_low_inventory_items_in_items_module', '0');
INSERT INTO `phppos_app_config` VALUES ('id_to_show_on_barcode', 'id');
INSERT INTO `phppos_app_config` VALUES ('id_to_show_on_sale_interface', 'number');
INSERT INTO `phppos_app_config` VALUES ('include_child_categories_when_searching_or_reporting', '0');
INSERT INTO `phppos_app_config` VALUES ('indicate_taxable_on_receipt', '0');
INSERT INTO `phppos_app_config` VALUES ('item_id_auto_increment', '1');
INSERT INTO `phppos_app_config` VALUES ('item_kit_id_auto_increment', '1');
INSERT INTO `phppos_app_config` VALUES ('item_lookup_order', 'a:6:{i:0;s:7:\"item_id\";i:1;s:11:\"item_number\";i:2;s:10:\"product_id\";i:3;s:23:\"additional_item_numbers\";i:4;s:14:\"serial_numbers\";i:5;s:26:\"item_variation_item_number\";}');
INSERT INTO `phppos_app_config` VALUES ('language', 'english');
INSERT INTO `phppos_app_config` VALUES ('legacy_detailed_report_export', '0');
INSERT INTO `phppos_app_config` VALUES ('limit_manual_price_adj', '0');
INSERT INTO `phppos_app_config` VALUES ('lock_prices_suspended_sales', '0');
INSERT INTO `phppos_app_config` VALUES ('logout_on_clock_out', '0');
INSERT INTO `phppos_app_config` VALUES ('loyalty_option', 'simple');
INSERT INTO `phppos_app_config` VALUES ('loyalty_points_without_tax', '0');
INSERT INTO `phppos_app_config` VALUES ('mailing_labels_type', 'pdf');
INSERT INTO `phppos_app_config` VALUES ('new_items_are_ecommerce_by_default', '1');
INSERT INTO `phppos_app_config` VALUES ('newline', '\r\n');
INSERT INTO `phppos_app_config` VALUES ('number_of_decimals', '');
INSERT INTO `phppos_app_config` VALUES ('number_of_decimals_for_quantity_on_receipt', '');
INSERT INTO `phppos_app_config` VALUES ('number_of_items_in_grid', '14');
INSERT INTO `phppos_app_config` VALUES ('number_of_items_per_page', '20');
INSERT INTO `phppos_app_config` VALUES ('number_of_recent_sales', '10');
INSERT INTO `phppos_app_config` VALUES ('number_of_sales_for_discount', '');
INSERT INTO `phppos_app_config` VALUES ('online_price_tier', '0');
INSERT INTO `phppos_app_config` VALUES ('override_receipt_title', '');
INSERT INTO `phppos_app_config` VALUES ('override_tier_name', '');
INSERT INTO `phppos_app_config` VALUES ('overwrite_existing_items_on_excel_import', '0');
INSERT INTO `phppos_app_config` VALUES ('past_inventory_date', '2021-04-02');
INSERT INTO `phppos_app_config` VALUES ('paypal_me', '');
INSERT INTO `phppos_app_config` VALUES ('phppos_session_expiration', '0');
INSERT INTO `phppos_app_config` VALUES ('point_value', '');
INSERT INTO `phppos_app_config` VALUES ('prices_include_tax', '0');
INSERT INTO `phppos_app_config` VALUES ('print_after_receiving', '0');
INSERT INTO `phppos_app_config` VALUES ('print_after_sale', '0');
INSERT INTO `phppos_app_config` VALUES ('prompt_for_ccv_swipe', '0');
INSERT INTO `phppos_app_config` VALUES ('prompt_to_use_points', '0');
INSERT INTO `phppos_app_config` VALUES ('protocol', '');
INSERT INTO `phppos_app_config` VALUES ('qb_sync_operations', 'a:1:{i:0;s:33:\"export_journalentry_to_quickbooks\";}');
INSERT INTO `phppos_app_config` VALUES ('receipt_text_size', 'small');
INSERT INTO `phppos_app_config` VALUES ('receiving_id_auto_increment', '1');
INSERT INTO `phppos_app_config` VALUES ('redirect_to_sale_or_recv_screen_after_printing_receipt', '0');
INSERT INTO `phppos_app_config` VALUES ('remove_commission_from_profit_in_reports', '0');
INSERT INTO `phppos_app_config` VALUES ('remove_customer_company_from_receipt', '0');
INSERT INTO `phppos_app_config` VALUES ('remove_customer_contact_info_from_receipt', '0');
INSERT INTO `phppos_app_config` VALUES ('remove_customer_name_from_receipt', '0');
INSERT INTO `phppos_app_config` VALUES ('remove_employee_from_receipt', '0');
INSERT INTO `phppos_app_config` VALUES ('remove_points_from_profit', '0');
INSERT INTO `phppos_app_config` VALUES ('report_sort_order', 'asc');
INSERT INTO `phppos_app_config` VALUES ('require_customer_for_sale', '0');
INSERT INTO `phppos_app_config` VALUES ('require_customer_for_suspended_sale', '0');
INSERT INTO `phppos_app_config` VALUES ('require_employee_login_before_each_sale', '0');
INSERT INTO `phppos_app_config` VALUES ('reset_location_when_switching_employee', '0');
INSERT INTO `phppos_app_config` VALUES ('return_policy', 'Change return policy');
INSERT INTO `phppos_app_config` VALUES ('round_cash_on_sales', '0');
INSERT INTO `phppos_app_config` VALUES ('round_tier_prices_to_2_decimals', '0');
INSERT INTO `phppos_app_config` VALUES ('sale_id_auto_increment', '1');
INSERT INTO `phppos_app_config` VALUES ('sale_prefix', 'POS');
INSERT INTO `phppos_app_config` VALUES ('scale_divide_by', '100');
INSERT INTO `phppos_app_config` VALUES ('scale_format', 'scale_1');
INSERT INTO `phppos_app_config` VALUES ('select_sales_person_during_sale', '0');
INSERT INTO `phppos_app_config` VALUES ('show_barcode_company_name', '1');
INSERT INTO `phppos_app_config` VALUES ('show_clock_on_header', '0');
INSERT INTO `phppos_app_config` VALUES ('show_item_id_on_receipt', '0');
INSERT INTO `phppos_app_config` VALUES ('show_language_switcher', '0');
INSERT INTO `phppos_app_config` VALUES ('show_orig_price_if_marked_down_on_receipt', '0');
INSERT INTO `phppos_app_config` VALUES ('show_receipt_after_suspending_sale', '0');
INSERT INTO `phppos_app_config` VALUES ('shown_setup_wizard', '0');
INSERT INTO `phppos_app_config` VALUES ('smtp_crypto', '');
INSERT INTO `phppos_app_config` VALUES ('smtp_host', '');
INSERT INTO `phppos_app_config` VALUES ('smtp_pass', '');
INSERT INTO `phppos_app_config` VALUES ('smtp_port', '');
INSERT INTO `phppos_app_config` VALUES ('smtp_timeout', '');
INSERT INTO `phppos_app_config` VALUES ('smtp_user', '');
INSERT INTO `phppos_app_config` VALUES ('speed_up_search_queries', '0');
INSERT INTO `phppos_app_config` VALUES ('spend_to_point_ratio', '');
INSERT INTO `phppos_app_config` VALUES ('spreadsheet_format', 'XLSX');
INSERT INTO `phppos_app_config` VALUES ('store_account_statement_message', '');
INSERT INTO `phppos_app_config` VALUES ('store_closing_time', '');
INSERT INTO `phppos_app_config` VALUES ('store_opening_time', '');
INSERT INTO `phppos_app_config` VALUES ('suppliers_store_accounts', '0');
INSERT INTO `phppos_app_config` VALUES ('supports_full_text', '1');
INSERT INTO `phppos_app_config` VALUES ('tax_class_id', '1');
INSERT INTO `phppos_app_config` VALUES ('test_mode', '0');
INSERT INTO `phppos_app_config` VALUES ('thousands_separator', ',');
INSERT INTO `phppos_app_config` VALUES ('time_format', '12_hour');
INSERT INTO `phppos_app_config` VALUES ('timeclock', '0');
INSERT INTO `phppos_app_config` VALUES ('track_cash', '0');
INSERT INTO `phppos_app_config` VALUES ('user_configured_layaway_name', '');
INSERT INTO `phppos_app_config` VALUES ('version', '17.8');
INSERT INTO `phppos_app_config` VALUES ('virtual_keyboard', '');
INSERT INTO `phppos_app_config` VALUES ('website', '');
INSERT INTO `phppos_app_config` VALUES ('wide_printer_receipt_format', '0');
INSERT INTO `phppos_app_config` VALUES ('woo_api_key', '');
INSERT INTO `phppos_app_config` VALUES ('woo_api_secret', '');
INSERT INTO `phppos_app_config` VALUES ('woo_api_url', '');
INSERT INTO `phppos_app_config` VALUES ('woo_sku_sync_field', 'item_number');
INSERT INTO `phppos_app_config` VALUES ('woo_version', '3.0.0');

-- ----------------------------
-- Table structure for phppos_app_files
-- ----------------------------
DROP TABLE IF EXISTS `phppos_app_files`;
CREATE TABLE `phppos_app_files`  (
  `file_id` int NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `file_data` longblob NOT NULL,
  `timestamp` timestamp(0) NOT NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `expires` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`file_id`) USING BTREE,
  INDEX `expires`(`expires`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_app_files
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_appointment_types
-- ----------------------------
DROP TABLE IF EXISTS `phppos_appointment_types`;
CREATE TABLE `phppos_appointment_types`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_appointment_types
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_appointments
-- ----------------------------
DROP TABLE IF EXISTS `phppos_appointments`;
CREATE TABLE `phppos_appointments`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `location_id` int NOT NULL,
  `person_id` int NULL DEFAULT NULL,
  `employee_id` int NULL DEFAULT NULL,
  `start_time` timestamp(0) NULL DEFAULT NULL,
  `end_time` timestamp(0) NULL DEFAULT NULL,
  `appointments_type_id` int NOT NULL,
  `notes` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_appointments_ibfk_1`(`appointments_type_id`) USING BTREE,
  INDEX `phppos_appointments_ibfk_2`(`person_id`) USING BTREE,
  INDEX `phppos_appointments_ibfk_3`(`location_id`) USING BTREE,
  INDEX `phppos_appointments_ibfk_4`(`employee_id`) USING BTREE,
  CONSTRAINT `phppos_appointments_ibfk_1` FOREIGN KEY (`appointments_type_id`) REFERENCES `phppos_appointment_types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_appointments_ibfk_2` FOREIGN KEY (`person_id`) REFERENCES `phppos_people` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_appointments_ibfk_3` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_appointments_ibfk_4` FOREIGN KEY (`employee_id`) REFERENCES `phppos_employees` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_appointments
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_attribute_values
-- ----------------------------
DROP TABLE IF EXISTS `phppos_attribute_values`;
CREATE TABLE `phppos_attribute_values`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `ecommerce_attribute_term_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `attribute_id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int NOT NULL DEFAULT 0,
  `last_modified` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name_attribute_id`(`name`, `attribute_id`) USING BTREE,
  INDEX `phppos_attribute_values_ibfk_1`(`attribute_id`) USING BTREE,
  CONSTRAINT `phppos_attribute_values_ibfk_1` FOREIGN KEY (`attribute_id`) REFERENCES `phppos_attributes` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_attribute_values
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_attributes
-- ----------------------------
DROP TABLE IF EXISTS `phppos_attributes`;
CREATE TABLE `phppos_attributes`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `item_id` int NULL DEFAULT NULL,
  `ecommerce_attribute_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int NOT NULL DEFAULT 0,
  `last_modified` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`item_id`, `name`) USING BTREE,
  CONSTRAINT `phppos_attributes_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `phppos_items` (`item_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_attributes
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_categories
-- ----------------------------
DROP TABLE IF EXISTS `phppos_categories`;
CREATE TABLE `phppos_categories`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `ecommerce_category_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `last_modified` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  `deleted` int NOT NULL DEFAULT 0,
  `hide_from_grid` int NOT NULL DEFAULT 0,
  `parent_id` int NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `image_id` int NULL DEFAULT NULL,
  `color` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `system_category` int NULL DEFAULT 0,
  `exclude_from_e_commerce` int NOT NULL DEFAULT 0,
  `category_info_popup` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `deleted`(`deleted`) USING BTREE,
  INDEX `phppos_categories_ibfk_1`(`parent_id`) USING BTREE,
  INDEX `phppos_categories_ibfk_2`(`image_id`) USING BTREE,
  INDEX `name`(`name`) USING BTREE,
  CONSTRAINT `phppos_categories_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `phppos_categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_categories_ibfk_2` FOREIGN KEY (`image_id`) REFERENCES `phppos_app_files` (`file_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_categories
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_currency_exchange_rates
-- ----------------------------
DROP TABLE IF EXISTS `phppos_currency_exchange_rates`;
CREATE TABLE `phppos_currency_exchange_rates`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `currency_code_to` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `currency_symbol` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `exchange_rate` decimal(23, 10) NOT NULL,
  `currency_symbol_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `number_of_decimals` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `thousands_separator` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `decimal_point` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_currency_exchange_rates
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_customers
-- ----------------------------
DROP TABLE IF EXISTS `phppos_customers`;
CREATE TABLE `phppos_customers`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `person_id` int NOT NULL,
  `account_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `override_default_tax` int NOT NULL DEFAULT 0,
  `company_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `balance` decimal(23, 10) NOT NULL DEFAULT 0,
  `credit_limit` decimal(23, 10) NULL DEFAULT NULL,
  `points` decimal(23, 10) NOT NULL DEFAULT 0,
  `disable_loyalty` int NOT NULL DEFAULT 0,
  `current_spend_for_points` decimal(23, 10) NOT NULL DEFAULT 0,
  `current_sales_for_discount` int NOT NULL DEFAULT 0,
  `taxable` int NOT NULL DEFAULT 1,
  `tax_certificate` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cc_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `cc_expire` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `cc_ref_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `cc_preview` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `card_issuer` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  `tier_id` int NULL DEFAULT NULL,
  `deleted` int NOT NULL DEFAULT 0,
  `tax_class_id` int NULL DEFAULT NULL,
  `custom_field_1_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_2_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_3_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_4_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_5_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_6_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_7_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_8_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_9_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_10_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `location_id` int NULL DEFAULT NULL,
  `internal_notes` varchar(0) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `customer_info_popup` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `auto_email_receipt` int NOT NULL DEFAULT 0,
  `always_sms_receipt` int NOT NULL DEFAULT 0,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `account_number`(`account_number`) USING BTREE,
  INDEX `person_id`(`person_id`) USING BTREE,
  INDEX `deleted`(`deleted`) USING BTREE,
  INDEX `cc_token`(`cc_token`) USING BTREE,
  INDEX `phppos_customers_ibfk_2`(`tier_id`) USING BTREE,
  INDEX `company_name`(`company_name`) USING BTREE,
  INDEX `phppos_customers_ibfk_3`(`tax_class_id`) USING BTREE,
  INDEX `custom_field_1_value`(`custom_field_1_value`) USING BTREE,
  INDEX `custom_field_2_value`(`custom_field_2_value`) USING BTREE,
  INDEX `custom_field_3_value`(`custom_field_3_value`) USING BTREE,
  INDEX `custom_field_4_value`(`custom_field_4_value`) USING BTREE,
  INDEX `custom_field_5_value`(`custom_field_5_value`) USING BTREE,
  INDEX `custom_field_6_value`(`custom_field_6_value`) USING BTREE,
  INDEX `custom_field_7_value`(`custom_field_7_value`) USING BTREE,
  INDEX `custom_field_8_value`(`custom_field_8_value`) USING BTREE,
  INDEX `custom_field_9_value`(`custom_field_9_value`) USING BTREE,
  INDEX `custom_field_10_value`(`custom_field_10_value`) USING BTREE,
  INDEX `phppos_customers_ibfk_4`(`location_id`) USING BTREE,
  CONSTRAINT `phppos_customers_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `phppos_people` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_customers_ibfk_2` FOREIGN KEY (`tier_id`) REFERENCES `phppos_price_tiers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_customers_ibfk_3` FOREIGN KEY (`tax_class_id`) REFERENCES `phppos_tax_classes` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_customers_ibfk_4` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_customers
-- ----------------------------
INSERT INTO `phppos_customers` VALUES (16, 17, '0014', 0, 'asdasd', 0.0000000000, NULL, 0.0000000000, 0, 0.0000000000, 0, 1, '', NULL, NULL, NULL, NULL, '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2021-04-04 10:19:29', '2021-04-04 10:19:29');
INSERT INTO `phppos_customers` VALUES (17, 18, '12313', 0, 'asdad', 0.0000000000, NULL, 0.0000000000, 0, 0.0000000000, 0, 1, '', NULL, NULL, NULL, NULL, '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2021-04-04 10:23:40', '2021-04-04 10:23:40');
INSERT INTO `phppos_customers` VALUES (18, 19, '4234', 0, 'sdfs', 0.0000000000, NULL, 0.0000000000, 0, 0.0000000000, 0, 1, '', NULL, NULL, NULL, NULL, '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2021-04-04 10:24:04', '2021-04-04 10:24:04');

-- ----------------------------
-- Table structure for phppos_customers_series
-- ----------------------------
DROP TABLE IF EXISTS `phppos_customers_series`;
CREATE TABLE `phppos_customers_series`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `sale_id` int NOT NULL,
  `item_id` int NOT NULL DEFAULT 0,
  `expire_date` date NULL DEFAULT NULL,
  `quantity_remaining` decimal(23, 10) NULL DEFAULT 0,
  `customer_id` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_customers_series_ibfk_1`(`item_id`) USING BTREE,
  INDEX `phppos_customers_series_ibfk_2`(`customer_id`) USING BTREE,
  INDEX `phppos_customers_series_ibfk_3`(`sale_id`) USING BTREE,
  CONSTRAINT `phppos_customers_series_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `phppos_items` (`item_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_customers_series_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `phppos_people` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_customers_series_ibfk_3` FOREIGN KEY (`sale_id`) REFERENCES `phppos_sales` (`sale_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_customers_series
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_customers_series_log
-- ----------------------------
DROP TABLE IF EXISTS `phppos_customers_series_log`;
CREATE TABLE `phppos_customers_series_log`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `series_id` int NOT NULL,
  `date` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  `quantity_used` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_customers_series_log_ibfk_1`(`series_id`) USING BTREE,
  CONSTRAINT `phppos_customers_series_log_ibfk_1` FOREIGN KEY (`series_id`) REFERENCES `phppos_customers_series` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_customers_series_log
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_customers_taxes
-- ----------------------------
DROP TABLE IF EXISTS `phppos_customers_taxes`;
CREATE TABLE `phppos_customers_taxes`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `customer_id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15, 3) NOT NULL,
  `cumulative` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unique_tax`(`customer_id`, `name`, `percent`) USING BTREE,
  CONSTRAINT `phppos_customers_taxes_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `phppos_customers` (`person_id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_customers_taxes
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_damaged_items_log
-- ----------------------------
DROP TABLE IF EXISTS `phppos_damaged_items_log`;
CREATE TABLE `phppos_damaged_items_log`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `damaged_date` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  `damaged_qty` decimal(23, 10) NOT NULL DEFAULT 0,
  `item_id` int NOT NULL,
  `item_variation_id` int NULL DEFAULT NULL,
  `sale_id` int NULL DEFAULT NULL,
  `location_id` int NOT NULL,
  `damaged_reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_damaged_items_log_ibfk_1`(`item_id`) USING BTREE,
  INDEX `phppos_damaged_items_log_ibfk_2`(`item_variation_id`) USING BTREE,
  INDEX `phppos_damaged_items_log_ibfk_3`(`sale_id`) USING BTREE,
  INDEX `phppos_damaged_items_log_ibfk_4`(`location_id`) USING BTREE,
  CONSTRAINT `phppos_damaged_items_log_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `phppos_items` (`item_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_damaged_items_log_ibfk_2` FOREIGN KEY (`item_variation_id`) REFERENCES `phppos_item_variations` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_damaged_items_log_ibfk_3` FOREIGN KEY (`sale_id`) REFERENCES `phppos_sales` (`sale_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_damaged_items_log_ibfk_4` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_damaged_items_log
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_ecommerce_locations
-- ----------------------------
DROP TABLE IF EXISTS `phppos_ecommerce_locations`;
CREATE TABLE `phppos_ecommerce_locations`  (
  `location_id` int NOT NULL,
  PRIMARY KEY (`location_id`) USING BTREE,
  CONSTRAINT `phppos_ecommerce_locations_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_ecommerce_locations
-- ----------------------------
INSERT INTO `phppos_ecommerce_locations` VALUES (1);

-- ----------------------------
-- Table structure for phppos_employee_registers
-- ----------------------------
DROP TABLE IF EXISTS `phppos_employee_registers`;
CREATE TABLE `phppos_employee_registers`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `employee_id` int NOT NULL,
  `register_id` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_employee_registers_ibfk_1`(`employee_id`) USING BTREE,
  INDEX `phppos_employee_registers_ibfk_2`(`register_id`) USING BTREE,
  CONSTRAINT `phppos_employee_registers_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `phppos_employees` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_employee_registers_ibfk_2` FOREIGN KEY (`register_id`) REFERENCES `phppos_registers` (`register_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_employee_registers
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_employees
-- ----------------------------
DROP TABLE IF EXISTS `phppos_employees`;
CREATE TABLE `phppos_employees`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `force_password_change` int NOT NULL DEFAULT 0,
  `always_require_password` int NOT NULL DEFAULT 0,
  `person_id` int NOT NULL,
  `language` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `commission_percent` decimal(23, 10) NULL DEFAULT 0,
  `commission_percent_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  `hourly_pay_rate` decimal(23, 10) NOT NULL DEFAULT 0,
  `not_required_to_clock_in` int NOT NULL DEFAULT 0,
  `inactive` int NOT NULL DEFAULT 0,
  `reason_inactive` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `hire_date` date NULL DEFAULT NULL,
  `employee_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `birthday` date NULL DEFAULT NULL,
  `termination_date` date NULL DEFAULT NULL,
  `deleted` int NOT NULL DEFAULT 0,
  `custom_field_1_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_2_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_3_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_4_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_5_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_6_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_7_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_8_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_9_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_10_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `max_discount_percent` decimal(15, 3) NULL DEFAULT NULL,
  `login_start_time` time(0) NULL DEFAULT NULL,
  `login_end_time` time(0) NULL DEFAULT NULL,
  `dark_mode` int NOT NULL DEFAULT 0,
  `template_id` int NULL DEFAULT NULL,
  `override_price_adjustments` int NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE,
  UNIQUE INDEX `employee_number`(`employee_number`) USING BTREE,
  INDEX `person_id`(`person_id`) USING BTREE,
  INDEX `deleted`(`deleted`) USING BTREE,
  INDEX `custom_field_1_value`(`custom_field_1_value`) USING BTREE,
  INDEX `custom_field_2_value`(`custom_field_2_value`) USING BTREE,
  INDEX `custom_field_3_value`(`custom_field_3_value`) USING BTREE,
  INDEX `custom_field_4_value`(`custom_field_4_value`) USING BTREE,
  INDEX `custom_field_5_value`(`custom_field_5_value`) USING BTREE,
  INDEX `custom_field_6_value`(`custom_field_6_value`) USING BTREE,
  INDEX `custom_field_7_value`(`custom_field_7_value`) USING BTREE,
  INDEX `custom_field_8_value`(`custom_field_8_value`) USING BTREE,
  INDEX `custom_field_9_value`(`custom_field_9_value`) USING BTREE,
  INDEX `custom_field_10_value`(`custom_field_10_value`) USING BTREE,
  INDEX `template_id`(`template_id`) USING BTREE,
  CONSTRAINT `phppos_employees_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `phppos_people` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_employees
-- ----------------------------
INSERT INTO `phppos_employees` VALUES (1, 'admin', '439a6de57d475c1a0ba9bcb1c39f0af6', 0, 0, 1, NULL, 0.0000000000, '', 0.0000000000, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);

-- ----------------------------
-- Table structure for phppos_employees_app_config
-- ----------------------------
DROP TABLE IF EXISTS `phppos_employees_app_config`;
CREATE TABLE `phppos_employees_app_config`  (
  `employee_id` int NOT NULL,
  `key` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`employee_id`, `key`) USING BTREE,
  CONSTRAINT `phppos_employees_app_config_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `phppos_employees` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_employees_app_config
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_employees_locations
-- ----------------------------
DROP TABLE IF EXISTS `phppos_employees_locations`;
CREATE TABLE `phppos_employees_locations`  (
  `employee_id` int NOT NULL,
  `location_id` int NOT NULL,
  PRIMARY KEY (`employee_id`, `location_id`) USING BTREE,
  INDEX `phppos_employees_locations_ibfk_2`(`location_id`) USING BTREE,
  CONSTRAINT `phppos_employees_locations_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `phppos_employees` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_employees_locations_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_employees_locations
-- ----------------------------
INSERT INTO `phppos_employees_locations` VALUES (1, 1);

-- ----------------------------
-- Table structure for phppos_employees_reset_password
-- ----------------------------
DROP TABLE IF EXISTS `phppos_employees_reset_password`;
CREATE TABLE `phppos_employees_reset_password`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `key` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `employee_id` int NOT NULL,
  `expire` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_employees_reset_password_ibfk_1`(`employee_id`) USING BTREE,
  CONSTRAINT `phppos_employees_reset_password_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `phppos_employees` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_employees_reset_password
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_employees_time_clock
-- ----------------------------
DROP TABLE IF EXISTS `phppos_employees_time_clock`;
CREATE TABLE `phppos_employees_time_clock`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `employee_id` int NOT NULL,
  `location_id` int NOT NULL,
  `clock_in` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00',
  `clock_out` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00',
  `clock_in_comment` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `clock_out_comment` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `hourly_pay_rate` decimal(23, 10) NOT NULL DEFAULT 0,
  `ip_address_clock_in` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ip_address_clock_out` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_employees_time_clock_ibfk_1`(`employee_id`) USING BTREE,
  INDEX `phppos_employees_time_clock_ibfk_2`(`location_id`) USING BTREE,
  CONSTRAINT `phppos_employees_time_clock_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `phppos_employees` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_employees_time_clock_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_employees_time_clock
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_employees_time_off
-- ----------------------------
DROP TABLE IF EXISTS `phppos_employees_time_off`;
CREATE TABLE `phppos_employees_time_off`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `approved` int NOT NULL DEFAULT 0,
  `start_day` date NULL DEFAULT NULL,
  `end_day` date NULL DEFAULT NULL,
  `hours_requested` decimal(23, 10) NULL DEFAULT 0,
  `is_paid` int NOT NULL DEFAULT 0,
  `reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `employee_requested_person_id` int NULL DEFAULT NULL,
  `employee_requested_location_id` int NULL DEFAULT NULL,
  `employee_approved_person_id` int NULL DEFAULT NULL,
  `deleted` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_employees_time_off_ibfk_1`(`employee_requested_person_id`) USING BTREE,
  INDEX `phppos_employees_time_off_ibfk_2`(`employee_approved_person_id`) USING BTREE,
  INDEX `phppos_employees_time_off_ibfk_3`(`employee_requested_location_id`) USING BTREE,
  CONSTRAINT `phppos_employees_time_off_ibfk_1` FOREIGN KEY (`employee_requested_person_id`) REFERENCES `phppos_people` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_employees_time_off_ibfk_2` FOREIGN KEY (`employee_approved_person_id`) REFERENCES `phppos_people` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_employees_time_off_ibfk_3` FOREIGN KEY (`employee_requested_location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_employees_time_off
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_expenses
-- ----------------------------
DROP TABLE IF EXISTS `phppos_expenses`;
CREATE TABLE `phppos_expenses`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `location_id` int NOT NULL,
  `category_id` int NULL DEFAULT NULL,
  `expense_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `expense_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `expense_reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `expense_date` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  `expense_amount` decimal(23, 10) NOT NULL,
  `expense_tax` decimal(23, 10) NOT NULL,
  `expense_note` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `employee_id` int NOT NULL,
  `approved_employee_id` int NULL DEFAULT NULL,
  `deleted` int NOT NULL DEFAULT 0,
  `expense_payment_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `location_id`(`location_id`) USING BTREE,
  INDEX `employee_id`(`employee_id`) USING BTREE,
  INDEX `approved_employee_id`(`approved_employee_id`) USING BTREE,
  INDEX `category_id`(`category_id`) USING BTREE,
  INDEX `deleted`(`deleted`) USING BTREE,
  INDEX `expense_type`(`expense_type`) USING BTREE,
  INDEX `expense_date`(`expense_date`) USING BTREE,
  INDEX `expense_amount`(`expense_amount`) USING BTREE,
  INDEX `expense_description`(`expense_description`(255)) USING BTREE,
  INDEX `expense_reason`(`expense_reason`) USING BTREE,
  INDEX `expense_note`(`expense_note`(255)) USING BTREE,
  CONSTRAINT `phppos_expenses_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_expenses_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `phppos_employees` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_expenses_ibfk_3` FOREIGN KEY (`category_id`) REFERENCES `phppos_categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_expenses_ibfk_4` FOREIGN KEY (`approved_employee_id`) REFERENCES `phppos_employees` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_expenses
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_giftcards
-- ----------------------------
DROP TABLE IF EXISTS `phppos_giftcards`;
CREATE TABLE `phppos_giftcards`  (
  `giftcard_id` int NOT NULL AUTO_INCREMENT,
  `giftcard_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `value` decimal(23, 10) NOT NULL,
  `customer_id` int NULL DEFAULT NULL,
  `inactive` int NOT NULL DEFAULT 0,
  `deleted` int NOT NULL DEFAULT 0,
  `integrated_gift_card` int NOT NULL DEFAULT 0,
  `integrated_auth_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`giftcard_id`) USING BTREE,
  UNIQUE INDEX `giftcard_number`(`giftcard_number`) USING BTREE,
  INDEX `deleted`(`deleted`) USING BTREE,
  INDEX `phppos_giftcards_ibfk_1`(`customer_id`) USING BTREE,
  INDEX `description`(`description`(255)) USING BTREE,
  CONSTRAINT `phppos_giftcards_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `phppos_customers` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_giftcards
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_giftcards_log
-- ----------------------------
DROP TABLE IF EXISTS `phppos_giftcards_log`;
CREATE TABLE `phppos_giftcards_log`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `log_date` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  `giftcard_id` int NOT NULL,
  `transaction_amount` decimal(23, 10) NOT NULL,
  `log_message` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_giftcards_log_ibfk_1`(`giftcard_id`) USING BTREE,
  CONSTRAINT `phppos_giftcards_log_ibfk_1` FOREIGN KEY (`giftcard_id`) REFERENCES `phppos_giftcards` (`giftcard_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_giftcards_log
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_grid_hidden_categories
-- ----------------------------
DROP TABLE IF EXISTS `phppos_grid_hidden_categories`;
CREATE TABLE `phppos_grid_hidden_categories`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `category_id` int NOT NULL,
  `location_id` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unique_grid`(`category_id`, `location_id`) USING BTREE,
  INDEX `phppos_grid_hidden_categories_ibfk_2`(`location_id`) USING BTREE,
  CONSTRAINT `phppos_grid_hidden_categories_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `phppos_categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_grid_hidden_categories_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_grid_hidden_categories
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_grid_hidden_item_kits
-- ----------------------------
DROP TABLE IF EXISTS `phppos_grid_hidden_item_kits`;
CREATE TABLE `phppos_grid_hidden_item_kits`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `item_kit_id` int NOT NULL,
  `location_id` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unique_grid`(`item_kit_id`, `location_id`) USING BTREE,
  INDEX `phppos_grid_hidden_item_kits_ibfk_2`(`location_id`) USING BTREE,
  CONSTRAINT `phppos_grid_hidden_item_kits_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `phppos_item_kits` (`item_kit_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_grid_hidden_item_kits_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_grid_hidden_item_kits
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_grid_hidden_items
-- ----------------------------
DROP TABLE IF EXISTS `phppos_grid_hidden_items`;
CREATE TABLE `phppos_grid_hidden_items`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `item_id` int NOT NULL,
  `location_id` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unique_grid`(`item_id`, `location_id`) USING BTREE,
  INDEX `phppos_grid_hidden_items_ibfk_2`(`location_id`) USING BTREE,
  CONSTRAINT `phppos_grid_hidden_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `phppos_items` (`item_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_grid_hidden_items_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_grid_hidden_items
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_grid_hidden_tags
-- ----------------------------
DROP TABLE IF EXISTS `phppos_grid_hidden_tags`;
CREATE TABLE `phppos_grid_hidden_tags`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `tag_id` int NOT NULL,
  `location_id` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unique_grid`(`tag_id`, `location_id`) USING BTREE,
  INDEX `phppos_grid_hidden_tags_ibfk_2`(`location_id`) USING BTREE,
  CONSTRAINT `phppos_grid_hidden_tags_ibfk_1` FOREIGN KEY (`tag_id`) REFERENCES `phppos_tags` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_grid_hidden_tags_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_grid_hidden_tags
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_inventory
-- ----------------------------
DROP TABLE IF EXISTS `phppos_inventory`;
CREATE TABLE `phppos_inventory`  (
  `trans_id` int NOT NULL AUTO_INCREMENT,
  `trans_items` int NOT NULL DEFAULT 0,
  `item_variation_id` int NULL DEFAULT NULL,
  `trans_user` int NOT NULL DEFAULT 0,
  `trans_date` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  `trans_comment` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `trans_inventory` decimal(23, 10) NOT NULL DEFAULT 0,
  `location_id` int NOT NULL,
  `trans_current_quantity` decimal(23, 10) NULL DEFAULT NULL,
  PRIMARY KEY (`trans_id`) USING BTREE,
  INDEX `phppos_inventory_ibfk_1`(`trans_items`) USING BTREE,
  INDEX `phppos_inventory_ibfk_2`(`trans_user`) USING BTREE,
  INDEX `location_id`(`location_id`) USING BTREE,
  INDEX `trans_date`(`trans_date`, `trans_inventory`, `location_id`) USING BTREE,
  INDEX `phppos_inventory_ibfk_4`(`item_variation_id`) USING BTREE,
  INDEX `phppos_inventory_custom`(`trans_items`, `location_id`, `trans_date`, `item_variation_id`, `trans_id`) USING BTREE,
  CONSTRAINT `phppos_inventory_ibfk_1` FOREIGN KEY (`trans_items`) REFERENCES `phppos_items` (`item_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_inventory_ibfk_2` FOREIGN KEY (`trans_user`) REFERENCES `phppos_employees` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_inventory_ibfk_3` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_inventory_ibfk_4` FOREIGN KEY (`item_variation_id`) REFERENCES `phppos_item_variations` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_inventory
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_inventory_counts
-- ----------------------------
DROP TABLE IF EXISTS `phppos_inventory_counts`;
CREATE TABLE `phppos_inventory_counts`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `count_date` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00',
  `employee_id` int NOT NULL,
  `location_id` int NOT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `comment` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_inventory_counts_ibfk_1`(`employee_id`) USING BTREE,
  INDEX `phppos_inventory_counts_ibfk_2`(`location_id`) USING BTREE,
  CONSTRAINT `phppos_inventory_counts_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `phppos_employees` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_inventory_counts_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_inventory_counts
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_inventory_counts_items
-- ----------------------------
DROP TABLE IF EXISTS `phppos_inventory_counts_items`;
CREATE TABLE `phppos_inventory_counts_items`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `inventory_counts_id` int NOT NULL,
  `item_id` int NOT NULL,
  `item_variation_id` int NULL DEFAULT NULL,
  `count` decimal(23, 10) NULL DEFAULT 0,
  `actual_quantity` decimal(23, 10) NULL DEFAULT 0,
  `comment` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_inventory_counts_items_ibfk_1`(`inventory_counts_id`) USING BTREE,
  INDEX `phppos_inventory_counts_items_ibfk_2`(`item_id`) USING BTREE,
  INDEX `inventory_counts_items_ibfk_3`(`item_variation_id`) USING BTREE,
  CONSTRAINT `inventory_counts_items_ibfk_3` FOREIGN KEY (`item_variation_id`) REFERENCES `phppos_item_variations` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_inventory_counts_items_ibfk_1` FOREIGN KEY (`inventory_counts_id`) REFERENCES `phppos_inventory_counts` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_inventory_counts_items_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `phppos_items` (`item_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_inventory_counts_items
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_item_attribute_values
-- ----------------------------
DROP TABLE IF EXISTS `phppos_item_attribute_values`;
CREATE TABLE `phppos_item_attribute_values`  (
  `item_id` int NOT NULL,
  `attribute_value_id` int NOT NULL,
  PRIMARY KEY (`attribute_value_id`, `item_id`) USING BTREE,
  INDEX `phppos_item_attribute_values_ibfk_1`(`item_id`) USING BTREE,
  CONSTRAINT `phppos_item_attribute_values_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `phppos_items` (`item_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_item_attribute_values_ibfk_2` FOREIGN KEY (`attribute_value_id`) REFERENCES `phppos_attribute_values` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_item_attribute_values
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_item_attributes
-- ----------------------------
DROP TABLE IF EXISTS `phppos_item_attributes`;
CREATE TABLE `phppos_item_attributes`  (
  `attribute_id` int NOT NULL,
  `item_id` int NOT NULL,
  PRIMARY KEY (`attribute_id`, `item_id`) USING BTREE,
  INDEX `phppos_item_attributes_ibfk_1`(`item_id`) USING BTREE,
  CONSTRAINT `phppos_item_attributes_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `phppos_items` (`item_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_item_attributes_ibfk_2` FOREIGN KEY (`attribute_id`) REFERENCES `phppos_attributes` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_item_attributes
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_item_images
-- ----------------------------
DROP TABLE IF EXISTS `phppos_item_images`;
CREATE TABLE `phppos_item_images`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `alt_text` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `item_id` int NULL DEFAULT NULL,
  `item_variation_id` int NULL DEFAULT NULL,
  `ecommerce_image_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `image_id` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_item_images_ibfk_1`(`item_id`) USING BTREE,
  INDEX `phppos_item_images_ibfk_2`(`image_id`) USING BTREE,
  INDEX `phppos_item_images_ibfk_3`(`item_variation_id`) USING BTREE,
  CONSTRAINT `phppos_item_images_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `phppos_items` (`item_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_item_images_ibfk_2` FOREIGN KEY (`image_id`) REFERENCES `phppos_app_files` (`file_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_item_images_ibfk_3` FOREIGN KEY (`item_variation_id`) REFERENCES `phppos_item_variations` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_item_images
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_item_kit_images
-- ----------------------------
DROP TABLE IF EXISTS `phppos_item_kit_images`;
CREATE TABLE `phppos_item_kit_images`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `alt_text` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `item_kit_id` int NULL DEFAULT NULL,
  `image_id` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_item_kit_images_ibfk_1`(`item_kit_id`) USING BTREE,
  INDEX `phppos_item_kit_images_ibfk_2`(`image_id`) USING BTREE,
  CONSTRAINT `phppos_item_kit_images_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `phppos_item_kits` (`item_kit_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_item_kit_images_ibfk_2` FOREIGN KEY (`image_id`) REFERENCES `phppos_app_files` (`file_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_item_kit_images
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_item_kit_item_kits
-- ----------------------------
DROP TABLE IF EXISTS `phppos_item_kit_item_kits`;
CREATE TABLE `phppos_item_kit_item_kits`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `item_kit_id` int NOT NULL,
  `item_kit_item_kit` int NOT NULL,
  `quantity` decimal(23, 10) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_item_kit_item_kits_ibfk_1`(`item_kit_id`) USING BTREE,
  INDEX `phppos_item_kit_item_kits_ibfk_2`(`item_kit_item_kit`) USING BTREE,
  CONSTRAINT `phppos_item_kit_item_kits_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `phppos_item_kits` (`item_kit_id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `phppos_item_kit_item_kits_ibfk_2` FOREIGN KEY (`item_kit_item_kit`) REFERENCES `phppos_item_kits` (`item_kit_id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_item_kit_item_kits
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_item_kit_items
-- ----------------------------
DROP TABLE IF EXISTS `phppos_item_kit_items`;
CREATE TABLE `phppos_item_kit_items`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `item_kit_id` int NOT NULL,
  `item_id` int NOT NULL,
  `item_variation_id` int NULL DEFAULT NULL,
  `quantity` decimal(23, 10) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_item_kit_items_ibfk_1`(`item_kit_id`) USING BTREE,
  INDEX `phppos_item_kit_items_ibfk_2`(`item_id`) USING BTREE,
  INDEX `phppos_item_kit_items_ibfk_3`(`item_variation_id`) USING BTREE,
  CONSTRAINT `phppos_item_kit_items_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `phppos_item_kits` (`item_kit_id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `phppos_item_kit_items_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `phppos_items` (`item_id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `phppos_item_kit_items_ibfk_3` FOREIGN KEY (`item_variation_id`) REFERENCES `phppos_item_variations` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_item_kit_items
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_item_kits
-- ----------------------------
DROP TABLE IF EXISTS `phppos_item_kits`;
CREATE TABLE `phppos_item_kits`  (
  `item_kit_id` int NOT NULL AUTO_INCREMENT,
  `item_kit_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `product_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int NULL DEFAULT NULL,
  `manufacturer_id` int NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tax_included` int NOT NULL DEFAULT 0,
  `unit_price` decimal(23, 10) NULL DEFAULT NULL,
  `cost_price` decimal(23, 10) NULL DEFAULT NULL,
  `override_default_tax` int NOT NULL DEFAULT 0,
  `is_ebt_item` int NOT NULL DEFAULT 0,
  `commission_percent` decimal(23, 10) NULL DEFAULT 0,
  `commission_percent_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  `commission_fixed` decimal(23, 10) NULL DEFAULT 0,
  `change_cost_price` int NOT NULL DEFAULT 0,
  `disable_loyalty` int NOT NULL DEFAULT 0,
  `deleted` int NOT NULL DEFAULT 0,
  `tax_class_id` int NULL DEFAULT NULL,
  `max_discount_percent` decimal(15, 3) NULL DEFAULT NULL,
  `max_edit_price` decimal(23, 10) NULL DEFAULT NULL,
  `min_edit_price` decimal(23, 10) NULL DEFAULT NULL,
  `custom_field_1_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_2_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_3_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_4_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_5_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_6_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_7_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_8_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_9_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_10_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `required_age` int NULL DEFAULT NULL,
  `verify_age` int NOT NULL DEFAULT 0,
  `allow_price_override_regardless_of_permissions` int NULL DEFAULT 0,
  `only_integer` int NOT NULL DEFAULT 0,
  `is_barcoded` int NOT NULL DEFAULT 1,
  `default_quantity` decimal(23, 10) NULL DEFAULT NULL,
  `disable_from_price_rules` int NULL DEFAULT 0,
  `main_image_id` int NULL DEFAULT NULL,
  `dynamic_pricing` int NOT NULL DEFAULT 0,
  `info_popup` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `item_kit_inactive` int NULL DEFAULT 0,
  `barcode_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `is_favorite` int NULL DEFAULT 0,
  `loyalty_multiplier` decimal(23, 10) NULL DEFAULT NULL,
  PRIMARY KEY (`item_kit_id`) USING BTREE,
  UNIQUE INDEX `item_kit_number`(`item_kit_number`) USING BTREE,
  UNIQUE INDEX `product_id`(`product_id`) USING BTREE,
  INDEX `deleted`(`deleted`) USING BTREE,
  INDEX `phppos_item_kits_ibfk_1`(`category_id`) USING BTREE,
  INDEX `phppos_item_kits_ibfk_2`(`manufacturer_id`) USING BTREE,
  INDEX `name`(`name`) USING BTREE,
  INDEX `description`(`description`) USING BTREE,
  INDEX `cost_price`(`cost_price`) USING BTREE,
  INDEX `unit_price`(`unit_price`) USING BTREE,
  INDEX `phppos_item_kits_ibfk_3`(`tax_class_id`) USING BTREE,
  INDEX `custom_field_1_value`(`custom_field_1_value`) USING BTREE,
  INDEX `custom_field_2_value`(`custom_field_2_value`) USING BTREE,
  INDEX `custom_field_3_value`(`custom_field_3_value`) USING BTREE,
  INDEX `custom_field_4_value`(`custom_field_4_value`) USING BTREE,
  INDEX `custom_field_5_value`(`custom_field_5_value`) USING BTREE,
  INDEX `custom_field_6_value`(`custom_field_6_value`) USING BTREE,
  INDEX `custom_field_7_value`(`custom_field_7_value`) USING BTREE,
  INDEX `custom_field_8_value`(`custom_field_8_value`) USING BTREE,
  INDEX `custom_field_9_value`(`custom_field_9_value`) USING BTREE,
  INDEX `custom_field_10_value`(`custom_field_10_value`) USING BTREE,
  INDEX `verify_age`(`verify_age`) USING BTREE,
  INDEX `phppos_item_kits_ibfk_4`(`main_image_id`) USING BTREE,
  INDEX `item_kit_inactive_index`(`item_kit_inactive`) USING BTREE,
  INDEX `is_favorite_index`(`is_favorite`) USING BTREE,
  CONSTRAINT `phppos_item_kits_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `phppos_categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_item_kits_ibfk_2` FOREIGN KEY (`manufacturer_id`) REFERENCES `phppos_manufacturers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_item_kits_ibfk_3` FOREIGN KEY (`tax_class_id`) REFERENCES `phppos_tax_classes` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_item_kits_ibfk_4` FOREIGN KEY (`main_image_id`) REFERENCES `phppos_item_kit_images` (`image_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_item_kits
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_item_kits_modifiers
-- ----------------------------
DROP TABLE IF EXISTS `phppos_item_kits_modifiers`;
CREATE TABLE `phppos_item_kits_modifiers`  (
  `item_kit_id` int NOT NULL,
  `modifier_id` int NOT NULL,
  PRIMARY KEY (`item_kit_id`, `modifier_id`) USING BTREE,
  INDEX `phppos_item_kits_modifiers_ibfk_1`(`modifier_id`) USING BTREE,
  CONSTRAINT `phppos_item_kits_modifiers_ibfk_1` FOREIGN KEY (`modifier_id`) REFERENCES `phppos_modifiers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_item_kits_modifiers_ibfk_2` FOREIGN KEY (`item_kit_id`) REFERENCES `phppos_item_kits` (`item_kit_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_item_kits_modifiers
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_item_kits_pricing_history
-- ----------------------------
DROP TABLE IF EXISTS `phppos_item_kits_pricing_history`;
CREATE TABLE `phppos_item_kits_pricing_history`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `on_date` timestamp(0) NOT NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `employee_id` int NOT NULL,
  `item_kit_id` int NOT NULL,
  `location_id` int NULL DEFAULT NULL,
  `unit_price` decimal(23, 10) NULL DEFAULT NULL,
  `cost_price` decimal(23, 10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_item_kits_pricing_history_ibfk_1`(`item_kit_id`) USING BTREE,
  INDEX `phppos_item_kits_pricing_history_ibfk_2`(`location_id`) USING BTREE,
  INDEX `phppos_item_kits_pricing_history_ibfk_3`(`employee_id`) USING BTREE,
  CONSTRAINT `phppos_item_kits_pricing_history_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `phppos_item_kits` (`item_kit_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_item_kits_pricing_history_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_item_kits_pricing_history_ibfk_3` FOREIGN KEY (`employee_id`) REFERENCES `phppos_employees` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_item_kits_pricing_history
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_item_kits_tags
-- ----------------------------
DROP TABLE IF EXISTS `phppos_item_kits_tags`;
CREATE TABLE `phppos_item_kits_tags`  (
  `item_kit_id` int NOT NULL,
  `tag_id` int NOT NULL,
  PRIMARY KEY (`item_kit_id`, `tag_id`) USING BTREE,
  INDEX `phppos_item_kits_tags_ibfk_2`(`tag_id`) USING BTREE,
  CONSTRAINT `phppos_item_kits_tags_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `phppos_item_kits` (`item_kit_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_item_kits_tags_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `phppos_tags` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_item_kits_tags
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_item_kits_taxes
-- ----------------------------
DROP TABLE IF EXISTS `phppos_item_kits_taxes`;
CREATE TABLE `phppos_item_kits_taxes`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `item_kit_id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15, 3) NOT NULL,
  `cumulative` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unique_tax`(`item_kit_id`, `name`, `percent`) USING BTREE,
  CONSTRAINT `phppos_item_kits_taxes_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `phppos_item_kits` (`item_kit_id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_item_kits_taxes
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_item_kits_tier_prices
-- ----------------------------
DROP TABLE IF EXISTS `phppos_item_kits_tier_prices`;
CREATE TABLE `phppos_item_kits_tier_prices`  (
  `tier_id` int NOT NULL,
  `item_kit_id` int NOT NULL,
  `unit_price` decimal(23, 10) NULL DEFAULT 0,
  `percent_off` decimal(15, 3) NULL DEFAULT NULL,
  `cost_plus_percent` decimal(15, 3) NULL DEFAULT NULL,
  `cost_plus_fixed_amount` decimal(23, 10) NULL DEFAULT NULL,
  PRIMARY KEY (`tier_id`, `item_kit_id`) USING BTREE,
  INDEX `phppos_item_kits_tier_prices_ibfk_2`(`item_kit_id`) USING BTREE,
  CONSTRAINT `phppos_item_kits_tier_prices_ibfk_1` FOREIGN KEY (`tier_id`) REFERENCES `phppos_price_tiers` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `phppos_item_kits_tier_prices_ibfk_2` FOREIGN KEY (`item_kit_id`) REFERENCES `phppos_item_kits` (`item_kit_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_item_kits_tier_prices
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_item_variation_attribute_values
-- ----------------------------
DROP TABLE IF EXISTS `phppos_item_variation_attribute_values`;
CREATE TABLE `phppos_item_variation_attribute_values`  (
  `attribute_value_id` int NOT NULL,
  `item_variation_id` int NOT NULL,
  PRIMARY KEY (`attribute_value_id`, `item_variation_id`) USING BTREE,
  INDEX `phppos_item_variation_attribute_values_ibfk_2`(`item_variation_id`) USING BTREE,
  CONSTRAINT `phppos_item_variation_attribute_values_ibfk_1` FOREIGN KEY (`attribute_value_id`) REFERENCES `phppos_attribute_values` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `phppos_item_variation_attribute_values_ibfk_2` FOREIGN KEY (`item_variation_id`) REFERENCES `phppos_item_variations` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_item_variation_attribute_values
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_item_variations
-- ----------------------------
DROP TABLE IF EXISTS `phppos_item_variations`;
CREATE TABLE `phppos_item_variations`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `ecommerce_variation_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `ecommerce_variation_quantity` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `deleted` int NOT NULL DEFAULT 0,
  `item_id` int NOT NULL,
  `reorder_level` decimal(23, 10) NULL DEFAULT NULL,
  `replenish_level` decimal(23, 10) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  `item_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `unit_price` decimal(23, 10) NULL DEFAULT NULL,
  `cost_price` decimal(23, 10) NULL DEFAULT NULL,
  `promo_price` decimal(23, 10) NULL DEFAULT NULL,
  `start_date` date NULL DEFAULT NULL,
  `end_date` date NULL DEFAULT NULL,
  `last_modified` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  `ecommerce_last_modified` timestamp(0) NULL DEFAULT NULL,
  `is_ecommerce` int NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `item_number`(`item_number`) USING BTREE,
  INDEX `phppos_item_variations_ibfk_1`(`item_id`) USING BTREE,
  INDEX `phppos_item_variations_ibfk_2`(`ecommerce_variation_id`) USING BTREE,
  FULLTEXT INDEX `name_search`(`name`),
  CONSTRAINT `phppos_item_variations_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `phppos_items` (`item_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_item_variations
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_items
-- ----------------------------
DROP TABLE IF EXISTS `phppos_items`;
CREATE TABLE `phppos_items`  (
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int NULL DEFAULT NULL,
  `supplier_id` int NULL DEFAULT NULL,
  `manufacturer_id` int NULL DEFAULT NULL,
  `item_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `product_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `ecommerce_product_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `ecommerce_product_quantity` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `size` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tax_included` int NOT NULL DEFAULT 0,
  `cost_price` decimal(23, 10) NOT NULL,
  `unit_price` decimal(23, 10) NOT NULL,
  `promo_price` decimal(23, 10) NULL DEFAULT NULL,
  `start_date` date NULL DEFAULT NULL,
  `end_date` date NULL DEFAULT NULL,
  `reorder_level` decimal(23, 10) NULL DEFAULT NULL,
  `expire_days` int NULL DEFAULT NULL,
  `item_id` int NOT NULL AUTO_INCREMENT,
  `allow_alt_description` tinyint(1) NOT NULL,
  `is_serialized` tinyint(1) NOT NULL,
  `override_default_tax` int NOT NULL DEFAULT 0,
  `is_ecommerce` int NULL DEFAULT 1,
  `is_service` int NOT NULL DEFAULT 0,
  `is_ebt_item` int NOT NULL DEFAULT 0,
  `commission_percent` decimal(23, 10) NULL DEFAULT 0,
  `commission_percent_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  `commission_fixed` decimal(23, 10) NULL DEFAULT 0,
  `change_cost_price` int NOT NULL DEFAULT 0,
  `disable_loyalty` int NOT NULL DEFAULT 0,
  `deleted` int NOT NULL DEFAULT 0,
  `last_modified` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  `ecommerce_last_modified` timestamp(0) NULL DEFAULT NULL,
  `tax_class_id` int NULL DEFAULT NULL,
  `replenish_level` decimal(23, 10) NULL DEFAULT NULL,
  `system_item` int NOT NULL DEFAULT 0,
  `max_discount_percent` decimal(15, 3) NULL DEFAULT NULL,
  `max_edit_price` decimal(23, 10) NULL DEFAULT NULL,
  `min_edit_price` decimal(23, 10) NULL DEFAULT NULL,
  `custom_field_1_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_2_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_3_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_4_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_5_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_6_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_7_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_8_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_9_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_10_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `required_age` int NULL DEFAULT NULL,
  `verify_age` int NOT NULL DEFAULT 0,
  `weight` decimal(23, 10) NULL DEFAULT NULL,
  `length` decimal(23, 10) NULL DEFAULT NULL,
  `width` decimal(23, 10) NULL DEFAULT NULL,
  `height` decimal(23, 10) NULL DEFAULT NULL,
  `ecommerce_shipping_class_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `long_description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `allow_price_override_regardless_of_permissions` int NULL DEFAULT 0,
  `main_image_id` int NULL DEFAULT NULL,
  `only_integer` int NOT NULL DEFAULT 0,
  `is_series_package` int NOT NULL DEFAULT 0,
  `series_quantity` int NULL DEFAULT NULL,
  `series_days_to_use_within` int NULL DEFAULT NULL,
  `is_barcoded` int NOT NULL DEFAULT 1,
  `default_quantity` decimal(23, 10) NULL DEFAULT NULL,
  `disable_from_price_rules` int NULL DEFAULT 0,
  `last_edited` timestamp(0) NULL DEFAULT NULL,
  `info_popup` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `item_inactive` int NULL DEFAULT 0,
  `barcode_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tags` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  `is_favorite` int NULL DEFAULT 0,
  `loyalty_multiplier` decimal(23, 10) NULL DEFAULT NULL,
  PRIMARY KEY (`item_id`) USING BTREE,
  UNIQUE INDEX `item_number`(`item_number`) USING BTREE,
  UNIQUE INDEX `product_id`(`product_id`) USING BTREE,
  INDEX `phppos_items_ibfk_1`(`supplier_id`) USING BTREE,
  INDEX `deleted`(`deleted`) USING BTREE,
  INDEX `phppos_items_ibfk_3`(`category_id`) USING BTREE,
  INDEX `phppos_items_ibfk_4`(`manufacturer_id`) USING BTREE,
  INDEX `phppos_items_ibfk_5`(`ecommerce_product_id`) USING BTREE,
  INDEX `description`(`description`(255)) USING BTREE,
  INDEX `size`(`size`) USING BTREE,
  INDEX `reorder_level`(`reorder_level`) USING BTREE,
  INDEX `cost_price`(`cost_price`) USING BTREE,
  INDEX `unit_price`(`unit_price`) USING BTREE,
  INDEX `promo_price`(`promo_price`) USING BTREE,
  INDEX `last_modified`(`last_modified`) USING BTREE,
  INDEX `name`(`name`) USING BTREE,
  INDEX `phppos_items_ibfk_6`(`tax_class_id`) USING BTREE,
  INDEX `deleted_system_item`(`deleted`, `system_item`) USING BTREE,
  INDEX `custom_field_1_value`(`custom_field_1_value`) USING BTREE,
  INDEX `custom_field_2_value`(`custom_field_2_value`) USING BTREE,
  INDEX `custom_field_3_value`(`custom_field_3_value`) USING BTREE,
  INDEX `custom_field_4_value`(`custom_field_4_value`) USING BTREE,
  INDEX `custom_field_5_value`(`custom_field_5_value`) USING BTREE,
  INDEX `custom_field_6_value`(`custom_field_6_value`) USING BTREE,
  INDEX `custom_field_7_value`(`custom_field_7_value`) USING BTREE,
  INDEX `custom_field_8_value`(`custom_field_8_value`) USING BTREE,
  INDEX `custom_field_9_value`(`custom_field_9_value`) USING BTREE,
  INDEX `custom_field_10_value`(`custom_field_10_value`) USING BTREE,
  INDEX `verify_age`(`verify_age`) USING BTREE,
  INDEX `phppos_items_ibfk_7`(`main_image_id`) USING BTREE,
  INDEX `item_inactive_index`(`item_inactive`) USING BTREE,
  INDEX `tags`(`tags`) USING BTREE,
  INDEX `is_favorite_index`(`is_favorite`) USING BTREE,
  INDEX `ecommerce_product_id`(`ecommerce_product_id`) USING BTREE,
  FULLTEXT INDEX `full_search`(`name`, `item_number`, `product_id`, `description`),
  FULLTEXT INDEX `name_search`(`name`),
  FULLTEXT INDEX `item_number_search`(`item_number`),
  FULLTEXT INDEX `product_id_search`(`product_id`),
  FULLTEXT INDEX `description_search`(`description`),
  FULLTEXT INDEX `size_search`(`size`),
  FULLTEXT INDEX `custom_field_1_value_search`(`custom_field_1_value`),
  FULLTEXT INDEX `custom_field_2_value_search`(`custom_field_2_value`),
  FULLTEXT INDEX `custom_field_3_value_search`(`custom_field_3_value`),
  FULLTEXT INDEX `custom_field_4_value_search`(`custom_field_4_value`),
  FULLTEXT INDEX `custom_field_5_value_search`(`custom_field_5_value`),
  FULLTEXT INDEX `custom_field_6_value_search`(`custom_field_6_value`),
  FULLTEXT INDEX `custom_field_7_value_search`(`custom_field_7_value`),
  FULLTEXT INDEX `custom_field_8_value_search`(`custom_field_8_value`),
  FULLTEXT INDEX `custom_field_9_value_search`(`custom_field_9_value`),
  FULLTEXT INDEX `custom_field_10_value_search`(`custom_field_10_value`),
  CONSTRAINT `phppos_items_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `phppos_suppliers` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_items_ibfk_3` FOREIGN KEY (`category_id`) REFERENCES `phppos_categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_items_ibfk_4` FOREIGN KEY (`manufacturer_id`) REFERENCES `phppos_manufacturers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_items_ibfk_6` FOREIGN KEY (`tax_class_id`) REFERENCES `phppos_tax_classes` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_items_ibfk_7` FOREIGN KEY (`main_image_id`) REFERENCES `phppos_item_images` (`image_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_items
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_items_modifiers
-- ----------------------------
DROP TABLE IF EXISTS `phppos_items_modifiers`;
CREATE TABLE `phppos_items_modifiers`  (
  `item_id` int NOT NULL,
  `modifier_id` int NOT NULL,
  PRIMARY KEY (`item_id`, `modifier_id`) USING BTREE,
  INDEX `phppos_items_modifiers_ibfk_1`(`modifier_id`) USING BTREE,
  CONSTRAINT `phppos_items_modifiers_ibfk_1` FOREIGN KEY (`modifier_id`) REFERENCES `phppos_modifiers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_items_modifiers_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `phppos_items` (`item_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_items_modifiers
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_items_pricing_history
-- ----------------------------
DROP TABLE IF EXISTS `phppos_items_pricing_history`;
CREATE TABLE `phppos_items_pricing_history`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `on_date` timestamp(0) NOT NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `employee_id` int NOT NULL,
  `item_id` int NOT NULL,
  `item_variation_id` int NULL DEFAULT NULL,
  `location_id` int NULL DEFAULT NULL,
  `unit_price` decimal(23, 10) NULL DEFAULT NULL,
  `cost_price` decimal(23, 10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_items_pricing_history_ibfk_1`(`item_id`) USING BTREE,
  INDEX `phppos_items_pricing_history_ibfk_2`(`item_variation_id`) USING BTREE,
  INDEX `phppos_items_pricing_history_ibfk_3`(`location_id`) USING BTREE,
  INDEX `phppos_items_pricing_history_ibfk_4`(`employee_id`) USING BTREE,
  CONSTRAINT `phppos_items_pricing_history_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `phppos_items` (`item_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_items_pricing_history_ibfk_2` FOREIGN KEY (`item_variation_id`) REFERENCES `phppos_item_variations` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_items_pricing_history_ibfk_3` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_items_pricing_history_ibfk_4` FOREIGN KEY (`employee_id`) REFERENCES `phppos_employees` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_items_pricing_history
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_items_quantity_units
-- ----------------------------
DROP TABLE IF EXISTS `phppos_items_quantity_units`;
CREATE TABLE `phppos_items_quantity_units`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `item_id` int NOT NULL,
  `unit_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `unit_quantity` decimal(23, 10) NOT NULL,
  `unit_price` decimal(23, 10) NULL DEFAULT NULL,
  `cost_price` decimal(23, 10) NULL DEFAULT NULL,
  `quantity_unit_item_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `quantity_unit_item_number`(`quantity_unit_item_number`) USING BTREE,
  INDEX `phppos_items_quantity_units_ibfk_1`(`item_id`) USING BTREE,
  CONSTRAINT `phppos_items_quantity_units_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `phppos_items` (`item_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_items_quantity_units
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_items_serial_numbers
-- ----------------------------
DROP TABLE IF EXISTS `phppos_items_serial_numbers`;
CREATE TABLE `phppos_items_serial_numbers`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `item_id` int NOT NULL,
  `serial_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `unit_price` decimal(23, 10) NULL DEFAULT NULL,
  `cost_price` decimal(23, 10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `serial_number`(`serial_number`) USING BTREE,
  INDEX `phppos_items_serial_numbers_ibfk_1`(`item_id`) USING BTREE,
  CONSTRAINT `phppos_items_serial_numbers_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `phppos_items` (`item_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_items_serial_numbers
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_items_tags
-- ----------------------------
DROP TABLE IF EXISTS `phppos_items_tags`;
CREATE TABLE `phppos_items_tags`  (
  `item_id` int NOT NULL,
  `tag_id` int NOT NULL,
  PRIMARY KEY (`item_id`, `tag_id`) USING BTREE,
  INDEX `phppos_items_tags_ibfk_2`(`tag_id`) USING BTREE,
  CONSTRAINT `phppos_items_tags_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `phppos_items` (`item_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_items_tags_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `phppos_tags` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_items_tags
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_items_taxes
-- ----------------------------
DROP TABLE IF EXISTS `phppos_items_taxes`;
CREATE TABLE `phppos_items_taxes`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `item_id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15, 3) NOT NULL,
  `cumulative` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unique_tax`(`item_id`, `name`, `percent`) USING BTREE,
  CONSTRAINT `phppos_items_taxes_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `phppos_items` (`item_id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_items_taxes
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_items_tier_prices
-- ----------------------------
DROP TABLE IF EXISTS `phppos_items_tier_prices`;
CREATE TABLE `phppos_items_tier_prices`  (
  `tier_id` int NOT NULL,
  `item_id` int NOT NULL,
  `unit_price` decimal(23, 10) NULL DEFAULT 0,
  `percent_off` decimal(15, 3) NULL DEFAULT NULL,
  `cost_plus_percent` decimal(15, 3) NULL DEFAULT NULL,
  `cost_plus_fixed_amount` decimal(23, 10) NULL DEFAULT NULL,
  PRIMARY KEY (`tier_id`, `item_id`) USING BTREE,
  INDEX `phppos_items_tier_prices_ibfk_2`(`item_id`) USING BTREE,
  CONSTRAINT `phppos_items_tier_prices_ibfk_1` FOREIGN KEY (`tier_id`) REFERENCES `phppos_price_tiers` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `phppos_items_tier_prices_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `phppos_items` (`item_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_items_tier_prices
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_keys
-- ----------------------------
DROP TABLE IF EXISTS `phppos_keys`;
CREATE TABLE `phppos_keys`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NULL DEFAULT NULL,
  `key` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `key_ending` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `level` int NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT 0,
  `is_private_key` tinyint(1) NOT NULL DEFAULT 0,
  `ip_addresses` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `date_created` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `key`(`key`) USING BTREE,
  INDEX `phppos_keys_user_id_fk`(`user_id`) USING BTREE,
  CONSTRAINT `phppos_keys_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `phppos_employees` (`person_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_keys
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_limits
-- ----------------------------
DROP TABLE IF EXISTS `phppos_limits`;
CREATE TABLE `phppos_limits`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `api_key` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `uri` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `count` int NOT NULL,
  `hour_started` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uri`(`uri`) USING BTREE,
  INDEX `phppos_limits_api_key_fk`(`api_key`) USING BTREE,
  CONSTRAINT `phppos_limits_api_key_fk` FOREIGN KEY (`api_key`) REFERENCES `phppos_keys` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_limits
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_location_item_kits
-- ----------------------------
DROP TABLE IF EXISTS `phppos_location_item_kits`;
CREATE TABLE `phppos_location_item_kits`  (
  `location_id` int NOT NULL,
  `item_kit_id` int NOT NULL,
  `unit_price` decimal(23, 10) NULL DEFAULT NULL,
  `cost_price` decimal(23, 10) NULL DEFAULT NULL,
  `override_default_tax` int NOT NULL DEFAULT 0,
  `tax_class_id` int NULL DEFAULT NULL,
  PRIMARY KEY (`location_id`, `item_kit_id`) USING BTREE,
  INDEX `phppos_location_item_kits_ibfk_2`(`item_kit_id`) USING BTREE,
  INDEX `phppos_location_item_kits_ibfk_3`(`tax_class_id`) USING BTREE,
  CONSTRAINT `phppos_location_item_kits_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_location_item_kits_ibfk_2` FOREIGN KEY (`item_kit_id`) REFERENCES `phppos_item_kits` (`item_kit_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_location_item_kits_ibfk_3` FOREIGN KEY (`tax_class_id`) REFERENCES `phppos_tax_classes` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_location_item_kits
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_location_item_kits_taxes
-- ----------------------------
DROP TABLE IF EXISTS `phppos_location_item_kits_taxes`;
CREATE TABLE `phppos_location_item_kits_taxes`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `location_id` int NOT NULL,
  `item_kit_id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(16, 3) NOT NULL,
  `cumulative` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unique_tax`(`location_id`, `item_kit_id`, `name`, `percent`) USING BTREE,
  INDEX `phppos_location_item_kits_taxes_ibfk_2`(`item_kit_id`) USING BTREE,
  CONSTRAINT `phppos_location_item_kits_taxes_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `phppos_location_item_kits_taxes_ibfk_2` FOREIGN KEY (`item_kit_id`) REFERENCES `phppos_item_kits` (`item_kit_id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_location_item_kits_taxes
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_location_item_kits_tier_prices
-- ----------------------------
DROP TABLE IF EXISTS `phppos_location_item_kits_tier_prices`;
CREATE TABLE `phppos_location_item_kits_tier_prices`  (
  `tier_id` int NOT NULL,
  `item_kit_id` int NOT NULL,
  `location_id` int NOT NULL,
  `unit_price` decimal(23, 10) NULL DEFAULT 0,
  `percent_off` decimal(15, 3) NULL DEFAULT NULL,
  `cost_plus_percent` decimal(15, 3) NULL DEFAULT NULL,
  `cost_plus_fixed_amount` decimal(23, 10) NULL DEFAULT NULL,
  PRIMARY KEY (`tier_id`, `item_kit_id`, `location_id`) USING BTREE,
  INDEX `phppos_location_item_kits_tier_prices_ibfk_2`(`location_id`) USING BTREE,
  INDEX `phppos_location_item_kits_tier_prices_ibfk_3`(`item_kit_id`) USING BTREE,
  CONSTRAINT `phppos_location_item_kits_tier_prices_ibfk_1` FOREIGN KEY (`tier_id`) REFERENCES `phppos_price_tiers` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `phppos_location_item_kits_tier_prices_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_location_item_kits_tier_prices_ibfk_3` FOREIGN KEY (`item_kit_id`) REFERENCES `phppos_item_kits` (`item_kit_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_location_item_kits_tier_prices
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_location_item_variations
-- ----------------------------
DROP TABLE IF EXISTS `phppos_location_item_variations`;
CREATE TABLE `phppos_location_item_variations`  (
  `item_variation_id` int NOT NULL,
  `location_id` int NOT NULL,
  `quantity` int NULL DEFAULT NULL,
  `reorder_level` decimal(23, 10) NULL DEFAULT NULL,
  `replenish_level` decimal(23, 10) NULL DEFAULT NULL,
  `unit_price` decimal(23, 10) NULL DEFAULT NULL,
  `cost_price` decimal(23, 10) NULL DEFAULT NULL,
  PRIMARY KEY (`item_variation_id`, `location_id`) USING BTREE,
  INDEX `phppos_item_attribute_location_values_ibfk_2`(`location_id`) USING BTREE,
  CONSTRAINT `phppos_item_attribute_location_values_ibfk_1` FOREIGN KEY (`item_variation_id`) REFERENCES `phppos_item_variations` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_item_attribute_location_values_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_location_item_variations
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_location_items
-- ----------------------------
DROP TABLE IF EXISTS `phppos_location_items`;
CREATE TABLE `phppos_location_items`  (
  `location_id` int NOT NULL,
  `item_id` int NOT NULL,
  `location` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cost_price` decimal(23, 10) NULL DEFAULT NULL,
  `unit_price` decimal(23, 10) NULL DEFAULT NULL,
  `promo_price` decimal(23, 10) NULL DEFAULT NULL,
  `start_date` date NULL DEFAULT NULL,
  `end_date` date NULL DEFAULT NULL,
  `quantity` decimal(23, 10) NULL DEFAULT 0,
  `reorder_level` decimal(23, 10) NULL DEFAULT NULL,
  `override_default_tax` int NOT NULL DEFAULT 0,
  `tax_class_id` int NULL DEFAULT NULL,
  `replenish_level` decimal(23, 10) NULL DEFAULT NULL,
  PRIMARY KEY (`location_id`, `item_id`) USING BTREE,
  INDEX `phppos_location_items_ibfk_2`(`item_id`) USING BTREE,
  INDEX `quantity`(`quantity`) USING BTREE,
  INDEX `phppos_location_items_ibfk_3`(`tax_class_id`) USING BTREE,
  CONSTRAINT `phppos_location_items_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_location_items_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `phppos_items` (`item_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_location_items_ibfk_3` FOREIGN KEY (`tax_class_id`) REFERENCES `phppos_tax_classes` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_location_items
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_location_items_taxes
-- ----------------------------
DROP TABLE IF EXISTS `phppos_location_items_taxes`;
CREATE TABLE `phppos_location_items_taxes`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `location_id` int NOT NULL,
  `item_id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(16, 3) NOT NULL,
  `cumulative` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unique_tax`(`location_id`, `item_id`, `name`, `percent`) USING BTREE,
  INDEX `phppos_location_items_taxes_ibfk_2`(`item_id`) USING BTREE,
  CONSTRAINT `phppos_location_items_taxes_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `phppos_location_items_taxes_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `phppos_items` (`item_id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_location_items_taxes
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_location_items_tier_prices
-- ----------------------------
DROP TABLE IF EXISTS `phppos_location_items_tier_prices`;
CREATE TABLE `phppos_location_items_tier_prices`  (
  `tier_id` int NOT NULL,
  `item_id` int NOT NULL,
  `location_id` int NOT NULL,
  `unit_price` decimal(23, 10) NULL DEFAULT 0,
  `percent_off` decimal(15, 3) NULL DEFAULT NULL,
  `cost_plus_percent` decimal(15, 3) NULL DEFAULT NULL,
  `cost_plus_fixed_amount` decimal(23, 10) NULL DEFAULT NULL,
  PRIMARY KEY (`tier_id`, `item_id`, `location_id`) USING BTREE,
  INDEX `phppos_location_items_tier_prices_ibfk_2`(`location_id`) USING BTREE,
  INDEX `phppos_location_items_tier_prices_ibfk_3`(`item_id`) USING BTREE,
  CONSTRAINT `phppos_location_items_tier_prices_ibfk_1` FOREIGN KEY (`tier_id`) REFERENCES `phppos_price_tiers` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `phppos_location_items_tier_prices_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_location_items_tier_prices_ibfk_3` FOREIGN KEY (`item_id`) REFERENCES `phppos_items` (`item_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_location_items_tier_prices
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_locations
-- ----------------------------
DROP TABLE IF EXISTS `phppos_locations`;
CREATE TABLE `phppos_locations`  (
  `location_id` int NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `company` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `website` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `company_logo` int NULL DEFAULT NULL,
  `address` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `phone` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `fax` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `email` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `cc_email` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `bcc_email` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `color` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `return_policy` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `receive_stock_alert` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `stock_alert_email` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `timezone` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `mailchimp_api_key` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `enable_credit_card_processing` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `credit_card_processor` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `hosted_checkout_merchant_id` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `hosted_checkout_merchant_password` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `emv_merchant_id` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `net_e_pay_server` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `listener_port` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `com_port` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `stripe_public` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `stripe_private` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `stripe_currency_code` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `braintree_merchant_id` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `braintree_public_key` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `braintree_private_key` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `default_tax_1_rate` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `default_tax_1_name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `default_tax_2_rate` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `default_tax_2_name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `default_tax_2_cumulative` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `default_tax_3_rate` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `default_tax_3_name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `default_tax_4_rate` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `default_tax_4_name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `default_tax_5_rate` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `default_tax_5_name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `deleted` int NULL DEFAULT 0,
  `secure_device_override_emv` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `secure_device_override_non_emv` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tax_class_id` int NULL DEFAULT NULL,
  `ebt_integrated` int NOT NULL DEFAULT 0,
  `integrated_gift_cards` int NOT NULL DEFAULT 0,
  `square_currency_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'USD',
  `square_location_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `square_currency_multiplier` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '100',
  `email_sales_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `email_receivings_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `stock_alerts_just_order_level` int NULL DEFAULT 0,
  `platformly_api_key` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `platformly_project_id` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `tax_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `disable_markup_markdown` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `card_connect_mid` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `card_connect_rest_username` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `card_connect_rest_password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `default_mailchimp_lists` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `twilio_sid` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `twilio_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `twilio_sms_from` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`location_id`) USING BTREE,
  INDEX `deleted`(`deleted`) USING BTREE,
  INDEX `phppos_locations_ibfk_1`(`company_logo`) USING BTREE,
  INDEX `name`(`name`(255)) USING BTREE,
  INDEX `address`(`address`(255)) USING BTREE,
  INDEX `phone`(`phone`(255)) USING BTREE,
  INDEX `email`(`email`(255)) USING BTREE,
  INDEX `phppos_locations_ibfk_2`(`tax_class_id`) USING BTREE,
  CONSTRAINT `phppos_locations_ibfk_1` FOREIGN KEY (`company_logo`) REFERENCES `phppos_app_files` (`file_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_locations_ibfk_2` FOREIGN KEY (`tax_class_id`) REFERENCES `phppos_tax_classes` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_locations
-- ----------------------------
INSERT INTO `phppos_locations` VALUES (1, 'Default', NULL, NULL, NULL, '123 Nowhere street', '555-555-5555', '', 'no-reply@phppointofsale.com', NULL, NULL, NULL, NULL, '0', '', 'America/New_York', '', '0', NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '0', '', '', '', '', '', '', 0, '', '', NULL, 1, 0, 'USD', '', '100', NULL, NULL, 0, NULL, NULL, '', NULL, NULL, NULL, NULL, '', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for phppos_logs
-- ----------------------------
DROP TABLE IF EXISTS `phppos_logs`;
CREATE TABLE `phppos_logs`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `api_key` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `uri` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `method` enum('get','post','options','put','patch','delete') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `params` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `ip_address` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `time` int NOT NULL,
  `rtime` float NULL DEFAULT NULL,
  `authorized` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `response_code` smallint NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_logs
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_manufacturers
-- ----------------------------
DROP TABLE IF EXISTS `phppos_manufacturers`;
CREATE TABLE `phppos_manufacturers`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `deleted` int NOT NULL DEFAULT 0,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `deleted`(`deleted`) USING BTREE,
  INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_manufacturers
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_message_receiver
-- ----------------------------
DROP TABLE IF EXISTS `phppos_message_receiver`;
CREATE TABLE `phppos_message_receiver`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `message_id` int NOT NULL,
  `receiver_id` int NOT NULL,
  `message_read` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_message_receiver_ibfk_2`(`receiver_id`) USING BTREE,
  INDEX `phppos_message_receiver_key_1`(`message_id`, `receiver_id`) USING BTREE,
  CONSTRAINT `phppos_message_receiver_ibfk_1` FOREIGN KEY (`message_id`) REFERENCES `phppos_messages` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_message_receiver_ibfk_2` FOREIGN KEY (`receiver_id`) REFERENCES `phppos_employees` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_message_receiver
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_messages
-- ----------------------------
DROP TABLE IF EXISTS `phppos_messages`;
CREATE TABLE `phppos_messages`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `message` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sender_id` int NOT NULL,
  `created_at` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  `deleted` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_messages_ibfk_1`(`sender_id`) USING BTREE,
  INDEX `phppos_messages_key_1`(`deleted`, `created_at`, `id`) USING BTREE,
  CONSTRAINT `phppos_messages_ibfk_1` FOREIGN KEY (`sender_id`) REFERENCES `phppos_employees` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_messages
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_migrations
-- ----------------------------
DROP TABLE IF EXISTS `phppos_migrations`;
CREATE TABLE `phppos_migrations`  (
  `version` bigint NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_migrations
-- ----------------------------
INSERT INTO `phppos_migrations` VALUES (20201012095537);

-- ----------------------------
-- Table structure for phppos_modifier_items
-- ----------------------------
DROP TABLE IF EXISTS `phppos_modifier_items`;
CREATE TABLE `phppos_modifier_items`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `sort_order` int NOT NULL DEFAULT 0,
  `modifier_id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `cost_price` decimal(23, 10) NOT NULL DEFAULT 0,
  `unit_price` decimal(23, 10) NOT NULL DEFAULT 0,
  `deleted` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_modifier_items_ibfk_1`(`modifier_id`) USING BTREE,
  INDEX `sort_index`(`deleted`, `modifier_id`, `sort_order`) USING BTREE,
  CONSTRAINT `phppos_modifier_items_ibfk_1` FOREIGN KEY (`modifier_id`) REFERENCES `phppos_modifiers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_modifier_items
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_modifiers
-- ----------------------------
DROP TABLE IF EXISTS `phppos_modifiers`;
CREATE TABLE `phppos_modifiers`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `sort_order` int NOT NULL DEFAULT 0,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE,
  INDEX `sort_index`(`deleted`, `sort_order`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_modifiers
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_modules
-- ----------------------------
DROP TABLE IF EXISTS `phppos_modules`;
CREATE TABLE `phppos_modules`  (
  `name_lang_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `desc_lang_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sort` int NOT NULL,
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `module_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`module_id`) USING BTREE,
  UNIQUE INDEX `desc_lang_key`(`desc_lang_key`) USING BTREE,
  UNIQUE INDEX `name_lang_key`(`name_lang_key`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_modules
-- ----------------------------
INSERT INTO `phppos_modules` VALUES ('module_appointments', 'module_appointments_desc', 75, 'ti-calendar', 'appointments');
INSERT INTO `phppos_modules` VALUES ('module_config', 'module_config_desc', 100, 'icon ti-settings', 'config');
INSERT INTO `phppos_modules` VALUES ('module_customers', 'module_customers_desc', 10, 'icon ti-user', 'customers');
INSERT INTO `phppos_modules` VALUES ('module_deliveries', 'module_deliveries_desc', 71, 'ion-android-car', 'deliveries');
INSERT INTO `phppos_modules` VALUES ('module_employees', 'module_employees_desc', 80, 'icon ti-id-badge', 'employees');
INSERT INTO `phppos_modules` VALUES ('module_expenses', 'module_expenses_desc', 75, 'icon ti-money', 'expenses');
INSERT INTO `phppos_modules` VALUES ('module_giftcards', 'module_giftcards_desc', 90, 'icon ti-credit-card', 'giftcards');
INSERT INTO `phppos_modules` VALUES ('module_item_kits', 'module_item_kits_desc', 30, 'icon ti-harddrives', 'item_kits');
INSERT INTO `phppos_modules` VALUES ('module_items', 'module_items_desc', 20, 'icon ti-harddrive', 'items');
INSERT INTO `phppos_modules` VALUES ('module_locations', 'module_locations_desc', 110, 'icon ti-home', 'locations');
INSERT INTO `phppos_modules` VALUES ('module_messages', 'module_messages_desc', 120, 'icon ti-email', 'messages');
INSERT INTO `phppos_modules` VALUES ('module_price_rules', 'module_item_price_rules_desc', 35, 'ion-ios-pricetags-outline', 'price_rules');
INSERT INTO `phppos_modules` VALUES ('module_receivings', 'module_receivings_desc', 60, 'icon ti-cloud-down', 'receivings');
INSERT INTO `phppos_modules` VALUES ('module_reports', 'module_reports_desc', 50, 'icon ti-bar-chart', 'reports');
INSERT INTO `phppos_modules` VALUES ('module_sales', 'module_sales_desc', 70, 'icon ti-shopping-cart', 'sales');
INSERT INTO `phppos_modules` VALUES ('module_suppliers', 'module_suppliers_desc', 40, 'icon ti-download', 'suppliers');

-- ----------------------------
-- Table structure for phppos_modules_actions
-- ----------------------------
DROP TABLE IF EXISTS `phppos_modules_actions`;
CREATE TABLE `phppos_modules_actions`  (
  `action_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `module_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `action_name_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sort` int NOT NULL,
  PRIMARY KEY (`action_id`, `module_id`) USING BTREE,
  INDEX `phppos_modules_actions_ibfk_1`(`module_id`) USING BTREE,
  CONSTRAINT `phppos_modules_actions_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `phppos_modules` (`module_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_modules_actions
-- ----------------------------
INSERT INTO `phppos_modules_actions` VALUES ('add', 'appointments', 'appointments_add', 240);
INSERT INTO `phppos_modules_actions` VALUES ('add_update', 'customers', 'module_action_add_update', 1);
INSERT INTO `phppos_modules_actions` VALUES ('add_update', 'deliveries', 'deliveries_add_update', 240);
INSERT INTO `phppos_modules_actions` VALUES ('add_update', 'employees', 'module_action_add_update', 130);
INSERT INTO `phppos_modules_actions` VALUES ('add_update', 'expenses', 'module_expenses_add_update', 315);
INSERT INTO `phppos_modules_actions` VALUES ('add_update', 'giftcards', 'module_action_add_update', 200);
INSERT INTO `phppos_modules_actions` VALUES ('add_update', 'item_kits', 'module_action_add_update', 70);
INSERT INTO `phppos_modules_actions` VALUES ('add_update', 'items', 'module_action_add_update', 40);
INSERT INTO `phppos_modules_actions` VALUES ('add_update', 'locations', 'module_action_add_update', 240);
INSERT INTO `phppos_modules_actions` VALUES ('add_update', 'price_rules', 'module_action_add_update', 400);
INSERT INTO `phppos_modules_actions` VALUES ('add_update', 'suppliers', 'module_action_add_update', 100);
INSERT INTO `phppos_modules_actions` VALUES ('allow_customer_search_suggestions_for_sales', 'sales', 'sales_allow_customer_search_suggestions_for_sales', 302);
INSERT INTO `phppos_modules_actions` VALUES ('allow_item_search_suggestions_for_receivings', 'receivings', 'receivings_allow_item_search_suggestions_for_receivings', 301);
INSERT INTO `phppos_modules_actions` VALUES ('allow_item_search_suggestions_for_sales', 'sales', 'sales_allow_item_search_suggestions_for_sales', 300);
INSERT INTO `phppos_modules_actions` VALUES ('allow_supplier_search_suggestions_for_suppliers', 'receivings', 'receivings_allow_supplier_search_suggestions_for_suppliers', 303);
INSERT INTO `phppos_modules_actions` VALUES ('assign_all_locations', 'employees', 'module_action_assign_all_locations', 151);
INSERT INTO `phppos_modules_actions` VALUES ('can_edit_inventory_comment', 'items', 'items_can_edit_inventory_comment', 500);
INSERT INTO `phppos_modules_actions` VALUES ('can_lookup_receipt', 'sales', 'sales_can_lookup_receipt', 503);
INSERT INTO `phppos_modules_actions` VALUES ('change_sale_date', 'sales', 'sales_change_sale_date', 184);
INSERT INTO `phppos_modules_actions` VALUES ('complete_sale', 'sales', 'sales_complete_sale', 184);
INSERT INTO `phppos_modules_actions` VALUES ('complete_transfer', 'receivings', 'receivings_complete_transfer', 184);
INSERT INTO `phppos_modules_actions` VALUES ('count_inventory', 'items', 'items_count_inventory', 65);
INSERT INTO `phppos_modules_actions` VALUES ('delete', 'appointments', 'appointments_delete', 250);
INSERT INTO `phppos_modules_actions` VALUES ('delete', 'customers', 'module_action_delete', 20);
INSERT INTO `phppos_modules_actions` VALUES ('delete', 'deliveries', 'deliveries_delete', 250);
INSERT INTO `phppos_modules_actions` VALUES ('delete', 'employees', 'module_action_delete', 140);
INSERT INTO `phppos_modules_actions` VALUES ('delete', 'expenses', 'module_expenses_delete', 330);
INSERT INTO `phppos_modules_actions` VALUES ('delete', 'giftcards', 'module_action_delete', 210);
INSERT INTO `phppos_modules_actions` VALUES ('delete', 'item_kits', 'module_action_delete', 80);
INSERT INTO `phppos_modules_actions` VALUES ('delete', 'items', 'module_action_delete', 50);
INSERT INTO `phppos_modules_actions` VALUES ('delete', 'locations', 'module_action_delete', 250);
INSERT INTO `phppos_modules_actions` VALUES ('delete', 'price_rules', 'module_action_delete', 405);
INSERT INTO `phppos_modules_actions` VALUES ('delete', 'suppliers', 'module_action_delete', 110);
INSERT INTO `phppos_modules_actions` VALUES ('delete_receiving', 'receivings', 'module_action_delete_receiving', 306);
INSERT INTO `phppos_modules_actions` VALUES ('delete_register_log', 'reports', 'common_delete_register_log', 232);
INSERT INTO `phppos_modules_actions` VALUES ('delete_sale', 'sales', 'module_action_delete_sale', 230);
INSERT INTO `phppos_modules_actions` VALUES ('delete_suspended_sale', 'sales', 'module_action_delete_suspended_sale', 181);
INSERT INTO `phppos_modules_actions` VALUES ('delete_taxes', 'receivings', 'module_action_delete_taxes', 300);
INSERT INTO `phppos_modules_actions` VALUES ('delete_taxes', 'sales', 'module_action_delete_taxes', 182);
INSERT INTO `phppos_modules_actions` VALUES ('edit', 'appointments', 'appointments_edit', 245);
INSERT INTO `phppos_modules_actions` VALUES ('edit', 'deliveries', 'deliveries_edit', 245);
INSERT INTO `phppos_modules_actions` VALUES ('edit_customer_points', 'customers', 'module_edit_customer_points', 35);
INSERT INTO `phppos_modules_actions` VALUES ('edit_giftcard_value', 'giftcards', 'module_edit_giftcard_value', 205);
INSERT INTO `phppos_modules_actions` VALUES ('edit_prices', 'item_kits', 'common_edit_prices', 502);
INSERT INTO `phppos_modules_actions` VALUES ('edit_prices', 'items', 'common_edit_prices', 501);
INSERT INTO `phppos_modules_actions` VALUES ('edit_profile', 'employees', 'common_edit_profile', 155);
INSERT INTO `phppos_modules_actions` VALUES ('edit_quantity', 'items', 'items_edit_quantity', 62);
INSERT INTO `phppos_modules_actions` VALUES ('edit_receiving', 'receivings', 'module_action_edit_receiving', 303);
INSERT INTO `phppos_modules_actions` VALUES ('edit_register_log', 'reports', 'common_edit_register_log', 231);
INSERT INTO `phppos_modules_actions` VALUES ('edit_sale', 'sales', 'module_edit_sale', 190);
INSERT INTO `phppos_modules_actions` VALUES ('edit_sale_cost_price', 'sales', 'module_edit_sale_cost_price', 175);
INSERT INTO `phppos_modules_actions` VALUES ('edit_sale_price', 'sales', 'module_edit_sale_price', 170);
INSERT INTO `phppos_modules_actions` VALUES ('edit_store_account_balance', 'customers', 'customers_edit_store_account_balance', 31);
INSERT INTO `phppos_modules_actions` VALUES ('edit_store_account_balance', 'suppliers', 'suppliers_edit_store_account_balance', 130);
INSERT INTO `phppos_modules_actions` VALUES ('edit_suspended_sale', 'sales', 'sales_edit_suspended_sale', 192);
INSERT INTO `phppos_modules_actions` VALUES ('edit_taxes', 'receivings', 'module_edit_taxes', 304);
INSERT INTO `phppos_modules_actions` VALUES ('edit_taxes', 'sales', 'module_edit_taxes', 191);
INSERT INTO `phppos_modules_actions` VALUES ('edit_tier', 'customers', 'customers_edit_tier', 45);
INSERT INTO `phppos_modules_actions` VALUES ('excel_export', 'customers', 'common_excel_export', 40);
INSERT INTO `phppos_modules_actions` VALUES ('excel_export', 'employees', 'common_excel_export', 160);
INSERT INTO `phppos_modules_actions` VALUES ('excel_export', 'giftcards', 'common_excel_export', 225);
INSERT INTO `phppos_modules_actions` VALUES ('excel_export', 'item_kits', 'common_excel_export', 95);
INSERT INTO `phppos_modules_actions` VALUES ('excel_export', 'items', 'common_excel_export', 80);
INSERT INTO `phppos_modules_actions` VALUES ('excel_export', 'suppliers', 'common_excel_export', 135);
INSERT INTO `phppos_modules_actions` VALUES ('give_discount', 'sales', 'module_give_discount', 180);
INSERT INTO `phppos_modules_actions` VALUES ('manage_categories', 'items', 'items_manage_categories', 70);
INSERT INTO `phppos_modules_actions` VALUES ('manage_manufacturers', 'items', 'items_manage_manufacturers', 76);
INSERT INTO `phppos_modules_actions` VALUES ('manage_tags', 'items', 'items_manage_tags', 75);
INSERT INTO `phppos_modules_actions` VALUES ('process_returns', 'sales', 'config_process_returns', 184);
INSERT INTO `phppos_modules_actions` VALUES ('receive_store_account_payment', 'receivings', 'common_receive_store_account_payment', 260);
INSERT INTO `phppos_modules_actions` VALUES ('receive_store_account_payment', 'sales', 'common_receive_store_account_payment', 255);
INSERT INTO `phppos_modules_actions` VALUES ('search', 'appointments', 'appointments_search', 255);
INSERT INTO `phppos_modules_actions` VALUES ('search', 'customers', 'module_action_search_customers', 30);
INSERT INTO `phppos_modules_actions` VALUES ('search', 'deliveries', 'deliveries_search', 255);
INSERT INTO `phppos_modules_actions` VALUES ('search', 'employees', 'module_action_search_employees', 150);
INSERT INTO `phppos_modules_actions` VALUES ('search', 'expenses', 'module_expenses_search', 310);
INSERT INTO `phppos_modules_actions` VALUES ('search', 'giftcards', 'module_action_search_giftcards', 220);
INSERT INTO `phppos_modules_actions` VALUES ('search', 'item_kits', 'module_action_search_item_kits', 90);
INSERT INTO `phppos_modules_actions` VALUES ('search', 'items', 'module_action_search_items', 60);
INSERT INTO `phppos_modules_actions` VALUES ('search', 'locations', 'module_action_search_locations', 260);
INSERT INTO `phppos_modules_actions` VALUES ('search', 'price_rules', 'module_action_search_price_rules', 415);
INSERT INTO `phppos_modules_actions` VALUES ('search', 'sales', 'module_action_search_sales', 235);
INSERT INTO `phppos_modules_actions` VALUES ('search', 'suppliers', 'module_action_search_suppliers', 120);
INSERT INTO `phppos_modules_actions` VALUES ('see_cost_price', 'item_kits', 'module_see_cost_price', 91);
INSERT INTO `phppos_modules_actions` VALUES ('see_cost_price', 'items', 'module_see_cost_price', 61);
INSERT INTO `phppos_modules_actions` VALUES ('see_count_when_count_inventory', 'items', 'items_see_count_when_count_inventory', 66);
INSERT INTO `phppos_modules_actions` VALUES ('send_message', 'messages', 'employees_send_message', 350);
INSERT INTO `phppos_modules_actions` VALUES ('send_transfer', 'receivings', 'receivings_send_transfer', 185);
INSERT INTO `phppos_modules_actions` VALUES ('show_cost_price', 'reports', 'reports_show_cost_price', 290);
INSERT INTO `phppos_modules_actions` VALUES ('show_profit', 'reports', 'reports_show_profit', 280);
INSERT INTO `phppos_modules_actions` VALUES ('suspend_sale', 'sales', 'sales_suspend_sale', 183);
INSERT INTO `phppos_modules_actions` VALUES ('view_all_employee_commissions', 'reports', 'reports_view_all_employee_commissions', 107);
INSERT INTO `phppos_modules_actions` VALUES ('view_appointments', 'reports', 'reports_appointments', 95);
INSERT INTO `phppos_modules_actions` VALUES ('view_categories', 'reports', 'reports_categories', 100);
INSERT INTO `phppos_modules_actions` VALUES ('view_closeout', 'reports', 'reports_closeout', 105);
INSERT INTO `phppos_modules_actions` VALUES ('view_commissions', 'reports', 'reports_commission', 106);
INSERT INTO `phppos_modules_actions` VALUES ('view_customers', 'reports', 'reports_customers', 120);
INSERT INTO `phppos_modules_actions` VALUES ('view_dashboard_stats', 'reports', 'reports_view_dashboard_stats', 300);
INSERT INTO `phppos_modules_actions` VALUES ('view_deleted_sales', 'reports', 'reports_deleted_sales', 130);
INSERT INTO `phppos_modules_actions` VALUES ('view_deliveries', 'reports', 'reports_deliveries', 135);
INSERT INTO `phppos_modules_actions` VALUES ('view_discounts', 'reports', 'reports_discounts', 140);
INSERT INTO `phppos_modules_actions` VALUES ('view_employees', 'reports', 'reports_employees', 150);
INSERT INTO `phppos_modules_actions` VALUES ('view_expenses', 'reports', 'module_expenses_report', 155);
INSERT INTO `phppos_modules_actions` VALUES ('view_giftcards', 'reports', 'reports_giftcards', 160);
INSERT INTO `phppos_modules_actions` VALUES ('view_inventory_at_all_locations', 'items', 'common_view_inventory_at_all_locations', 268);
INSERT INTO `phppos_modules_actions` VALUES ('view_inventory_at_all_locations', 'reports', 'reports_view_inventory_at_all_locations', 300);
INSERT INTO `phppos_modules_actions` VALUES ('view_inventory_print_list', 'items', 'common_view_inventory_print_list', 267);
INSERT INTO `phppos_modules_actions` VALUES ('view_inventory_reports', 'reports', 'reports_inventory_reports', 170);
INSERT INTO `phppos_modules_actions` VALUES ('view_item_kits', 'reports', 'module_item_kits', 180);
INSERT INTO `phppos_modules_actions` VALUES ('view_items', 'reports', 'reports_items', 190);
INSERT INTO `phppos_modules_actions` VALUES ('view_manufacturers', 'reports', 'reports_manufacturers', 195);
INSERT INTO `phppos_modules_actions` VALUES ('view_payments', 'reports', 'reports_payments', 200);
INSERT INTO `phppos_modules_actions` VALUES ('view_price_rules', 'reports', 'reports_price_rules', 205);
INSERT INTO `phppos_modules_actions` VALUES ('view_profit_and_loss', 'reports', 'reports_profit_and_loss', 210);
INSERT INTO `phppos_modules_actions` VALUES ('view_receivings', 'reports', 'reports_receivings', 220);
INSERT INTO `phppos_modules_actions` VALUES ('view_register_log', 'reports', 'reports_register_log_title', 230);
INSERT INTO `phppos_modules_actions` VALUES ('view_registers', 'reports', 'reports_registers', 235);
INSERT INTO `phppos_modules_actions` VALUES ('view_sales', 'reports', 'reports_sales', 240);
INSERT INTO `phppos_modules_actions` VALUES ('view_sales_generator', 'reports', 'reports_sales_generator', 110);
INSERT INTO `phppos_modules_actions` VALUES ('view_store_account', 'reports', 'reports_store_account', 250);
INSERT INTO `phppos_modules_actions` VALUES ('view_store_account_suppliers', 'reports', 'reports_store_account_suppliers', 255);
INSERT INTO `phppos_modules_actions` VALUES ('view_suppliers', 'reports', 'reports_suppliers', 260);
INSERT INTO `phppos_modules_actions` VALUES ('view_suspended_sales', 'reports', 'reports_suspended_sales', 261);
INSERT INTO `phppos_modules_actions` VALUES ('view_tags', 'reports', 'common_tags', 264);
INSERT INTO `phppos_modules_actions` VALUES ('view_taxes', 'reports', 'reports_taxes', 270);
INSERT INTO `phppos_modules_actions` VALUES ('view_tiers', 'reports', 'reports_tiers', 275);
INSERT INTO `phppos_modules_actions` VALUES ('view_timeclock', 'reports', 'employees_timeclock', 280);

-- ----------------------------
-- Table structure for phppos_people
-- ----------------------------
DROP TABLE IF EXISTS `phppos_people`;
CREATE TABLE `phppos_people`  (
  `first_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `full_name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `phone_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `address_1` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `address_2` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `state` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `zip` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `country` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `comments` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `image_id` int NULL DEFAULT NULL,
  `person_id` int NOT NULL AUTO_INCREMENT,
  `last_modified` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`person_id`) USING BTREE,
  INDEX `phppos_people_ibfk_1`(`image_id`) USING BTREE,
  INDEX `first_name`(`first_name`) USING BTREE,
  INDEX `last_name`(`last_name`) USING BTREE,
  INDEX `email`(`email`) USING BTREE,
  INDEX `phone_number`(`phone_number`) USING BTREE,
  INDEX `full_name`(`full_name`(255)) USING BTREE,
  CONSTRAINT `phppos_people_ibfk_1` FOREIGN KEY (`image_id`) REFERENCES `phppos_app_files` (`file_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_people
-- ----------------------------
INSERT INTO `phppos_people` VALUES ('John', 'Doe', 'John Doe', '555-555-5555', 'no-reply@phppointofsale.com', 'Address 1', '', '', '', '', '', '', NULL, 1, NULL, NULL, NULL);
INSERT INTO `phppos_people` VALUES ('dsf', 'dsf', '', '', '', '', '', '', '', '', '', '', NULL, 2, NULL, NULL, NULL);
INSERT INTO `phppos_people` VALUES ('asdasd', 'asdasd', NULL, NULL, 'chsokunthea@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, '2021-04-03 17:14:06', '2021-04-03 17:14:06');
INSERT INTO `phppos_people` VALUES ('Roth', 'Phearom', NULL, NULL, 'chsokunthea@gmail.com', 'asd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, '2021-04-03 17:14:21', '2021-04-04 08:48:53');
INSERT INTO `phppos_people` VALUES ('General', 'TITB2', NULL, '010446162', 'phearom@titb.biz', 'PP', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, '2021-04-03 17:15:29', '2021-04-03 17:15:29');
INSERT INTO `phppos_people` VALUES ('General', 'Phearom', NULL, NULL, 'phearom@gmail.com', 'sdfsf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, NULL, '2021-04-04 09:22:38', '2021-04-04 09:22:38');
INSERT INTO `phppos_people` VALUES ('General', 'Phearom', NULL, '010446162', 'phearom@gmail.com', 'sdfsf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, NULL, '2021-04-04 09:23:33', '2021-04-04 09:24:00');
INSERT INTO `phppos_people` VALUES ('General1', 'KH', NULL, NULL, 'tevengkheang1@gmail.com', 'Phnom', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 14, NULL, '2021-04-04 10:16:44', '2021-04-04 10:16:44');
INSERT INTO `phppos_people` VALUES ('General1', 'General', NULL, NULL, 'stellar.supermarket@gmail.com', 'fddsf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 16, NULL, '2021-04-04 10:17:38', '2021-04-04 10:17:38');
INSERT INTO `phppos_people` VALUES ('sfsdfds', 'sdfsdf', NULL, NULL, 'chsokunthea@gmail.com', 'sfdsd', NULL, 'PP', NULL, NULL, NULL, NULL, NULL, 17, NULL, '2021-04-04 10:19:29', '2021-04-04 13:32:12');
INSERT INTO `phppos_people` VALUES ('asdsad', 'asda', NULL, NULL, 'stellar.supermarket@gmail.com', 'asdsa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, NULL, '2021-04-04 10:23:40', '2021-04-04 10:23:40');
INSERT INTO `phppos_people` VALUES ('Phearom12', 'rothj', NULL, NULL, 'chsokunthea@gmail.com', 'dsfs', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 19, NULL, '2021-04-04 10:24:04', '2021-04-04 10:42:42');
INSERT INTO `phppos_people` VALUES ('General12', '121', NULL, NULL, 'sphat8@gmail.com', 'asda', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 20, NULL, '2021-04-04 10:28:11', '2021-04-04 10:28:11');
INSERT INTO `phppos_people` VALUES ('General12', 'TITB', NULL, NULL, 'stellar.supermarket@gmail.com', '152135', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 21, NULL, '2021-04-04 13:08:58', '2021-04-04 13:09:24');

-- ----------------------------
-- Table structure for phppos_people_files
-- ----------------------------
DROP TABLE IF EXISTS `phppos_people_files`;
CREATE TABLE `phppos_people_files`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `file_id` int NOT NULL,
  `person_id` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_people_files_ibfk_1`(`file_id`) USING BTREE,
  CONSTRAINT `phppos_people_files_ibfk_1` FOREIGN KEY (`file_id`) REFERENCES `phppos_app_files` (`file_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_people_files
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_permissions
-- ----------------------------
DROP TABLE IF EXISTS `phppos_permissions`;
CREATE TABLE `phppos_permissions`  (
  `module_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `person_id` int NOT NULL,
  PRIMARY KEY (`module_id`, `person_id`) USING BTREE,
  INDEX `person_id`(`person_id`) USING BTREE,
  CONSTRAINT `phppos_permissions_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `phppos_employees` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_permissions_ibfk_2` FOREIGN KEY (`module_id`) REFERENCES `phppos_modules` (`module_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_permissions
-- ----------------------------
INSERT INTO `phppos_permissions` VALUES ('appointments', 1);
INSERT INTO `phppos_permissions` VALUES ('config', 1);
INSERT INTO `phppos_permissions` VALUES ('customers', 1);
INSERT INTO `phppos_permissions` VALUES ('deliveries', 1);
INSERT INTO `phppos_permissions` VALUES ('employees', 1);
INSERT INTO `phppos_permissions` VALUES ('expenses', 1);
INSERT INTO `phppos_permissions` VALUES ('giftcards', 1);
INSERT INTO `phppos_permissions` VALUES ('item_kits', 1);
INSERT INTO `phppos_permissions` VALUES ('items', 1);
INSERT INTO `phppos_permissions` VALUES ('locations', 1);
INSERT INTO `phppos_permissions` VALUES ('messages', 1);
INSERT INTO `phppos_permissions` VALUES ('price_rules', 1);
INSERT INTO `phppos_permissions` VALUES ('receivings', 1);
INSERT INTO `phppos_permissions` VALUES ('reports', 1);
INSERT INTO `phppos_permissions` VALUES ('sales', 1);
INSERT INTO `phppos_permissions` VALUES ('suppliers', 1);

-- ----------------------------
-- Table structure for phppos_permissions_actions
-- ----------------------------
DROP TABLE IF EXISTS `phppos_permissions_actions`;
CREATE TABLE `phppos_permissions_actions`  (
  `module_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `person_id` int NOT NULL,
  `action_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`module_id`, `person_id`, `action_id`) USING BTREE,
  INDEX `phppos_permissions_actions_ibfk_2`(`person_id`) USING BTREE,
  INDEX `phppos_permissions_actions_ibfk_3`(`action_id`) USING BTREE,
  CONSTRAINT `phppos_permissions_actions_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `phppos_modules` (`module_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_permissions_actions_ibfk_2` FOREIGN KEY (`person_id`) REFERENCES `phppos_employees` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_permissions_actions_ibfk_3` FOREIGN KEY (`action_id`) REFERENCES `phppos_modules_actions` (`action_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_permissions_actions
-- ----------------------------
INSERT INTO `phppos_permissions_actions` VALUES ('appointments', 1, 'add');
INSERT INTO `phppos_permissions_actions` VALUES ('appointments', 1, 'delete');
INSERT INTO `phppos_permissions_actions` VALUES ('appointments', 1, 'edit');
INSERT INTO `phppos_permissions_actions` VALUES ('appointments', 1, 'search');
INSERT INTO `phppos_permissions_actions` VALUES ('customers', 1, 'add_update');
INSERT INTO `phppos_permissions_actions` VALUES ('customers', 1, 'delete');
INSERT INTO `phppos_permissions_actions` VALUES ('customers', 1, 'edit_customer_points');
INSERT INTO `phppos_permissions_actions` VALUES ('customers', 1, 'edit_store_account_balance');
INSERT INTO `phppos_permissions_actions` VALUES ('customers', 1, 'edit_tier');
INSERT INTO `phppos_permissions_actions` VALUES ('customers', 1, 'excel_export');
INSERT INTO `phppos_permissions_actions` VALUES ('customers', 1, 'search');
INSERT INTO `phppos_permissions_actions` VALUES ('deliveries', 1, 'add_update');
INSERT INTO `phppos_permissions_actions` VALUES ('deliveries', 1, 'delete');
INSERT INTO `phppos_permissions_actions` VALUES ('deliveries', 1, 'edit');
INSERT INTO `phppos_permissions_actions` VALUES ('deliveries', 1, 'search');
INSERT INTO `phppos_permissions_actions` VALUES ('employees', 1, 'add_update');
INSERT INTO `phppos_permissions_actions` VALUES ('employees', 1, 'assign_all_locations');
INSERT INTO `phppos_permissions_actions` VALUES ('employees', 1, 'delete');
INSERT INTO `phppos_permissions_actions` VALUES ('employees', 1, 'edit_profile');
INSERT INTO `phppos_permissions_actions` VALUES ('employees', 1, 'excel_export');
INSERT INTO `phppos_permissions_actions` VALUES ('employees', 1, 'search');
INSERT INTO `phppos_permissions_actions` VALUES ('expenses', 1, 'add_update');
INSERT INTO `phppos_permissions_actions` VALUES ('expenses', 1, 'delete');
INSERT INTO `phppos_permissions_actions` VALUES ('expenses', 1, 'search');
INSERT INTO `phppos_permissions_actions` VALUES ('giftcards', 1, 'add_update');
INSERT INTO `phppos_permissions_actions` VALUES ('giftcards', 1, 'delete');
INSERT INTO `phppos_permissions_actions` VALUES ('giftcards', 1, 'edit_giftcard_value');
INSERT INTO `phppos_permissions_actions` VALUES ('giftcards', 1, 'excel_export');
INSERT INTO `phppos_permissions_actions` VALUES ('giftcards', 1, 'search');
INSERT INTO `phppos_permissions_actions` VALUES ('item_kits', 1, 'add_update');
INSERT INTO `phppos_permissions_actions` VALUES ('item_kits', 1, 'delete');
INSERT INTO `phppos_permissions_actions` VALUES ('item_kits', 1, 'edit_prices');
INSERT INTO `phppos_permissions_actions` VALUES ('item_kits', 1, 'excel_export');
INSERT INTO `phppos_permissions_actions` VALUES ('item_kits', 1, 'search');
INSERT INTO `phppos_permissions_actions` VALUES ('item_kits', 1, 'see_cost_price');
INSERT INTO `phppos_permissions_actions` VALUES ('items', 1, 'add_update');
INSERT INTO `phppos_permissions_actions` VALUES ('items', 1, 'can_edit_inventory_comment');
INSERT INTO `phppos_permissions_actions` VALUES ('items', 1, 'count_inventory');
INSERT INTO `phppos_permissions_actions` VALUES ('items', 1, 'delete');
INSERT INTO `phppos_permissions_actions` VALUES ('items', 1, 'edit_prices');
INSERT INTO `phppos_permissions_actions` VALUES ('items', 1, 'edit_quantity');
INSERT INTO `phppos_permissions_actions` VALUES ('items', 1, 'excel_export');
INSERT INTO `phppos_permissions_actions` VALUES ('items', 1, 'manage_categories');
INSERT INTO `phppos_permissions_actions` VALUES ('items', 1, 'manage_manufacturers');
INSERT INTO `phppos_permissions_actions` VALUES ('items', 1, 'manage_tags');
INSERT INTO `phppos_permissions_actions` VALUES ('items', 1, 'search');
INSERT INTO `phppos_permissions_actions` VALUES ('items', 1, 'see_cost_price');
INSERT INTO `phppos_permissions_actions` VALUES ('items', 1, 'see_count_when_count_inventory');
INSERT INTO `phppos_permissions_actions` VALUES ('items', 1, 'view_inventory_at_all_locations');
INSERT INTO `phppos_permissions_actions` VALUES ('items', 1, 'view_inventory_print_list');
INSERT INTO `phppos_permissions_actions` VALUES ('locations', 1, 'add_update');
INSERT INTO `phppos_permissions_actions` VALUES ('locations', 1, 'delete');
INSERT INTO `phppos_permissions_actions` VALUES ('locations', 1, 'search');
INSERT INTO `phppos_permissions_actions` VALUES ('messages', 1, 'send_message');
INSERT INTO `phppos_permissions_actions` VALUES ('price_rules', 1, 'add_update');
INSERT INTO `phppos_permissions_actions` VALUES ('price_rules', 1, 'delete');
INSERT INTO `phppos_permissions_actions` VALUES ('price_rules', 1, 'search');
INSERT INTO `phppos_permissions_actions` VALUES ('receivings', 1, 'allow_item_search_suggestions_for_receivings');
INSERT INTO `phppos_permissions_actions` VALUES ('receivings', 1, 'allow_supplier_search_suggestions_for_suppliers');
INSERT INTO `phppos_permissions_actions` VALUES ('receivings', 1, 'complete_transfer');
INSERT INTO `phppos_permissions_actions` VALUES ('receivings', 1, 'delete_receiving');
INSERT INTO `phppos_permissions_actions` VALUES ('receivings', 1, 'delete_taxes');
INSERT INTO `phppos_permissions_actions` VALUES ('receivings', 1, 'edit_receiving');
INSERT INTO `phppos_permissions_actions` VALUES ('receivings', 1, 'edit_taxes');
INSERT INTO `phppos_permissions_actions` VALUES ('receivings', 1, 'receive_store_account_payment');
INSERT INTO `phppos_permissions_actions` VALUES ('receivings', 1, 'send_transfer');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'delete_register_log');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'edit_register_log');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'show_cost_price');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'show_profit');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_all_employee_commissions');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_appointments');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_categories');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_closeout');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_commissions');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_customers');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_dashboard_stats');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_deleted_sales');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_deliveries');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_discounts');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_employees');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_expenses');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_giftcards');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_inventory_at_all_locations');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_inventory_reports');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_item_kits');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_items');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_manufacturers');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_payments');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_price_rules');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_profit_and_loss');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_receivings');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_register_log');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_registers');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_sales');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_sales_generator');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_store_account');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_store_account_suppliers');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_suppliers');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_suspended_sales');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_tags');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_taxes');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_tiers');
INSERT INTO `phppos_permissions_actions` VALUES ('reports', 1, 'view_timeclock');
INSERT INTO `phppos_permissions_actions` VALUES ('sales', 1, 'allow_customer_search_suggestions_for_sales');
INSERT INTO `phppos_permissions_actions` VALUES ('sales', 1, 'allow_item_search_suggestions_for_sales');
INSERT INTO `phppos_permissions_actions` VALUES ('sales', 1, 'can_lookup_receipt');
INSERT INTO `phppos_permissions_actions` VALUES ('sales', 1, 'change_sale_date');
INSERT INTO `phppos_permissions_actions` VALUES ('sales', 1, 'complete_sale');
INSERT INTO `phppos_permissions_actions` VALUES ('sales', 1, 'delete_sale');
INSERT INTO `phppos_permissions_actions` VALUES ('sales', 1, 'delete_suspended_sale');
INSERT INTO `phppos_permissions_actions` VALUES ('sales', 1, 'delete_taxes');
INSERT INTO `phppos_permissions_actions` VALUES ('sales', 1, 'edit_sale');
INSERT INTO `phppos_permissions_actions` VALUES ('sales', 1, 'edit_sale_cost_price');
INSERT INTO `phppos_permissions_actions` VALUES ('sales', 1, 'edit_sale_price');
INSERT INTO `phppos_permissions_actions` VALUES ('sales', 1, 'edit_suspended_sale');
INSERT INTO `phppos_permissions_actions` VALUES ('sales', 1, 'edit_taxes');
INSERT INTO `phppos_permissions_actions` VALUES ('sales', 1, 'give_discount');
INSERT INTO `phppos_permissions_actions` VALUES ('sales', 1, 'process_returns');
INSERT INTO `phppos_permissions_actions` VALUES ('sales', 1, 'receive_store_account_payment');
INSERT INTO `phppos_permissions_actions` VALUES ('sales', 1, 'search');
INSERT INTO `phppos_permissions_actions` VALUES ('sales', 1, 'suspend_sale');
INSERT INTO `phppos_permissions_actions` VALUES ('suppliers', 1, 'add_update');
INSERT INTO `phppos_permissions_actions` VALUES ('suppliers', 1, 'delete');
INSERT INTO `phppos_permissions_actions` VALUES ('suppliers', 1, 'edit_store_account_balance');
INSERT INTO `phppos_permissions_actions` VALUES ('suppliers', 1, 'excel_export');
INSERT INTO `phppos_permissions_actions` VALUES ('suppliers', 1, 'search');

-- ----------------------------
-- Table structure for phppos_permissions_actions_locations
-- ----------------------------
DROP TABLE IF EXISTS `phppos_permissions_actions_locations`;
CREATE TABLE `phppos_permissions_actions_locations`  (
  `module_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `person_id` int NOT NULL,
  `action_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `location_id` int NOT NULL,
  PRIMARY KEY (`module_id`, `person_id`, `action_id`, `location_id`) USING BTREE,
  INDEX `phppos_permissions_actions_locations_ibfk_2`(`person_id`) USING BTREE,
  INDEX `phppos_permissions_actions_locations_ibfk_3`(`action_id`) USING BTREE,
  INDEX `phppos_permissions_actions_locations_ibfk_4`(`location_id`) USING BTREE,
  CONSTRAINT `phppos_permissions_actions_locations_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `phppos_modules` (`module_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_permissions_actions_locations_ibfk_2` FOREIGN KEY (`person_id`) REFERENCES `phppos_employees` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_permissions_actions_locations_ibfk_3` FOREIGN KEY (`action_id`) REFERENCES `phppos_modules_actions` (`action_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_permissions_actions_locations_ibfk_4` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_permissions_actions_locations
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_permissions_locations
-- ----------------------------
DROP TABLE IF EXISTS `phppos_permissions_locations`;
CREATE TABLE `phppos_permissions_locations`  (
  `module_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `person_id` int NOT NULL,
  `location_id` int NOT NULL,
  PRIMARY KEY (`module_id`, `person_id`, `location_id`) USING BTREE,
  INDEX `phppos_permissions_locations_ibfk_1`(`person_id`) USING BTREE,
  INDEX `phppos_permissions_locations_ibfk_3`(`location_id`) USING BTREE,
  CONSTRAINT `phppos_permissions_locations_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `phppos_employees` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_permissions_locations_ibfk_2` FOREIGN KEY (`module_id`) REFERENCES `phppos_modules` (`module_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_permissions_locations_ibfk_3` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_permissions_locations
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_permissions_template
-- ----------------------------
DROP TABLE IF EXISTS `phppos_permissions_template`;
CREATE TABLE `phppos_permissions_template`  (
  `template_id` int NOT NULL,
  `module_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`template_id`, `module_id`) USING BTREE,
  INDEX `phppos_permissions_template_ibfk_1`(`module_id`) USING BTREE,
  CONSTRAINT `phppos_permissions_template_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `phppos_modules` (`module_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_permissions_template_ibfk_2` FOREIGN KEY (`template_id`) REFERENCES `phppos_permissions_templates` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_permissions_template
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_permissions_template_actions
-- ----------------------------
DROP TABLE IF EXISTS `phppos_permissions_template_actions`;
CREATE TABLE `phppos_permissions_template_actions`  (
  `template_id` int NOT NULL,
  `module_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `action_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`template_id`, `module_id`, `action_id`) USING BTREE,
  INDEX `phppos_permissions_template_actions_ibfk_2`(`action_id`) USING BTREE,
  INDEX `phppos_permissions_template_actions_ibfk_3`(`template_id`) USING BTREE,
  INDEX `phppos_permissions_template_actions_ibfk_1`(`module_id`) USING BTREE,
  CONSTRAINT `phppos_permissions_template_actions_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `phppos_modules` (`module_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_permissions_template_actions_ibfk_2` FOREIGN KEY (`action_id`) REFERENCES `phppos_modules_actions` (`action_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_permissions_template_actions_ibfk_3` FOREIGN KEY (`template_id`) REFERENCES `phppos_permissions_templates` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_permissions_template_actions
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_permissions_template_actions_locations
-- ----------------------------
DROP TABLE IF EXISTS `phppos_permissions_template_actions_locations`;
CREATE TABLE `phppos_permissions_template_actions_locations`  (
  `template_id` int NOT NULL,
  `module_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `action_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `location_id` int NOT NULL,
  PRIMARY KEY (`template_id`, `module_id`, `action_id`, `location_id`) USING BTREE,
  INDEX `phppos_permissions_template_actions_locations_ibfk_2`(`action_id`) USING BTREE,
  INDEX `phppos_permissions_template_actions_locations_ibfk_3`(`location_id`) USING BTREE,
  INDEX `phppos_permissions_template_actions_locations_ibfk_4`(`template_id`) USING BTREE,
  INDEX `phppos_permissions_template_actions_locations_ibfk_1`(`module_id`) USING BTREE,
  CONSTRAINT `phppos_permissions_template_actions_locations_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `phppos_modules` (`module_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_permissions_template_actions_locations_ibfk_2` FOREIGN KEY (`action_id`) REFERENCES `phppos_modules_actions` (`action_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_permissions_template_actions_locations_ibfk_3` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_permissions_template_actions_locations_ibfk_4` FOREIGN KEY (`template_id`) REFERENCES `phppos_permissions_templates` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_permissions_template_actions_locations
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_permissions_template_locations
-- ----------------------------
DROP TABLE IF EXISTS `phppos_permissions_template_locations`;
CREATE TABLE `phppos_permissions_template_locations`  (
  `template_id` int NOT NULL,
  `module_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `location_id` int NOT NULL,
  PRIMARY KEY (`template_id`, `module_id`, `location_id`) USING BTREE,
  INDEX `phppos_permissions_template_locations_ibfk_2`(`location_id`) USING BTREE,
  INDEX `phppos_permissions_template_locations_ibfk_3`(`template_id`) USING BTREE,
  INDEX `phppos_permissions_template_locations_ibfk_1`(`module_id`) USING BTREE,
  CONSTRAINT `phppos_permissions_template_locations_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `phppos_modules` (`module_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_permissions_template_locations_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_permissions_template_locations_ibfk_3` FOREIGN KEY (`template_id`) REFERENCES `phppos_permissions_templates` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_permissions_template_locations
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_permissions_templates
-- ----------------------------
DROP TABLE IF EXISTS `phppos_permissions_templates`;
CREATE TABLE `phppos_permissions_templates`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `deleted`(`deleted`) USING BTREE,
  INDEX `name`(`name`) USING BTREE,
  INDEX `name_deleted`(`name`, `deleted`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_permissions_templates
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_price_rules
-- ----------------------------
DROP TABLE IF EXISTS `phppos_price_rules`;
CREATE TABLE `phppos_price_rules`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `start_date` timestamp(0) NULL DEFAULT NULL,
  `end_date` timestamp(0) NULL DEFAULT NULL,
  `added_on` timestamp(0) NULL DEFAULT current_timestamp(0),
  `active` int NOT NULL DEFAULT 1,
  `deleted` int NOT NULL DEFAULT 0,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `items_to_buy` decimal(23, 10) NULL DEFAULT NULL,
  `items_to_get` decimal(23, 10) NULL DEFAULT NULL,
  `percent_off` decimal(23, 10) NULL DEFAULT NULL,
  `fixed_off` decimal(23, 10) NULL DEFAULT NULL,
  `spend_amount` decimal(23, 10) NULL DEFAULT NULL,
  `num_times_to_apply` int NOT NULL,
  `coupon_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `show_on_receipt` int NOT NULL DEFAULT 0,
  `coupon_spend_amount` decimal(23, 10) NULL DEFAULT NULL,
  `mix_and_match` int NOT NULL DEFAULT 0,
  `disable_loyalty_for_rule` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `name`(`name`) USING BTREE,
  INDEX `start_date`(`start_date`) USING BTREE,
  INDEX `end_date`(`end_date`) USING BTREE,
  INDEX `type`(`type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_price_rules
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_price_rules_categories
-- ----------------------------
DROP TABLE IF EXISTS `phppos_price_rules_categories`;
CREATE TABLE `phppos_price_rules_categories`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `rule_id` int NOT NULL,
  `category_id` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_price_rules_categories_ibfk_1`(`rule_id`) USING BTREE,
  INDEX `phppos_price_rules_categories_ibfk_2`(`category_id`) USING BTREE,
  CONSTRAINT `phppos_price_rules_categories_ibfk_1` FOREIGN KEY (`rule_id`) REFERENCES `phppos_price_rules` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_price_rules_categories_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `phppos_categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_price_rules_categories
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_price_rules_item_kits
-- ----------------------------
DROP TABLE IF EXISTS `phppos_price_rules_item_kits`;
CREATE TABLE `phppos_price_rules_item_kits`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `rule_id` int NOT NULL,
  `item_kit_id` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_price_rules_item_kits_ibfk_1`(`rule_id`) USING BTREE,
  INDEX `phppos_price_rules_item_kits_ibfk_2`(`item_kit_id`) USING BTREE,
  CONSTRAINT `phppos_price_rules_item_kits_ibfk_1` FOREIGN KEY (`rule_id`) REFERENCES `phppos_price_rules` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_price_rules_item_kits_ibfk_2` FOREIGN KEY (`item_kit_id`) REFERENCES `phppos_item_kits` (`item_kit_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_price_rules_item_kits
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_price_rules_items
-- ----------------------------
DROP TABLE IF EXISTS `phppos_price_rules_items`;
CREATE TABLE `phppos_price_rules_items`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `rule_id` int NOT NULL,
  `item_id` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_price_rules_items_ibfk_1`(`rule_id`) USING BTREE,
  INDEX `phppos_price_rules_items_ibfk_2`(`item_id`) USING BTREE,
  CONSTRAINT `phppos_price_rules_items_ibfk_1` FOREIGN KEY (`rule_id`) REFERENCES `phppos_price_rules` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_price_rules_items_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `phppos_items` (`item_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_price_rules_items
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_price_rules_locations
-- ----------------------------
DROP TABLE IF EXISTS `phppos_price_rules_locations`;
CREATE TABLE `phppos_price_rules_locations`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `rule_id` int NOT NULL,
  `location_id` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_price_rules_locations_ibfk_1`(`rule_id`) USING BTREE,
  INDEX `phppos_price_rules_locations_ibfk_2`(`location_id`) USING BTREE,
  CONSTRAINT `phppos_price_rules_locations_ibfk_1` FOREIGN KEY (`rule_id`) REFERENCES `phppos_price_rules` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_price_rules_locations_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_price_rules_locations
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_price_rules_manufacturers
-- ----------------------------
DROP TABLE IF EXISTS `phppos_price_rules_manufacturers`;
CREATE TABLE `phppos_price_rules_manufacturers`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `rule_id` int NOT NULL,
  `manufacturer_id` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_price_rules_manufacturers_ibfk_1`(`rule_id`) USING BTREE,
  INDEX `phppos_price_rules_manufacturers_ibfk_2`(`manufacturer_id`) USING BTREE,
  CONSTRAINT `phppos_price_rules_manufacturers_ibfk_1` FOREIGN KEY (`rule_id`) REFERENCES `phppos_price_rules` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_price_rules_manufacturers_ibfk_2` FOREIGN KEY (`manufacturer_id`) REFERENCES `phppos_manufacturers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_price_rules_manufacturers
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_price_rules_price_breaks
-- ----------------------------
DROP TABLE IF EXISTS `phppos_price_rules_price_breaks`;
CREATE TABLE `phppos_price_rules_price_breaks`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `rule_id` int NOT NULL,
  `item_qty_to_buy` decimal(23, 10) NULL DEFAULT NULL,
  `discount_per_unit_fixed` decimal(23, 10) NULL DEFAULT NULL,
  `discount_per_unit_percent` decimal(23, 10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_price_rules_custom_ibfk_1`(`rule_id`) USING BTREE,
  CONSTRAINT `phppos_price_rules_price_breaks_ibfk_1` FOREIGN KEY (`rule_id`) REFERENCES `phppos_price_rules` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_price_rules_price_breaks
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_price_rules_tags
-- ----------------------------
DROP TABLE IF EXISTS `phppos_price_rules_tags`;
CREATE TABLE `phppos_price_rules_tags`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `rule_id` int NOT NULL,
  `tag_id` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_price_rules_tags_ibfk_1`(`rule_id`) USING BTREE,
  INDEX `phppos_price_rules_tags_ibfk_2`(`tag_id`) USING BTREE,
  CONSTRAINT `phppos_price_rules_tags_ibfk_1` FOREIGN KEY (`rule_id`) REFERENCES `phppos_price_rules` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_price_rules_tags_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `phppos_tags` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_price_rules_tags
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_price_rules_tiers_exclude
-- ----------------------------
DROP TABLE IF EXISTS `phppos_price_rules_tiers_exclude`;
CREATE TABLE `phppos_price_rules_tiers_exclude`  (
  `price_rule_id` int NOT NULL,
  `tier_id` int NOT NULL,
  PRIMARY KEY (`price_rule_id`, `tier_id`) USING BTREE,
  INDEX `phppos_price_rules_tiers_ibfk_2`(`tier_id`) USING BTREE,
  CONSTRAINT `phppos_price_rules_tiers_ibfk_1` FOREIGN KEY (`price_rule_id`) REFERENCES `phppos_price_rules` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_price_rules_tiers_ibfk_2` FOREIGN KEY (`tier_id`) REFERENCES `phppos_price_tiers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_price_rules_tiers_exclude
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_price_tiers
-- ----------------------------
DROP TABLE IF EXISTS `phppos_price_tiers`;
CREATE TABLE `phppos_price_tiers`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `order` int NOT NULL DEFAULT 0,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `default_percent_off` decimal(15, 3) NULL DEFAULT NULL,
  `default_cost_plus_percent` decimal(15, 3) NULL DEFAULT NULL,
  `default_cost_plus_fixed_amount` decimal(23, 10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_price_tiers
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_receivings
-- ----------------------------
DROP TABLE IF EXISTS `phppos_receivings`;
CREATE TABLE `phppos_receivings`  (
  `receiving_time` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  `supplier_id` int NULL DEFAULT NULL,
  `employee_id` int NOT NULL DEFAULT 0,
  `comment` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `receiving_id` int NOT NULL AUTO_INCREMENT,
  `payment_type` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `deleted` int NOT NULL DEFAULT 0,
  `deleted_by` int NULL DEFAULT NULL,
  `suspended` int NOT NULL DEFAULT 0,
  `location_id` int NOT NULL,
  `transfer_to_location_id` int NULL DEFAULT NULL,
  `deleted_taxes` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `is_po` int NOT NULL DEFAULT 0,
  `store_account_payment` int NOT NULL DEFAULT 0,
  `total_quantity_purchased` decimal(23, 10) NOT NULL DEFAULT 0,
  `total_quantity_received` decimal(23, 10) NOT NULL DEFAULT 0,
  `subtotal` decimal(23, 10) NOT NULL DEFAULT 0,
  `tax` decimal(23, 10) NOT NULL DEFAULT 0,
  `total` decimal(23, 10) NOT NULL DEFAULT 0,
  `profit` decimal(23, 10) NOT NULL DEFAULT 0,
  `custom_field_1_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_2_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_3_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_4_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_5_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_6_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_7_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_8_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_9_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_10_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `last_modified` timestamp(0) NULL DEFAULT NULL,
  `override_taxes` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `shipping_cost` decimal(23, 10) NULL DEFAULT NULL,
  `signature_image_id` int NULL DEFAULT NULL,
  PRIMARY KEY (`receiving_id`) USING BTREE,
  INDEX `supplier_id`(`supplier_id`) USING BTREE,
  INDEX `employee_id`(`employee_id`) USING BTREE,
  INDEX `deleted`(`deleted`) USING BTREE,
  INDEX `location_id`(`location_id`) USING BTREE,
  INDEX `transfer_to_location_id`(`transfer_to_location_id`) USING BTREE,
  INDEX `recv_search`(`location_id`, `deleted`, `receiving_time`, `suspended`, `store_account_payment`, `total_quantity_purchased`) USING BTREE,
  INDEX `custom_field_1_value`(`custom_field_1_value`) USING BTREE,
  INDEX `custom_field_2_value`(`custom_field_2_value`) USING BTREE,
  INDEX `custom_field_3_value`(`custom_field_3_value`) USING BTREE,
  INDEX `custom_field_4_value`(`custom_field_4_value`) USING BTREE,
  INDEX `custom_field_5_value`(`custom_field_5_value`) USING BTREE,
  INDEX `custom_field_6_value`(`custom_field_6_value`) USING BTREE,
  INDEX `custom_field_7_value`(`custom_field_7_value`) USING BTREE,
  INDEX `custom_field_8_value`(`custom_field_8_value`) USING BTREE,
  INDEX `custom_field_9_value`(`custom_field_9_value`) USING BTREE,
  INDEX `custom_field_10_value`(`custom_field_10_value`) USING BTREE,
  INDEX `signature_image_id`(`signature_image_id`) USING BTREE,
  CONSTRAINT `phppos_receivings_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `phppos_employees` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_receivings_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `phppos_suppliers` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_receivings_ibfk_3` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_receivings_ibfk_4` FOREIGN KEY (`transfer_to_location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_receivings_ibfk_5` FOREIGN KEY (`signature_image_id`) REFERENCES `phppos_app_files` (`file_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_receivings
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_receivings_items
-- ----------------------------
DROP TABLE IF EXISTS `phppos_receivings_items`;
CREATE TABLE `phppos_receivings_items`  (
  `receiving_id` int NOT NULL DEFAULT 0,
  `item_id` int NOT NULL DEFAULT 0,
  `item_variation_id` int NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `serialnumber` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `line` int NOT NULL DEFAULT 0,
  `quantity_purchased` decimal(23, 10) NOT NULL DEFAULT 0,
  `quantity_received` decimal(23, 10) NOT NULL DEFAULT 0,
  `item_cost_price` decimal(23, 10) NOT NULL,
  `item_unit_price` decimal(23, 10) NOT NULL,
  `discount_percent` decimal(15, 3) NOT NULL DEFAULT 0,
  `expire_date` date NULL DEFAULT NULL,
  `subtotal` decimal(23, 10) NOT NULL DEFAULT 0,
  `tax` decimal(23, 10) NOT NULL DEFAULT 0,
  `total` decimal(23, 10) NOT NULL DEFAULT 0,
  `profit` decimal(23, 10) NOT NULL DEFAULT 0,
  `override_taxes` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `unit_quantity` decimal(23, 10) NULL DEFAULT NULL,
  `items_quantity_units_id` int NULL DEFAULT NULL,
  PRIMARY KEY (`receiving_id`, `item_id`, `line`) USING BTREE,
  INDEX `item_id`(`item_id`) USING BTREE,
  INDEX `phppos_receivings_items_ibfk_3`(`item_variation_id`) USING BTREE,
  INDEX `phppos_receivings_items_ibfk_4`(`items_quantity_units_id`) USING BTREE,
  CONSTRAINT `phppos_receivings_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `phppos_items` (`item_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_receivings_items_ibfk_2` FOREIGN KEY (`receiving_id`) REFERENCES `phppos_receivings` (`receiving_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_receivings_items_ibfk_3` FOREIGN KEY (`item_variation_id`) REFERENCES `phppos_item_variations` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_receivings_items_ibfk_4` FOREIGN KEY (`items_quantity_units_id`) REFERENCES `phppos_items_quantity_units` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_receivings_items
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_receivings_items_taxes
-- ----------------------------
DROP TABLE IF EXISTS `phppos_receivings_items_taxes`;
CREATE TABLE `phppos_receivings_items_taxes`  (
  `receiving_id` int NOT NULL,
  `item_id` int NOT NULL,
  `line` int NOT NULL DEFAULT 0,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15, 3) NOT NULL,
  `cumulative` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`receiving_id`, `item_id`, `line`, `name`, `percent`) USING BTREE,
  INDEX `item_id`(`item_id`) USING BTREE,
  CONSTRAINT `phppos_receivings_items_taxes_ibfk_1` FOREIGN KEY (`receiving_id`) REFERENCES `phppos_receivings` (`receiving_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_receivings_items_taxes_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `phppos_items` (`item_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_receivings_items_taxes
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_receivings_payments
-- ----------------------------
DROP TABLE IF EXISTS `phppos_receivings_payments`;
CREATE TABLE `phppos_receivings_payments`  (
  `payment_id` int NOT NULL AUTO_INCREMENT,
  `receiving_id` int NOT NULL,
  `payment_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `payment_amount` decimal(23, 10) NOT NULL,
  `payment_date` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  PRIMARY KEY (`payment_id`) USING BTREE,
  INDEX `receiving_id`(`receiving_id`) USING BTREE,
  INDEX `payment_date`(`payment_date`) USING BTREE,
  CONSTRAINT `phppos_receivings_payments_ibfk_1` FOREIGN KEY (`receiving_id`) REFERENCES `phppos_receivings` (`receiving_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_receivings_payments
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_register_currency_denominations
-- ----------------------------
DROP TABLE IF EXISTS `phppos_register_currency_denominations`;
CREATE TABLE `phppos_register_currency_denominations`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `value` decimal(23, 10) NOT NULL,
  `deleted` int NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_register_currency_denominations
-- ----------------------------
INSERT INTO `phppos_register_currency_denominations` VALUES (1, '100\'s', 100.0000000000, 0);
INSERT INTO `phppos_register_currency_denominations` VALUES (2, '50\'s', 50.0000000000, 0);
INSERT INTO `phppos_register_currency_denominations` VALUES (3, '20\'s', 20.0000000000, 0);
INSERT INTO `phppos_register_currency_denominations` VALUES (4, '10\'s', 10.0000000000, 0);
INSERT INTO `phppos_register_currency_denominations` VALUES (5, '5\'s', 5.0000000000, 0);
INSERT INTO `phppos_register_currency_denominations` VALUES (6, '2\'s', 2.0000000000, 0);
INSERT INTO `phppos_register_currency_denominations` VALUES (7, '1\'s', 1.0000000000, 0);
INSERT INTO `phppos_register_currency_denominations` VALUES (8, 'Half Dollars', 0.5000000000, 0);
INSERT INTO `phppos_register_currency_denominations` VALUES (9, 'Quarters', 0.2500000000, 0);
INSERT INTO `phppos_register_currency_denominations` VALUES (10, 'Dimes', 0.1000000000, 0);
INSERT INTO `phppos_register_currency_denominations` VALUES (11, 'Nickels', 0.0500000000, 0);
INSERT INTO `phppos_register_currency_denominations` VALUES (12, 'Pennies', 0.0100000000, 0);

-- ----------------------------
-- Table structure for phppos_register_log
-- ----------------------------
DROP TABLE IF EXISTS `phppos_register_log`;
CREATE TABLE `phppos_register_log`  (
  `register_log_id` int NOT NULL AUTO_INCREMENT,
  `employee_id_open` int NOT NULL,
  `employee_id_close` int NULL DEFAULT NULL,
  `register_id` int NULL DEFAULT NULL,
  `shift_start` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00',
  `shift_end` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00',
  `notes` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`register_log_id`) USING BTREE,
  INDEX `phppos_register_log_ibfk_1`(`employee_id_open`) USING BTREE,
  INDEX `phppos_register_log_ibfk_2`(`register_id`) USING BTREE,
  INDEX `phppos_register_log_ibfk_3`(`employee_id_close`) USING BTREE,
  CONSTRAINT `phppos_register_log_ibfk_1` FOREIGN KEY (`employee_id_open`) REFERENCES `phppos_employees` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_register_log_ibfk_2` FOREIGN KEY (`register_id`) REFERENCES `phppos_registers` (`register_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_register_log_ibfk_3` FOREIGN KEY (`employee_id_close`) REFERENCES `phppos_employees` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_register_log
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_register_log_audit
-- ----------------------------
DROP TABLE IF EXISTS `phppos_register_log_audit`;
CREATE TABLE `phppos_register_log_audit`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `register_log_id` int NOT NULL,
  `employee_id` int NOT NULL,
  `date` timestamp(0) NOT NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `amount` decimal(23, 10) NOT NULL DEFAULT 0,
  `note` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `payment_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `register_log_audit_ibfk_1`(`register_log_id`) USING BTREE,
  INDEX `register_log_audit_ibfk_2`(`employee_id`) USING BTREE,
  CONSTRAINT `register_log_audit_ibfk_1` FOREIGN KEY (`register_log_id`) REFERENCES `phppos_register_log` (`register_log_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `register_log_audit_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `phppos_employees` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_register_log_audit
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_register_log_denoms
-- ----------------------------
DROP TABLE IF EXISTS `phppos_register_log_denoms`;
CREATE TABLE `phppos_register_log_denoms`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `register_log_id` int NOT NULL,
  `register_currency_denominations_id` int NOT NULL,
  `count` int NOT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_register_log_denoms_ibfk_1`(`register_log_id`) USING BTREE,
  INDEX `phppos_register_log_denoms_ibfk_2`(`register_currency_denominations_id`) USING BTREE,
  CONSTRAINT `phppos_register_log_denoms_ibfk_1` FOREIGN KEY (`register_log_id`) REFERENCES `phppos_register_log` (`register_log_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_register_log_denoms_ibfk_2` FOREIGN KEY (`register_currency_denominations_id`) REFERENCES `phppos_register_currency_denominations` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_register_log_denoms
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_register_log_payments
-- ----------------------------
DROP TABLE IF EXISTS `phppos_register_log_payments`;
CREATE TABLE `phppos_register_log_payments`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `register_log_id` int NOT NULL,
  `payment_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `open_amount` decimal(23, 10) NOT NULL,
  `close_amount` decimal(23, 10) NOT NULL,
  `payment_sales_amount` decimal(23, 10) NOT NULL,
  `total_payment_additions` decimal(23, 10) NOT NULL,
  `total_payment_subtractions` decimal(23, 10) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_register_log_payments_ibfk_1`(`register_log_id`) USING BTREE,
  CONSTRAINT `phppos_register_log_payments_ibfk_1` FOREIGN KEY (`register_log_id`) REFERENCES `phppos_register_log` (`register_log_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_register_log_payments
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_registers
-- ----------------------------
DROP TABLE IF EXISTS `phppos_registers`;
CREATE TABLE `phppos_registers`  (
  `register_id` int NOT NULL AUTO_INCREMENT,
  `location_id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `iptran_device_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `emv_terminal_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `deleted` int NOT NULL DEFAULT 0,
  `card_connect_hsn` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `emv_pinpad_ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `emv_pinpad_port` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`register_id`) USING BTREE,
  INDEX `deleted`(`deleted`) USING BTREE,
  INDEX `phppos_registers_ibfk_1`(`location_id`) USING BTREE,
  CONSTRAINT `phppos_registers_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_registers
-- ----------------------------
INSERT INTO `phppos_registers` VALUES (1, 1, 'Default', NULL, NULL, 0, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for phppos_registers_cart
-- ----------------------------
DROP TABLE IF EXISTS `phppos_registers_cart`;
CREATE TABLE `phppos_registers_cart`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `register_id` int NOT NULL,
  `data` longblob NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `register_id`(`register_id`) USING BTREE,
  CONSTRAINT `phppos_registers_cart_ibfk_1` FOREIGN KEY (`register_id`) REFERENCES `phppos_registers` (`register_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_registers_cart
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_sale_types
-- ----------------------------
DROP TABLE IF EXISTS `phppos_sale_types`;
CREATE TABLE `phppos_sale_types`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `sort` int NOT NULL,
  `system_sale_type` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_sale_types
-- ----------------------------
INSERT INTO `phppos_sale_types` VALUES (0, 'common_sale', 0, 1);
INSERT INTO `phppos_sale_types` VALUES (1, 'common_layaway', 0, 1);
INSERT INTO `phppos_sale_types` VALUES (2, 'common_estimate', 0, 1);

-- ----------------------------
-- Table structure for phppos_sales
-- ----------------------------
DROP TABLE IF EXISTS `phppos_sales`;
CREATE TABLE `phppos_sales`  (
  `sale_time` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  `customer_id` int NULL DEFAULT NULL,
  `employee_id` int NOT NULL DEFAULT 0,
  `sold_by_employee_id` int NULL DEFAULT NULL,
  `comment` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `discount_reason` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `show_comment_on_receipt` int NOT NULL DEFAULT 0,
  `sale_id` int NOT NULL AUTO_INCREMENT,
  `rule_id` int NULL DEFAULT NULL,
  `rule_discount` decimal(23, 10) NULL DEFAULT NULL,
  `payment_type` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `cc_ref_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `auth_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  `deleted_by` int NULL DEFAULT NULL,
  `deleted` int NOT NULL DEFAULT 0,
  `suspended` int NOT NULL DEFAULT 0,
  `is_ecommerce` int NOT NULL DEFAULT 0,
  `ecommerce_order_id` int NULL DEFAULT NULL,
  `ecommerce_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `store_account_payment` int NOT NULL DEFAULT 0,
  `was_layaway` int NOT NULL DEFAULT 0,
  `was_estimate` int NOT NULL DEFAULT 0,
  `location_id` int NOT NULL,
  `register_id` int NULL DEFAULT NULL,
  `tier_id` int NULL DEFAULT NULL,
  `points_used` decimal(23, 10) NOT NULL DEFAULT 0,
  `points_gained` decimal(23, 10) NOT NULL DEFAULT 0,
  `did_redeem_discount` int NOT NULL DEFAULT 0,
  `signature_image_id` int NULL DEFAULT NULL,
  `deleted_taxes` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `total_quantity_purchased` decimal(23, 10) NOT NULL DEFAULT 0,
  `subtotal` decimal(23, 10) NOT NULL DEFAULT 0,
  `tax` decimal(23, 10) NOT NULL DEFAULT 0,
  `total` decimal(23, 10) NOT NULL DEFAULT 0,
  `profit` decimal(23, 10) NOT NULL DEFAULT 0,
  `exchange_rate` decimal(23, 10) NOT NULL DEFAULT 1,
  `exchange_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `exchange_currency_symbol` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `exchange_currency_symbol_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `exchange_number_of_decimals` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `exchange_thousands_separator` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `exchange_decimal_point` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `is_purchase_points` int NOT NULL DEFAULT 0,
  `custom_field_1_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_2_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_3_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_4_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_5_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_6_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_7_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_8_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_9_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_10_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `last_modified` timestamp(0) NULL DEFAULT NULL,
  `override_taxes` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `return_sale_id` int NULL DEFAULT NULL,
  `tip` decimal(23, 10) NULL DEFAULT NULL,
  `total_quantity_received` decimal(23, 10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`sale_id`) USING BTREE,
  INDEX `customer_id`(`customer_id`) USING BTREE,
  INDEX `employee_id`(`employee_id`) USING BTREE,
  INDEX `deleted`(`deleted`) USING BTREE,
  INDEX `location_id`(`location_id`) USING BTREE,
  INDEX `phppos_sales_ibfk_4`(`deleted_by`) USING BTREE,
  INDEX `phppos_sales_ibfk_5`(`tier_id`) USING BTREE,
  INDEX `phppos_sales_ibfk_7`(`register_id`) USING BTREE,
  INDEX `phppos_sales_ibfk_6`(`sold_by_employee_id`) USING BTREE,
  INDEX `phppos_sales_ibfk_8`(`signature_image_id`) USING BTREE,
  INDEX `was_layaway`(`was_layaway`) USING BTREE,
  INDEX `was_estimate`(`was_estimate`) USING BTREE,
  INDEX `phppos_sales_ibfk_9`(`rule_id`) USING BTREE,
  INDEX `sales_search`(`location_id`, `deleted`, `sale_time`, `suspended`, `store_account_payment`, `total_quantity_purchased`) USING BTREE,
  INDEX `phppos_sales_ibfk_10`(`suspended`) USING BTREE,
  INDEX `custom_field_1_value`(`custom_field_1_value`) USING BTREE,
  INDEX `custom_field_2_value`(`custom_field_2_value`) USING BTREE,
  INDEX `custom_field_3_value`(`custom_field_3_value`) USING BTREE,
  INDEX `custom_field_4_value`(`custom_field_4_value`) USING BTREE,
  INDEX `custom_field_5_value`(`custom_field_5_value`) USING BTREE,
  INDEX `custom_field_6_value`(`custom_field_6_value`) USING BTREE,
  INDEX `custom_field_7_value`(`custom_field_7_value`) USING BTREE,
  INDEX `custom_field_8_value`(`custom_field_8_value`) USING BTREE,
  INDEX `custom_field_9_value`(`custom_field_9_value`) USING BTREE,
  INDEX `custom_field_10_value`(`custom_field_10_value`) USING BTREE,
  INDEX `phppos_sales_ibfk_11`(`return_sale_id`) USING BTREE,
  INDEX `ecommerce_order_id`(`ecommerce_order_id`) USING BTREE,
  CONSTRAINT `phppos_sales_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `phppos_employees` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_sales_ibfk_10` FOREIGN KEY (`suspended`) REFERENCES `phppos_sale_types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_sales_ibfk_11` FOREIGN KEY (`return_sale_id`) REFERENCES `phppos_sales` (`sale_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_sales_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `phppos_customers` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_sales_ibfk_3` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_sales_ibfk_4` FOREIGN KEY (`deleted_by`) REFERENCES `phppos_employees` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_sales_ibfk_5` FOREIGN KEY (`tier_id`) REFERENCES `phppos_price_tiers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_sales_ibfk_6` FOREIGN KEY (`sold_by_employee_id`) REFERENCES `phppos_employees` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_sales_ibfk_7` FOREIGN KEY (`register_id`) REFERENCES `phppos_registers` (`register_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_sales_ibfk_8` FOREIGN KEY (`signature_image_id`) REFERENCES `phppos_app_files` (`file_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_sales_ibfk_9` FOREIGN KEY (`rule_id`) REFERENCES `phppos_price_rules` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_sales
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_sales_coupons
-- ----------------------------
DROP TABLE IF EXISTS `phppos_sales_coupons`;
CREATE TABLE `phppos_sales_coupons`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `sale_id` int NOT NULL,
  `rule_id` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_sales_coupons_ibfk_1`(`sale_id`) USING BTREE,
  INDEX `phppos_sales_coupons_ibfk_2`(`rule_id`) USING BTREE,
  CONSTRAINT `phppos_sales_coupons_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `phppos_sales` (`sale_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_sales_coupons_ibfk_2` FOREIGN KEY (`rule_id`) REFERENCES `phppos_price_rules` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_sales_coupons
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_sales_deliveries
-- ----------------------------
DROP TABLE IF EXISTS `phppos_sales_deliveries`;
CREATE TABLE `phppos_sales_deliveries`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `sale_id` int NOT NULL,
  `shipping_address_person_id` int NOT NULL,
  `delivery_employee_person_id` int NULL DEFAULT NULL,
  `shipping_method_id` int NULL DEFAULT NULL,
  `shipping_zone_id` int NULL DEFAULT NULL,
  `tax_class_id` int NULL DEFAULT NULL,
  `status` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `estimated_shipping_date` timestamp(0) NULL DEFAULT NULL,
  `actual_shipping_date` timestamp(0) NULL DEFAULT NULL,
  `estimated_delivery_or_pickup_date` timestamp(0) NULL DEFAULT NULL,
  `actual_delivery_or_pickup_date` timestamp(0) NULL DEFAULT NULL,
  `is_pickup` int NOT NULL DEFAULT 0,
  `tracking_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `comment` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `search_index`(`status`, `shipping_address_person_id`) USING BTREE,
  INDEX `phppos_sales_deliveries_ibfk_1`(`sale_id`) USING BTREE,
  INDEX `phppos_sales_deliveries_ibfk_2`(`shipping_address_person_id`) USING BTREE,
  INDEX `phppos_sales_deliveries_ibfk_3`(`shipping_method_id`) USING BTREE,
  INDEX `phppos_sales_deliveries_ibfk_4`(`shipping_zone_id`) USING BTREE,
  INDEX `phppos_sales_deliveries_ibfk_5`(`tax_class_id`) USING BTREE,
  INDEX `deleted`(`deleted`) USING BTREE,
  INDEX `phppos_sales_deliveries_ibfk_6`(`delivery_employee_person_id`) USING BTREE,
  CONSTRAINT `phppos_sales_deliveries_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `phppos_sales` (`sale_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_sales_deliveries_ibfk_2` FOREIGN KEY (`shipping_address_person_id`) REFERENCES `phppos_people` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_sales_deliveries_ibfk_3` FOREIGN KEY (`shipping_method_id`) REFERENCES `phppos_shipping_methods` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_sales_deliveries_ibfk_4` FOREIGN KEY (`shipping_zone_id`) REFERENCES `phppos_shipping_zones` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_sales_deliveries_ibfk_5` FOREIGN KEY (`tax_class_id`) REFERENCES `phppos_tax_classes` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_sales_deliveries_ibfk_6` FOREIGN KEY (`delivery_employee_person_id`) REFERENCES `phppos_employees` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_sales_deliveries
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_sales_item_kits
-- ----------------------------
DROP TABLE IF EXISTS `phppos_sales_item_kits`;
CREATE TABLE `phppos_sales_item_kits`  (
  `sale_id` int NOT NULL DEFAULT 0,
  `item_kit_id` int NOT NULL DEFAULT 0,
  `rule_id` int NULL DEFAULT NULL,
  `rule_discount` decimal(23, 10) NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `line` int NOT NULL DEFAULT 0,
  `quantity_purchased` decimal(23, 10) NOT NULL DEFAULT 0,
  `quantity_received` decimal(23, 10) NOT NULL DEFAULT 0,
  `item_kit_cost_price` decimal(23, 10) NOT NULL,
  `item_kit_unit_price` decimal(23, 10) NOT NULL,
  `regular_item_kit_unit_price_at_time_of_sale` decimal(23, 10) NULL DEFAULT NULL,
  `discount_percent` decimal(15, 3) NOT NULL DEFAULT 0,
  `commission` decimal(23, 10) NOT NULL DEFAULT 0,
  `subtotal` decimal(23, 10) NOT NULL DEFAULT 0,
  `tax` decimal(23, 10) NOT NULL DEFAULT 0,
  `total` decimal(23, 10) NOT NULL DEFAULT 0,
  `profit` decimal(23, 10) NOT NULL DEFAULT 0,
  `tier_id` int NULL DEFAULT NULL,
  `override_taxes` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `loyalty_multiplier` decimal(23, 10) NULL DEFAULT NULL,
  PRIMARY KEY (`sale_id`, `item_kit_id`, `line`) USING BTREE,
  INDEX `item_kit_id`(`item_kit_id`) USING BTREE,
  INDEX `phppos_sales_item_kits_ibfk_3`(`rule_id`) USING BTREE,
  CONSTRAINT `phppos_sales_item_kits_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `phppos_item_kits` (`item_kit_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_sales_item_kits_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `phppos_sales` (`sale_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_sales_item_kits_ibfk_3` FOREIGN KEY (`rule_id`) REFERENCES `phppos_price_rules` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_sales_item_kits
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_sales_item_kits_modifier_items
-- ----------------------------
DROP TABLE IF EXISTS `phppos_sales_item_kits_modifier_items`;
CREATE TABLE `phppos_sales_item_kits_modifier_items`  (
  `item_kit_id` int NOT NULL,
  `sale_id` int NOT NULL,
  `line` int NOT NULL,
  `modifier_item_id` int NOT NULL,
  `cost_price` decimal(23, 10) NOT NULL DEFAULT 0,
  `unit_price` decimal(23, 10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`item_kit_id`, `sale_id`, `line`, `modifier_item_id`) USING BTREE,
  INDEX `phppos_sales_item_kits_modifier_items_ibfk_2`(`sale_id`) USING BTREE,
  INDEX `phppos_sales_item_kits_modifier_items_ibfk_3`(`modifier_item_id`) USING BTREE,
  CONSTRAINT `phppos_sales_item_kits_modifier_items_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `phppos_item_kits` (`item_kit_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_sales_item_kits_modifier_items_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `phppos_sales` (`sale_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_sales_item_kits_modifier_items_ibfk_3` FOREIGN KEY (`modifier_item_id`) REFERENCES `phppos_modifier_items` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_sales_item_kits_modifier_items
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_sales_item_kits_taxes
-- ----------------------------
DROP TABLE IF EXISTS `phppos_sales_item_kits_taxes`;
CREATE TABLE `phppos_sales_item_kits_taxes`  (
  `sale_id` int NOT NULL,
  `item_kit_id` int NOT NULL,
  `line` int NOT NULL DEFAULT 0,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15, 3) NOT NULL,
  `cumulative` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`sale_id`, `item_kit_id`, `line`, `name`, `percent`) USING BTREE,
  INDEX `item_id`(`item_kit_id`) USING BTREE,
  CONSTRAINT `phppos_sales_item_kits_taxes_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `phppos_sales_item_kits` (`sale_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_sales_item_kits_taxes_ibfk_2` FOREIGN KEY (`item_kit_id`) REFERENCES `phppos_item_kits` (`item_kit_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_sales_item_kits_taxes
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_sales_items
-- ----------------------------
DROP TABLE IF EXISTS `phppos_sales_items`;
CREATE TABLE `phppos_sales_items`  (
  `sale_id` int NOT NULL DEFAULT 0,
  `item_id` int NOT NULL DEFAULT 0,
  `item_variation_id` int NULL DEFAULT NULL,
  `rule_id` int NULL DEFAULT NULL,
  `rule_discount` decimal(23, 10) NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `serialnumber` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `line` int NOT NULL DEFAULT 0,
  `quantity_purchased` decimal(23, 10) NOT NULL DEFAULT 0,
  `quantity_received` decimal(23, 10) NOT NULL DEFAULT 0,
  `item_cost_price` decimal(23, 10) NOT NULL,
  `item_unit_price` decimal(23, 10) NOT NULL,
  `regular_item_unit_price_at_time_of_sale` decimal(23, 10) NULL DEFAULT NULL,
  `discount_percent` decimal(15, 3) NOT NULL DEFAULT 0,
  `commission` decimal(23, 10) NOT NULL DEFAULT 0,
  `subtotal` decimal(23, 10) NOT NULL DEFAULT 0,
  `tax` decimal(23, 10) NOT NULL DEFAULT 0,
  `total` decimal(23, 10) NOT NULL DEFAULT 0,
  `profit` decimal(23, 10) NOT NULL DEFAULT 0,
  `tier_id` int NULL DEFAULT NULL,
  `series_id` int NULL DEFAULT NULL,
  `damaged_qty` decimal(23, 10) NOT NULL DEFAULT 0,
  `override_taxes` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `unit_quantity` decimal(23, 10) NULL DEFAULT NULL,
  `items_quantity_units_id` int NULL DEFAULT NULL,
  `loyalty_multiplier` decimal(23, 10) NULL DEFAULT NULL,
  PRIMARY KEY (`sale_id`, `item_id`, `line`) USING BTREE,
  INDEX `item_id`(`item_id`) USING BTREE,
  INDEX `phppos_sales_items_ibfk_3`(`rule_id`) USING BTREE,
  INDEX `phppos_sales_items_ibfk_4`(`item_variation_id`) USING BTREE,
  INDEX `phppos_sales_items_ibfk_5`(`series_id`) USING BTREE,
  INDEX `phppos_sales_items_ibfk_6`(`items_quantity_units_id`) USING BTREE,
  CONSTRAINT `phppos_sales_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `phppos_items` (`item_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_sales_items_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `phppos_sales` (`sale_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_sales_items_ibfk_3` FOREIGN KEY (`rule_id`) REFERENCES `phppos_price_rules` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_sales_items_ibfk_4` FOREIGN KEY (`item_variation_id`) REFERENCES `phppos_item_variations` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_sales_items_ibfk_5` FOREIGN KEY (`series_id`) REFERENCES `phppos_customers_series` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_sales_items_ibfk_6` FOREIGN KEY (`items_quantity_units_id`) REFERENCES `phppos_items_quantity_units` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_sales_items
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_sales_items_modifier_items
-- ----------------------------
DROP TABLE IF EXISTS `phppos_sales_items_modifier_items`;
CREATE TABLE `phppos_sales_items_modifier_items`  (
  `item_id` int NOT NULL,
  `sale_id` int NOT NULL,
  `line` int NOT NULL,
  `modifier_item_id` int NOT NULL,
  `cost_price` decimal(23, 10) NOT NULL DEFAULT 0,
  `unit_price` decimal(23, 10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`item_id`, `sale_id`, `line`, `modifier_item_id`) USING BTREE,
  INDEX `phppos_sales_items_modifier_items_ibfk_2`(`sale_id`) USING BTREE,
  INDEX `phppos_sales_items_modifier_items_ibfk_3`(`modifier_item_id`) USING BTREE,
  CONSTRAINT `phppos_sales_items_modifier_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `phppos_items` (`item_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_sales_items_modifier_items_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `phppos_sales` (`sale_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_sales_items_modifier_items_ibfk_3` FOREIGN KEY (`modifier_item_id`) REFERENCES `phppos_modifier_items` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_sales_items_modifier_items
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_sales_items_taxes
-- ----------------------------
DROP TABLE IF EXISTS `phppos_sales_items_taxes`;
CREATE TABLE `phppos_sales_items_taxes`  (
  `sale_id` int NOT NULL,
  `item_id` int NOT NULL,
  `line` int NOT NULL DEFAULT 0,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15, 3) NOT NULL,
  `cumulative` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`sale_id`, `item_id`, `line`, `name`, `percent`) USING BTREE,
  INDEX `item_id`(`item_id`) USING BTREE,
  CONSTRAINT `phppos_sales_items_taxes_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `phppos_sales_items` (`sale_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_sales_items_taxes_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `phppos_items` (`item_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_sales_items_taxes
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_sales_payments
-- ----------------------------
DROP TABLE IF EXISTS `phppos_sales_payments`;
CREATE TABLE `phppos_sales_payments`  (
  `payment_id` int NOT NULL AUTO_INCREMENT,
  `sale_id` int NOT NULL,
  `payment_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `payment_amount` decimal(23, 10) NOT NULL,
  `auth_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  `ref_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  `cc_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  `acq_ref_data` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  `process_data` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  `entry_method` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  `aid` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  `tvr` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  `iad` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  `tsi` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  `arc` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  `cvm` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  `tran_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  `application_label` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  `truncated_card` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  `card_issuer` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  `payment_date` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  `ebt_auth_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  `ebt_voucher_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '',
  PRIMARY KEY (`payment_id`) USING BTREE,
  INDEX `sale_id`(`sale_id`) USING BTREE,
  INDEX `payment_date`(`payment_date`) USING BTREE,
  CONSTRAINT `phppos_sales_payments_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `phppos_sales` (`sale_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_sales_payments
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_sessions
-- ----------------------------
DROP TABLE IF EXISTS `phppos_sessions`;
CREATE TABLE `phppos_sessions`  (
  `id` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int UNSIGNED NOT NULL DEFAULT 0,
  `data` longblob NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_sessions_timestamp`(`timestamp`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_sessions
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_shipping_methods
-- ----------------------------
DROP TABLE IF EXISTS `phppos_shipping_methods`;
CREATE TABLE `phppos_shipping_methods`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `shipping_provider_id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `fee` decimal(23, 10) NOT NULL DEFAULT 0,
  `fee_tax_class_id` int NULL DEFAULT NULL,
  `time_in_days` int NULL DEFAULT NULL,
  `has_tracking_number` int NOT NULL DEFAULT 0,
  `is_default` int NOT NULL DEFAULT 0,
  `deleted` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_shipping_methods_ibfk_1`(`shipping_provider_id`) USING BTREE,
  INDEX `phppos_shipping_methods_ibfk_2`(`fee_tax_class_id`) USING BTREE,
  CONSTRAINT `phppos_shipping_methods_ibfk_1` FOREIGN KEY (`shipping_provider_id`) REFERENCES `phppos_shipping_providers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_shipping_methods_ibfk_2` FOREIGN KEY (`fee_tax_class_id`) REFERENCES `phppos_tax_classes` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_shipping_methods
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_shipping_providers
-- ----------------------------
DROP TABLE IF EXISTS `phppos_shipping_providers`;
CREATE TABLE `phppos_shipping_providers`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `order` int NOT NULL DEFAULT 0,
  `deleted` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_shipping_providers
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_shipping_zones
-- ----------------------------
DROP TABLE IF EXISTS `phppos_shipping_zones`;
CREATE TABLE `phppos_shipping_zones`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `fee` decimal(23, 10) NOT NULL DEFAULT 0,
  `tax_class_id` int NULL DEFAULT NULL,
  `order` int NOT NULL DEFAULT 0,
  `deleted` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_shipping_zones_ibfk_1`(`tax_class_id`) USING BTREE,
  CONSTRAINT `phppos_shipping_zones_ibfk_1` FOREIGN KEY (`tax_class_id`) REFERENCES `phppos_tax_classes` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_shipping_zones
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_store_accounts
-- ----------------------------
DROP TABLE IF EXISTS `phppos_store_accounts`;
CREATE TABLE `phppos_store_accounts`  (
  `sno` int NOT NULL AUTO_INCREMENT,
  `customer_id` int NOT NULL,
  `sale_id` int NULL DEFAULT NULL,
  `transaction_amount` decimal(23, 10) NOT NULL DEFAULT 0,
  `date` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  `balance` decimal(23, 10) NOT NULL DEFAULT 0,
  `comment` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`sno`) USING BTREE,
  INDEX `phppos_store_accounts_ibfk_1`(`sale_id`) USING BTREE,
  INDEX `phppos_store_accounts_ibfk_2`(`customer_id`) USING BTREE,
  CONSTRAINT `phppos_store_accounts_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `phppos_sales` (`sale_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_store_accounts_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `phppos_customers` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_store_accounts
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_store_accounts_paid_sales
-- ----------------------------
DROP TABLE IF EXISTS `phppos_store_accounts_paid_sales`;
CREATE TABLE `phppos_store_accounts_paid_sales`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `store_account_payment_sale_id` int NULL DEFAULT NULL,
  `sale_id` int NULL DEFAULT NULL,
  `partial_payment_amount` decimal(23, 10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_store_accounts_sales_ibfk_1`(`sale_id`) USING BTREE,
  INDEX `phppos_store_accounts_sales_ibfk_2`(`store_account_payment_sale_id`) USING BTREE,
  CONSTRAINT `phppos_store_accounts_sales_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `phppos_sales` (`sale_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_store_accounts_sales_ibfk_2` FOREIGN KEY (`store_account_payment_sale_id`) REFERENCES `phppos_sales` (`sale_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_store_accounts_paid_sales
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_supplier_store_accounts
-- ----------------------------
DROP TABLE IF EXISTS `phppos_supplier_store_accounts`;
CREATE TABLE `phppos_supplier_store_accounts`  (
  `sno` int NOT NULL AUTO_INCREMENT,
  `supplier_id` int NOT NULL,
  `receiving_id` int NULL DEFAULT NULL,
  `transaction_amount` decimal(23, 10) NOT NULL DEFAULT 0,
  `date` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  `balance` decimal(23, 10) NOT NULL DEFAULT 0,
  `comment` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`sno`) USING BTREE,
  INDEX `phppos_supplier_store_accounts_ibfk_1`(`receiving_id`) USING BTREE,
  INDEX `phppos_supplier_store_accounts_ibfk_2`(`supplier_id`) USING BTREE,
  CONSTRAINT `phppos_supplier_store_accounts_ibfk_1` FOREIGN KEY (`receiving_id`) REFERENCES `phppos_receivings` (`receiving_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_supplier_store_accounts_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `phppos_suppliers` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_supplier_store_accounts
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_supplier_store_accounts_paid_receivings
-- ----------------------------
DROP TABLE IF EXISTS `phppos_supplier_store_accounts_paid_receivings`;
CREATE TABLE `phppos_supplier_store_accounts_paid_receivings`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `store_account_payment_receiving_id` int NULL DEFAULT NULL,
  `receiving_id` int NULL DEFAULT NULL,
  `partial_payment_amount` decimal(23, 10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_supplier_store_accounts_paid_receivings_ibfk_1`(`receiving_id`) USING BTREE,
  INDEX `phppos_supplier_store_accounts_paid_receivings_ibfk_2`(`store_account_payment_receiving_id`) USING BTREE,
  CONSTRAINT `phppos_supplier_store_accounts_paid_receivings_ibfk_1` FOREIGN KEY (`receiving_id`) REFERENCES `phppos_receivings` (`receiving_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_supplier_store_accounts_paid_receivings_ibfk_2` FOREIGN KEY (`store_account_payment_receiving_id`) REFERENCES `phppos_receivings` (`receiving_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_supplier_store_accounts_paid_receivings
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_suppliers
-- ----------------------------
DROP TABLE IF EXISTS `phppos_suppliers`;
CREATE TABLE `phppos_suppliers`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `person_id` int NOT NULL,
  `company_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `account_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `override_default_tax` int NOT NULL DEFAULT 0,
  `balance` decimal(23, 10) NOT NULL DEFAULT 0,
  `deleted` int NOT NULL DEFAULT 0,
  `tax_class_id` int NULL DEFAULT NULL,
  `custom_field_1_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_2_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_3_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_4_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_5_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_6_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_7_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_8_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_9_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `custom_field_10_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `account_number`(`account_number`) USING BTREE,
  INDEX `person_id`(`person_id`) USING BTREE,
  INDEX `deleted`(`deleted`) USING BTREE,
  INDEX `phppos_suppliers_ibfk_2`(`tax_class_id`) USING BTREE,
  INDEX `custom_field_1_value`(`custom_field_1_value`) USING BTREE,
  INDEX `custom_field_2_value`(`custom_field_2_value`) USING BTREE,
  INDEX `custom_field_3_value`(`custom_field_3_value`) USING BTREE,
  INDEX `custom_field_4_value`(`custom_field_4_value`) USING BTREE,
  INDEX `custom_field_5_value`(`custom_field_5_value`) USING BTREE,
  INDEX `custom_field_6_value`(`custom_field_6_value`) USING BTREE,
  INDEX `custom_field_7_value`(`custom_field_7_value`) USING BTREE,
  INDEX `custom_field_8_value`(`custom_field_8_value`) USING BTREE,
  INDEX `custom_field_9_value`(`custom_field_9_value`) USING BTREE,
  INDEX `custom_field_10_value`(`custom_field_10_value`) USING BTREE,
  CONSTRAINT `phppos_suppliers_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `phppos_people` (`person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `phppos_suppliers_ibfk_2` FOREIGN KEY (`tax_class_id`) REFERENCES `phppos_tax_classes` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_suppliers
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_suppliers_taxes
-- ----------------------------
DROP TABLE IF EXISTS `phppos_suppliers_taxes`;
CREATE TABLE `phppos_suppliers_taxes`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `supplier_id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15, 3) NOT NULL,
  `cumulative` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unique_tax`(`supplier_id`, `name`, `percent`) USING BTREE,
  CONSTRAINT `phppos_suppliers_taxes_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `phppos_suppliers` (`person_id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_suppliers_taxes
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_tags
-- ----------------------------
DROP TABLE IF EXISTS `phppos_tags`;
CREATE TABLE `phppos_tags`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `ecommerce_tag_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `last_modified` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  `deleted` int NOT NULL DEFAULT 0,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `tag_name`(`name`) USING BTREE,
  INDEX `deleted`(`deleted`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_tags
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_tax_classes
-- ----------------------------
DROP TABLE IF EXISTS `phppos_tax_classes`;
CREATE TABLE `phppos_tax_classes`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `order` int NOT NULL DEFAULT 0,
  `location_id` int NULL DEFAULT NULL,
  `deleted` int NOT NULL DEFAULT 0,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ecommerce_tax_class_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `phppos_tax_classes_ibfk_1`(`location_id`) USING BTREE,
  CONSTRAINT `phppos_tax_classes_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `phppos_locations` (`location_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_tax_classes
-- ----------------------------
INSERT INTO `phppos_tax_classes` VALUES (1, 1, NULL, 0, 'Taxes', NULL);

-- ----------------------------
-- Table structure for phppos_tax_classes_taxes
-- ----------------------------
DROP TABLE IF EXISTS `phppos_tax_classes_taxes`;
CREATE TABLE `phppos_tax_classes_taxes`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `order` int NOT NULL DEFAULT 0,
  `tax_class_id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15, 3) NOT NULL,
  `cumulative` int NOT NULL DEFAULT 0,
  `ecommerce_tax_class_tax_rate_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unique_tax`(`tax_class_id`, `name`, `percent`) USING BTREE,
  CONSTRAINT `phppos_tax_classes_taxes_ibfk_1` FOREIGN KEY (`tax_class_id`) REFERENCES `phppos_tax_classes` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_tax_classes_taxes
-- ----------------------------

-- ----------------------------
-- Table structure for phppos_zips
-- ----------------------------
DROP TABLE IF EXISTS `phppos_zips`;
CREATE TABLE `phppos_zips`  (
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `shipping_zone_id` int NULL DEFAULT NULL,
  `order` int NOT NULL DEFAULT 0,
  `deleted` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`name`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE,
  INDEX `phppos_zips_ibfk_1`(`shipping_zone_id`) USING BTREE,
  CONSTRAINT `phppos_zips_ibfk_1` FOREIGN KEY (`shipping_zone_id`) REFERENCES `phppos_shipping_zones` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phppos_zips
-- ----------------------------

-- ----------------------------
-- Table structure for post_code
-- ----------------------------
DROP TABLE IF EXISTS `post_code`;
CREATE TABLE `post_code`  (
  `code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `village` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `commune` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `district` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `province` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `country_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'select|country|country|code|description|description_2',
  `inactived` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'No' COMMENT 'checkbox',
  `is_deleted` tinyint NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of post_code
-- ----------------------------

-- ----------------------------
-- Table structure for table
-- ----------------------------
DROP TABLE IF EXISTS `table`;
CREATE TABLE `table`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `table_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `view_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `system_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'System',
  `published` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'No',
  `comment` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_deleted` tinyint NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `territory_search_index`(`id`, `table_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of table
-- ----------------------------
INSERT INTO `table` VALUES (1, 'customer', 'v_customer', 'System', 'No', NULL, 0, NULL, NULL, NULL);
INSERT INTO `table` VALUES (2, 'post_code', 'v_post_code', 'System', 'No', NULL, 0, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for table_field
-- ----------------------------
DROP TABLE IF EXISTS `table_field`;
CREATE TABLE `table_field`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `table_id` int NULL DEFAULT NULL,
  `table_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `field_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `field_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `placeholder` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `index` int NULL DEFAULT NULL,
  `default_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `is_nullable` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT 'Yes',
  `data_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `field_data_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `input_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT 'input',
  `option_text` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `input_mask` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `alignment` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT 'text-left',
  `decimal_format` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `read_only` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT 'No',
  `search_index` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT 'No',
  `published` varchar(4) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT 'No',
  `validate_imported` varchar(4) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT 'No',
  `mandatory` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT 'No',
  `max_length` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `number_precision` int NULL DEFAULT NULL,
  `numberic_scale` int NULL DEFAULT NULL,
  `primary_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `extra` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `url_link` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `system_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT 'System',
  `local_currency` varchar(4) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `flow_field_method` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `flow_field_reverse_sign` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT 'No',
  `flow_field_table_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `flow_field_field_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `text_seperator` varchar(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `comments` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `field_width` int NULL DEFAULT NULL,
  `field_sortable` int NULL DEFAULT NULL,
  `is_updated` varchar(4) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT 'No',
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `table_field_index01`(`table_id`, `table_name`, `field_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of table_field
-- ----------------------------
INSERT INTO `table_field` VALUES (1, 1, 'customer', 'no', 'No', 'No', 1, NULL, 'Yes', 'text', 'text', 'input', NULL, NULL, 'text-left', 'text', 'No', 'No', 'No', 'No', 'No', NULL, NULL, NULL, NULL, NULL, NULL, 'System', NULL, NULL, 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', '2021-03-28 22:00:41', '2021-03-28 22:00:41', '2021-03-28 22:00:41');
INSERT INTO `table_field` VALUES (2, 1, 'customer', 'name', 'Name', 'Name', 1, NULL, 'Yes', 'text', 'text', 'input', NULL, NULL, 'text-left', 'text', 'No', 'No', 'No', 'No', 'No', NULL, NULL, NULL, NULL, NULL, NULL, 'System', NULL, NULL, 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', '2021-03-28 22:00:41', '2021-03-28 22:00:41', '2021-03-28 22:00:41');
INSERT INTO `table_field` VALUES (3, 1, 'customer', 'name_2', 'Name 2', 'Name', 1, NULL, 'Yes', 'text', 'text', 'input', NULL, NULL, 'text-left', 'text', 'No', 'No', 'No', 'No', 'No', NULL, NULL, NULL, NULL, NULL, NULL, 'System', NULL, NULL, 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', '2021-03-28 22:00:41', '2021-03-28 22:00:41', '2021-03-28 22:00:41');
INSERT INTO `table_field` VALUES (4, 1, 'customer', 'post_code', 'Post Code', 'Post Code', 1, NULL, 'Yes', 'text', 'text', 'input', NULL, NULL, 'text-left', 'text', 'No', 'No', 'No', 'No', 'No', NULL, NULL, NULL, NULL, NULL, NULL, 'System', NULL, NULL, 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', '2021-03-28 22:00:41', '2021-03-28 22:00:41', '2021-03-28 22:00:41');

-- ----------------------------
-- Table structure for table_relation
-- ----------------------------
DROP TABLE IF EXISTS `table_relation`;
CREATE TABLE `table_relation`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `field_id` bigint NULL DEFAULT NULL,
  `table_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `relation_table_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `data_table_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `relation_field_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `relation_desc_field` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `relation_desc_field_2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lookup` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `modal_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'Select' COMMENT 'Select,System',
  `cascade_on_delete` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'Yes',
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `table_relation_unique01`(`table_name`, `field_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1928 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of table_relation
-- ----------------------------
INSERT INTO `table_relation` VALUES (1927, 4, 'customer', 'post_code', 'post_code', 'post_code', 'code', 'description', 'description', NULL, NULL, 'Select', 'Yes', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for user_setup
-- ----------------------------
DROP TABLE IF EXISTS `user_setup`;
CREATE TABLE `user_setup`  (
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `default_organization_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `role_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'PRESIDENT' COMMENT 'select|roles|roles|code|description|description_2',
  `permission_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'ADMIN' COMMENT 'select|permission|permission|code|description|description_2',
  `location_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'MAIN' COMMENT 'select|location|location|code|description|description_2',
  `intransit_location_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'INTRAN' COMMENT 'select|intransit_location_code|location|code|description|description_2',
  `business_unit_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'select|business_units|business_units|code|description|description_2',
  `division_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'select|division|division|code|description|description_2',
  `store_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'select|store|store|code|description|description_2',
  `project_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'select|project|project|code|description|description_2',
  `salesperson_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'select|salesperson|salesperson|code|name|name_2',
  `resource_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'select|resource|resource|no|name|name_2',
  `distributor_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'select|distributor|distributor|code|name|name_2',
  `language_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'EN' COMMENT 'select|language|language|code|description|description_2',
  `allow_posting_from` date NULL DEFAULT '1900-01-01',
  `allow_posting_to` date NULL DEFAULT '2500-01-01',
  `default_working_date` date NULL DEFAULT NULL,
  `allow_custom_page` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'No' COMMENT 'checkbox',
  `hide_department` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'No' COMMENT 'checkbox',
  `department_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'select|department|department|code|description|description_2',
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'Full User' COMMENT 'option|Full User,Limited User,Mobile User,External User',
  `customer_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'GENERAL' COMMENT 'select',
  `vendor_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'GENERAL' COMMENT 'select',
  `user_id` bigint NULL DEFAULT NULL,
  `is_deleted` tinyint NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `inactived` tinyint NULL DEFAULT 0,
  PRIMARY KEY (`email`) USING BTREE,
  INDEX `user_setup_search_index`(`email`, `role_code`, `permission_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user_setup
-- ----------------------------
INSERT INTO `user_setup` VALUES ('phearom@gmail.com', 'titb', 'PRESIDENT', 'ADMIN', 'MAIN', 'INTRAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'EN', '1900-01-01', '2500-01-01', NULL, 'No', 'No', NULL, 'Full User', 'GENERAL', 'GENERAL', NULL, NULL, NULL, NULL, NULL, 0);

-- ----------------------------
-- View structure for v_phppos_customers
-- ----------------------------
DROP VIEW IF EXISTS `v_phppos_customers`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_phppos_customers` AS SELECT
phppos_customers.id,
	phppos_customers.credit_limit, 
	phppos_customers.override_default_tax, 
	phppos_customers.company_name, 
	phppos_customers.balance, 
	phppos_customers.points, 
	phppos_customers.disable_loyalty, 
	phppos_customers.current_spend_for_points, 
	phppos_customers.current_sales_for_discount, 
	phppos_customers.taxable, 
	phppos_customers.always_sms_receipt, 
	phppos_customers.auto_email_receipt, 
	phppos_customers.customer_info_popup, 
	phppos_customers.internal_notes, 
	phppos_customers.location_id, 
	phppos_customers.tax_class_id, 
	phppos_customers.deleted, 
	phppos_customers.tier_id, 
	phppos_customers.card_issuer, 
	phppos_customers.cc_preview, 
	phppos_customers.cc_ref_no, 
	phppos_customers.cc_expire, 
	phppos_customers.cc_token, 
	phppos_customers.tax_certificate, 
	phppos_customers.account_number, 
	phppos_customers.person_id,
	phppos_people.first_name
FROM
	phppos_customers
	INNER JOIN
	phppos_people
	ON 
		phppos_customers.person_id = phppos_people.person_id ;

SET FOREIGN_KEY_CHECKS = 1;
